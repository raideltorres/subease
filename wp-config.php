<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'subease');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'rT&2%-*p=x|99}gY<MQL%KM3P>j+F6a+xEN!PEu!Df:jgUN?Y.8M#[{5RC>(B>#F');
define('SECURE_AUTH_KEY',  '%EgXUdb/f%G^<y0{%E]g48m.#j7)_mRZ5z/_e$dW(a `Z?vD+kn++n9X Cmy0;{b');
define('LOGGED_IN_KEY',    'lB<}d)fS2d1*N;>6cmLuU-5+*=y#kiU$.Ifw>+[lbDlZw!;[1qxIS+m?: cqF/Ue');
define('NONCE_KEY',        'mKWc@N}2|3hh:jS}W[7*9p.k^~XO^:$V{*d#4ZSKrSYlXNA7#=Binp`&M6s!XW2&');
define('AUTH_SALT',        ';vuO,CD4{E0Q@(Z/X$VIs=$){]2r.lM^YP~&/IRH{WD[3=JK4M9_tQW^,yGl%>[m');
define('SECURE_AUTH_SALT', 'nUq)PXO~l<,I#7NwJS9VMgG|3$iE@SW3TqP]T&S,tJFL;vJ.s`gP.^mWgH[6U1<N');
define('LOGGED_IN_SALT',   'W[|V*X+s1{`B3ZK%[.:,C]xs1EgUr`$PBv7i0DeXxh#iAj!_|x7-|2!%G`Z[WwO3');
define('NONCE_SALT',       'j>]VC!PfR+h@/Jp|`EgprjC%t1f6Xc0EdjE|_L8gI.wO>S-|vyCPZlg@mgq<crfX');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
