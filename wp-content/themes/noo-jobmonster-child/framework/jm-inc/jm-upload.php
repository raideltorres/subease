<?php

if ( ! function_exists( 'noo_register_upload_script' ) ) :

	function noo_register_upload_script() {
		$js_folder_uri = SCRIPT_DEBUG ? NOO_ASSETS_URI . '/js' : NOO_ASSETS_URI . '/js/min';
		$js_suffix     = SCRIPT_DEBUG ? '' : '.min';
		wp_register_script( 'noo_plupload', $js_folder_uri . '/noo_plupload' . $js_suffix . '.js', array( 'jquery', 'plupload-all' ), null, true );

		$noo_plupload = array(
			'ajaxurl' => admin_url('admin-ajax.php'),
			'remove' => wp_create_nonce('noo-plupload-remove'),
			'confirmMsg' => __('Are you sure you want to delete this?', 'noo'),
		);
		wp_localize_script('noo_plupload', 'nooPluploadL10n', $noo_plupload);
	}

	add_action( 'wp_enqueue_scripts', 'noo_register_upload_script' );

endif;

if( !function_exists( 'noo_image_upload_form_field' ) ) :
	function noo_image_upload_form_field($field_name='',$value='',$is_multiple=false) {
		if( empty( $field_name ) ) return;
		wp_enqueue_script('noo_plupload');
		?>
		<div class="upload-btn-wrap">
			<div id="noo_upload-<?php echo $field_name; ?>-btn" class="btn btn-default">
				<i class="fa fa-folder-open-o"></i> <?php _e('Browse','noo');?>
			</div>
			<div class="noo_upload-status"></div>
		</div>
		<div id="noo_upload-<?php echo $field_name; ?>-preview" class="upload-preview-wrap">
			<?php
				noo_show_list_image_uploaded($value, $field_name);
			?>
		</div>
		<script>
		jQuery(document).ready(function($) {
			$('#noo_upload-<?php echo $field_name; ?>-btn').noo_upload({
				input_name : '<?php echo $field_name; ?>',
				container : 'noo_upload-<?php echo $field_name; ?>-wrap',
				browse_button : 'noo_upload-<?php echo $field_name; ?>-btn',
				tag_thumb : 'noo_upload-<?php echo $field_name; ?>-preview',
				multi_upload : <?php echo ( $is_multiple ? "true" : "false" ); ?>
			});
		});
		</script>
		<?php
	}
endif;

if( !function_exists( 'noo_file_upload_form_field' ) ) :
	function noo_file_upload_form_field($field_name='',$extensions=array(),$value='',$is_multiple=false) {
		if( !wp_script_is( 'noo_plupload', 'registered' ) ) {
			noo_register_upload_script();
		}
		wp_enqueue_script('noo_plupload');

		$id = uniqid('plupload_');
		$max_upload_size = wp_max_upload_size();
		if ( ! $max_upload_size ) {
			$max_upload_size = 0;
		}
		$plupload_init = array(
			'runtimes' => 'html5,flash,html4',
			'browse_button' => $id.'_uploader-btn',
			'container' => $id.'_upload-container',
			'file_data_name' => 'file',
			'max_file_size' => $max_upload_size,
			'url' => esc_url_raw( add_query_arg( array( 'action' => 'noo_plupload', 'nonce' => wp_create_nonce('noo-plupload') ), admin_url('admin-ajax.php') ) ),
			'flash_swf_url' => includes_url('js/plupload/plupload.flash.swf'),
			'filters' => array(array('title' => __('Allowed Files', 'noo'), 'extensions' => implode(',', $extensions))),
			'multipart' => true,
			'urlstream_upload' => true,
			'multi_selection' => $is_multiple
		);
		$plupload_init_json = htmlspecialchars(json_encode($plupload_init), ENT_QUOTES, 'UTF-8');
		?>
		<div id="<?php echo esc_attr($id.'_upload-container'); ?>" class="noo-plupload">
			<div class="noo-plupload-btn" data-settings="<?php echo esc_attr($plupload_init_json)?>">
				<a href="#" class="btn btn-default" id="<?php echo esc_attr($id.'_uploader-btn'); ?>"><i class="fa fa-folder-open-o"></i> <?php esc_html_e('Browse','noo')?></a>
		    	<p class="help-block"><?php printf( __( 'Maximum upload file size: %s', 'noo' ), esc_html( size_format( $max_upload_size ) ) ); ?></p>
		    	<?php if( !empty( $extensions ) ) : ?>
					<p class="help-block"><?php echo sprintf( __('Allowed file: %s', 'noo'), '.' . implode(', .', $extensions) ); ?></p>
				<?php endif; ?>
			</div>
			<div class="noo-plupload-preview">
				<?php if( !empty($value) ) :
					$file_name = noo_json_decode( $value );
					$file_name = $file_name[0];
				?>
				<div>
					<a class="delete-pluploaded" data-filename="<?php echo esc_attr($file_name); ?>" href="#" title="<?php _e('Delete File', 'noo'); ?>"><i class="fa fa-times-circle"></i>x</a>
					<strong><?php echo esc_html($file_name); ?></strong>
				</div>
				<?php endif; ?>
			</div>
			<input type="hidden" class="noo-plupload-value" name="<?php echo esc_attr($field_name)?>" value="<?php echo esc_attr($value); ?>" >
		</div>
		<?php
	}
endif;

if ( ! function_exists( 'jm_get_allowed_attach_file_types' ) ) :
	function jm_get_allowed_attach_file_types() {
		$settings = Noo_Resume::get_setting('extensions_upload_resume', 'doc,pdf');
		$settings = !empty( $settings ) ? explode(',', $settings ) : array();
		$allowed_exts = array();
		foreach ($settings as $type) {
			$type = trim($type);
			if( empty( $type ) || $type === '.' ) continue;
			$type = $type[0] === '.' ? substr( $type, 1 ) : $type;
			$allowed_exts[] = $type;
		}

		return $allowed_exts;
	}

endif;

if ( ! function_exists( 'noo_show_list_image_uploaded' ) ) :

	function noo_show_list_image_uploaded( $thumb_id, $input_name ) {
		echo '<input type="hidden" name="' . $input_name . '" value="" />'; // blank input to make sure you always have the submitted images
		if( !empty($thumb_id) ) {
			if ( is_array( $thumb_id ) ) {
				foreach ( $thumb_id as $img ) :
					$img = trim($img);
					$img_src = wp_get_attachment_image_src( $img, 'thumbnail' );
					if( !empty( $img ) && !empty( $img_src ) ) :
						echo '<div class="image-upload-thumb">';
						echo "<img src='{$img_src[0]}' alt='*' />";
						echo '<input type="hidden" name="' . $input_name . '[]" value="' . $img . '" class="img-' . $img . '"/>';
						echo '<a class="delete-uploaded" data-fileid="' . $img .'" href="#" title="' . __('Remove', 'noo') . '"><i class="fa fa-times-circle"></i></a></p>';
						echo '</div>';
					endif;
				endforeach;
			} else {
				$img_src = is_numeric( $thumb_id ) ? wp_get_attachment_image_src( $thumb_id, 'thumbnail' ) : '';
				$img_src = !empty( $img_src ) ? $img_src[0] : $thumb_id;
				echo '<div class="image-upload-thumb">';
				echo "<img src='{$img_src}' alt='*' />";
				echo '<input type="hidden" name="' . $input_name . '" value="' . $thumb_id . '" class="img-' . $thumb_id . '"/>';
				echo '<a class="delete-uploaded" data-fileid="' . $thumb_id .'" href="#" title="' . __('Remove', 'noo') . '"><i class="fa fa-times-circle"></i></a></p>';
				echo '</div>';
			}
		}

	}

endif;

// backward comparative
if ( ! function_exists( 'show_list_image_upload' ) ) :

	function show_list_image_upload( $thumb_id, $input_name ) {
		noo_show_list_image_uploaded( $thumb_id, $input_name );
	}

endif;

if( !function_exists( 'subease_image_upload_form_field' ) ) :
function subease_image_upload_form_field($field_name='',$value='',$dimentions = 1, $is_multiple=false, $offset = 0) {
	/*
	 * WARNING: This function's output is buffered into a string, so please make sure all HTML output contains NO DOUBLE-QUOTES
	 */

	if( empty( $field_name ) ) return;
	wp_enqueue_script('noo_plupload');
	if ($dimentions > 1) {
		$uniq = "_" . $offset;
	}

	?>
		<script>
		jQuery(document).ready(function($) {
			var fg = jQuery('input[type=\'text\'][name*=\'_experience_employer\'').length;
			console.log ('Number of Projects = ' + fg);
		});

		</script>
		<div class='upload-btn-wrap'>
			<div id='noo_upload-<?php echo $field_name . $uniq; ?>-btn' class='btn btn-default'>
				<i class='fa fa-folder-open-o'></i> <?php _e('Browse','noo');?>
			</div>
			<div class='noo_upload-status'></div>
		</div>
		<div id='noo_upload-<?php echo $field_name . $uniq; ?>-preview' class='upload-preview-wrap'>
			<?php
				subease_show_list_image_uploaded($value, $field_name, $dimentions, $is_multiple, $offset);
			?>
		</div>
		<script>
		jQuery(document).ready(function($) {


			$('#noo_upload-<?php echo $field_name . $uniq; ?>-btn').noo_upload({
				input_name : '<?php echo $field_name . $uniq; ?>',
				container : 'noo_upload-<?php echo $field_name . $uniq; ?>-wrap',
				browse_button : 'noo_upload-<?php echo $field_name . $uniq; ?>-btn',
				tag_thumb : 'noo_upload-<?php echo $field_name . $uniq; ?>-preview',
				multi_upload : <?php echo ( $is_multiple ? 'true' : 'false' ); ?>
			});
		});
		</script>
		<?php
	}
endif;

if ( ! function_exists( 'subease_show_list_image_uploaded' ) ) :

function subease_show_list_image_uploaded( $thumb_id, $input_name, $dimentions = 1, $is_multiple = false, $offset = 0, $display_only = false) {
	/*
	 * WARNING: This function's output is buffered into a string, so please make sure all HTML output contains NO DOUBLE-QUOTES
	 */
	echo "<input type='hidden' name='" . $input_name . "' value='' />"; // blank input to make sure you always have the submitted images
	if( !empty($thumb_id) ) {
		if ($is_multiple == true) {
			// Make sure it is always an array
			$thumb_id = is_array($thumb_id) ? $thumb_id : array($thumb_id);

			// If multi-dimentional array
			$d_str = '_' . $offset;
			for ($i = 1; $i < $dimentions; $i++) {
				$d_str .= '[]';
			}
			foreach ( $thumb_id as $img ) {
				$img = trim($img);
				$img_src = wp_get_attachment_image_src( $img, 'thumbnail' );
				if( !empty( $img ) && !empty( $img_src ) ) {
					echo "<div class='image-upload-thumb'>";
					echo "<img src='{$img_src[0]}' alt='*' />";
					if ($display_only == false) {
						echo "<input type='hidden' name='" . $input_name . $d_str . "' value='" . $img . "' class='img-" . $img . "'/>";
						echo "<a class='delete-uploaded' data-fileid='" . $img ."' href='#' title='" . __("Remove", "noo") . "'><i class='fa fa-times-circle'></i></a></p>";
					}
					echo "</div>";
				}
			}
		} else {
			$img_src = is_numeric( $thumb_id ) ? wp_get_attachment_image_src( $thumb_id, "thumbnail" ) : '';
			$img_src = !empty( $img_src ) ? $img_src[0] : $thumb_id;
			echo "<div class='image-upload-thumb'>";
			echo "<img src='{$img_src}' alt='*' />";
			if ($display_only == false) {
				echo "<input type='hidden' name='" . $input_name . "' value='" . $thumb_id . "' class='img-" . $thumb_id . "'/>";
				echo "<a class='delete-uploaded' data-fileid='" . $thumb_id ."' href='#' title='" . __("Remove", "noo") . "'><i class='fa fa-times-circle'></i></a></p>";
			}
			echo "</div>";
		}
	}

}

endif;
