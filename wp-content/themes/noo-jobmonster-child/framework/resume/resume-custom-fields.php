<?php

if( !function_exists( 'jm_get_resume_custom_fields' ) ) :
	function jm_get_resume_custom_fields( $all = true ) {
		$custom_fields = noo_get_custom_fields( 'noo_resume_custom_field', 'noo_resume_field_');

		if( empty( $custom_fields ) ) {
			$custom_fields = noo_get_custom_fields( 'noo_resume', 'noo_resume_field_');
		}

		if( $all ) {
			$default_fields = jm_get_resume_default_fields();
			// $custom_fields = array_merge( array_diff_key($default_fields, $custom_fields), $custom_fields );
			$custom_fields = noo_merge_custom_fields( $default_fields, $custom_fields );
		}

		return $custom_fields;
	}
endif;

if( !function_exists( 'jm_resume_custom_fields_menu' ) ) :
	function jm_resume_custom_fields_menu() {
		add_submenu_page(
			'edit.php?post_type=noo_resume',
			__( 'Custom Fields', 'noo' ),
			__( 'Custom Fields', 'noo' ),
			'edit_theme_options', 'resume_custom_field',
			'jm_resume_custom_fields_setting' );
	}

	add_action( 'admin_menu', 'jm_resume_custom_fields_menu', 10 );
endif;

if( !function_exists( 'jm_resume_custom_fields_setting' ) ) :
	function jm_resume_custom_fields_setting(){
		?>
		<div class="wrap">
			<form action="options.php" method="post">
				<?php
				noo_custom_fields_setting(
					'noo_resume_custom_field',
					'noo_resume_field_',
					jm_get_resume_custom_fields()
				);
				submit_button(__('Save Changes','noo'));
				?>
			</form>
		</div>
		<?php
	}
endif;

if( !function_exists( 'jm_resume_render_form_field') ) :
	function jm_resume_render_form_field( $field = array(), $resume_id = 0 ) {
		$blank_field = array( 'name' => '', 'label' => '', 'type' => 'text', 'value' => '', 'required' => '', 'is_disabled' => '' );
		$field = is_array( $field ) ? array_merge( $blank_field, $field ) : $blank_field;
		if( !isset( $field['name'] ) || empty( $field['name'] ) ) return;

		$field_id = '';
		if( isset( $field['is_default'] ) ) {
			if( isset( $field['is_disabled'] ) && ($field['is_disabled'] == 'yes') ) {
				return;
			}

			$field_id = $field['name'];
		} else {
			$field_id = '_noo_resume_field_'.sanitize_title($field['name']);
		}

		$value = !empty( $resume_id ) ? noo_get_post_meta( $resume_id, $field_id, '' ) : '';
		$value = !is_array($value) ? trim($value) : $value;

		$params = apply_filters( 'jm_resume_render_form_field_params', compact( 'field', 'field_id', 'value' ), $resume_id );
		extract($params);

		?>
		<div class="form-group row">
			<label for="<?php echo esc_attr($field_id)?>" class="col-sm-5 control-label"><?php echo(isset( $field['label_translated'] ) ? $field['label_translated'] : $field['label'])  ?></label>
			<div class="col-sm-7">
				<?php noo_render_field( $field, $field_id, $value ); ?>
		    </div>
		</div>
		<?php
	}
endif;





if( !function_exists( 'jm_resume_render_search_field') ) :
	function jm_resume_render_search_field( $field = array() ) {
		$blank_field = array( 'name' => '', 'label' => '', 'type' => 'text', 'value' => '', 'required' => '', 'is_disabled' => '' );
		$field = is_array( $field ) ? array_merge( $blank_field, $field ) : $blank_field;
		if( !isset( $field['name'] ) || empty( $field['name'] ) ) return;

		$field_id = '';
		if( isset( $field['is_default'] ) ) {
			$field_id = $field['name'];
		} else {
			$field_id = '_noo_resume_field_'.sanitize_title($field['name']);
		}

		$params = apply_filters( 'jm_resume_render_search_field_params', compact( 'field', 'field_id', 'value' ) );
		extract($params);

		$field['required'] = ''; // no need for required fields in search form

		$value = isset($_GET[$field_id]) ? $_GET[$field_id] : '';
		$value = !is_array($value) ? trim($value) : $value;
		?>
		<div class="form-group">
			<label for="<?php echo 'search-' . esc_attr($field_id)?>" class="control-label"><?php echo(isset( $field['label_translated'] ) ? $field['label_translated'] : $field['label'])  ?></label>
			<div class="advance-search-form-control">
				<?php
					if ( $field['type'] == "text" ) {
						global $wpdb;
						$field['value'] = $wpdb->get_col(
							$wpdb->prepare('
								SELECT DISTINCT meta_value
								FROM %1$s
								LEFT JOIN %2$s ON %1$s.post_id = %2$s.ID
								WHERE meta_key = \'%3$s\' AND post_type = \'%4$s\' AND post_status = \'%5$s\'
								', $wpdb->postmeta, $wpdb->posts, $field_id, 'noo_resume', 'publish'));
						$field['type'] = 'select';
						$field['no_translate'] = true;
					}
					noo_render_field( $field, $field_id, $value, 'search' );
				?>
		    </div>
		</div>
		<?php
	}
endif;

if( !function_exists( 'jm_resume_advanced_search_field' ) ) :
	function jm_resume_advanced_search_field( $field_val = '' ) {
		if(empty($field_val) || $field_val == 'no' )
			return '';

		$field_arr = explode('|', $field_val);
		$field_id = isset( $field_arr[0] ) ? $field_arr[0] : '';

		if( empty( $field_id ) ) return '';

		$fields = jm_get_resume_custom_fields();

		$field_prefix = '_noo_resume_field_';
		$field_id = str_replace($field_prefix, '', $field_id);

		foreach ($fields as $field) {
			if ( sanitize_title( $field['name'] ) == $field_id ) {
				jm_resume_render_search_field( $field );
				break;
			}
		}
		return '';
	}
endif;

/*
 * SUBEASE FUNCTIONS
 */

if (! function_exists('subease_render_resume_element_text_only')) :

function subease_render_resume_element_text_only($field = array(), $resume_id = '', $separator = ', ', $return = false) {
	if (! isset($field['name']) || empty($field['name']))
		return false;
		$field['type'] = isset($field['type']) ? $field['type'] : 'text';
		$id = '_noo_resume_field_' . sanitize_title($field['name']);
		if (isset($field['is_default'])) {
			if (isset($field['is_disabled']) && ($field['is_disabled'] == 'yes'))
				$id = $field['name'];
		}
		$value = $resume_id ? noo_get_post_meta($resume_id, $id, '') : '';
		$label = isset($field['label_translated']) ? $field['label_translated'] : $field['label'];

		$value = ! is_array($value) ? trim($value) : $value;
		if (empty($value)) {
			return '';
		}

		$value = noo_convert_custom_field_value($field, $value);
		if (in_array($field['type'], array(
			'multiple_select',
			'checkbox',
			'radio'
		))) {
			$value = implode($separator, $value);
		}

		if (! empty($value)) {
			if ($return == true) {
				return $value;
			} else {
				echo $value;
			}
		}
}


endif;





if( !function_exists( 'subease_resume_render_form_field_no_label') ) :
function subease_resume_render_form_field_no_label( $field = array(), $resume_id = 0 ) {
	$blank_field = array( 'name' => '', 'label' => '', 'type' => 'text', 'value' => '', 'required' => '', 'is_disabled' => '' );
	$field = is_array( $field ) ? array_merge( $blank_field, $field ) : $blank_field;
	if( !isset( $field['name'] ) || empty( $field['name'] ) ) return;

	$field_id = '';
	if( isset( $field['is_default'] ) ) {
		if( isset( $field['is_disabled'] ) && ($field['is_disabled'] == 'yes') ) {
			return;
		}

		$field_id = $field['name'];
	} else {
		$field_id = '_noo_resume_field_'.sanitize_title($field['name']);
	}

	$value = !empty( $resume_id ) ? noo_get_post_meta( $resume_id, $field_id, '' ) : '';
	$value = !is_array($value) ? trim($value) : $value;

	$params = apply_filters( 'jm_resume_render_form_field_params', compact( 'field', 'field_id', 'value' ), $resume_id );
	extract($params);

	?>
		<div class="form-group row">
			<div class="col-sm-7">
				<?php noo_render_field( $field, $field_id, $value ); ?>
		    </div>
		</div>
		<?php
	}
endif;

if( !function_exists( 'subease_resume_render_form_field') ) :
function subease_resume_render_form_field( $field = array(), $resume_id = 0, $field_name = '', $value = '', $force_field_type = '' ) {
    $blank_field = array( 'name' => '', 'label' => '', 'type' => 'text', 'value' => '', 'required' => '', 'is_disabled' => '' );
    $field = is_array( $field ) ? array_merge( $blank_field, $field ) : $blank_field;
    if( !isset( $field['name'] ) || empty( $field['name'] ) ) return;

    $field_id = '';
    if( isset( $field['is_default'] ) ) {
        if( isset( $field['is_disabled'] ) && ($field['is_disabled'] == 'yes') ) {
            return;
        }

        $field_id = $field_name;
    } else {
        $field_id = $field_name;
    }

    $value = !is_array($value) ? trim($value) : $value;

    // Force field type if requested
    if ( isset ($force_field_type) && ( !empty($force_field_type))) {
        $field['type'] = $force_field_type;
    }
    $params = apply_filters( 'jm_resume_render_form_field_params', compact( 'field', 'field_id', 'value' ), $resume_id );
    extract($params);

    ?>
		<div class="form-group row">
			<div class="col-sm-7">
				<?php noo_render_field( $field, $field_id, $value ); ?>
		    </div>
		</div>
		<?php
	}
endif;

if( !function_exists( 'subease_get_job_type_from_resume' ) ) :
function subease_get_job_type_from_resume($resume = null ) {
	global $noo_job_type;
	global $noo_job_type_colors;

	if( is_int( $resume ) ) {
		$resume = get_post( $resume );
	}

	if( empty( $resume->post_type ) || !is_object( $resume ) || $resume->post_type !== 'noo_resume' ) {
		return;
	}

	if( empty( $noo_job_type ) ) {
		$noo_job_type = array();
	}

	if( !isset( $noo_job_type[$resume->ID] ) ) {

		// Get the job types stored in the resume
		$resume_job_types = $resume->ID ? noo_get_post_meta($resume->ID, "_job_type", '') : '';

		// Now the whole whole taxonomy object for all the needed Job_Types
		$needed_job_types = noo_json_decode($resume_job_types);
		$job_type_terms = empty( $needed_job_types ) ? array() : get_terms( 'job_type', array('include' => array_merge($needed_job_types, array(-1)), 'hide_empty' => false, 'fields' => 'all') );


		if ( !is_wp_error( $resume_job_types ) && !empty( $resume_job_types ) ) {
			$noo_job_type_colors = empty( $noo_job_type_colors ) ? get_option('noo_job_type_colors') : $noo_job_type_colors;
			foreach ($resume_job_types as $type) {
				$value = array();
				$value['color'] = isset($noo_job_type_colors[$type]) ? $noo_job_type_colors[$type] : '';
				foreach ($job_type_terms as $terms) {
					if ($terms->term_id == $type) {
						$value['object'] = $terms;
						break;
					}
				}
				$noo_job_type[$type] = $value;
			}
		}
	}

	return $noo_job_type;
}
endif;