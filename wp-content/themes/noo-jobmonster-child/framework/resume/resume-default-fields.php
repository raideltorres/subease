<?php

if( !function_exists( 'jm_get_resume_default_fields' ) ) :
	function jm_get_resume_default_fields() {
		$default_fields = array(
			'_language' => array(
					'name' => '_language',
					'label' => __('Language', 'noo'),
					'type' => 'text',
					'value' => '',
					'std' => __( 'Your working language', 'noo' ),
					'is_default' => true,
					'required' => false
				),
			'_highest_degree' => array(
					'name' => '_highest_degree',
					'label' => __('Highest Degree Level', 'noo'),
					'type' => 'text',
					'value' => '',
					'std' => __( 'eg. &quot;Bachelor Degree&quot;', 'noo' ),
					'is_default' => true,
					'required' => false
				),
			'_experience_year' => array(
					'name' => '_experience_year',
					'label' => __('Total Years of Experience', 'noo'),
					'type' => 'text',
					'value' => '',
					'std' => __( 'eg. &quot;1&quot;, &quot;2&quot;', 'noo' ),
					'is_default' => true,
					'required' => false
				),
			'_job_category' => array(
					'name' => '_job_category',
					'label' => __('Job Category', 'noo'),
					'type' => 'job_category',
					'allowed_type' => array(
						'job_category'		=> __( 'Job Category', 'noo' )
					),
					'value' => '',
					'std' => '',
					'is_default' => true,
					'is_tax' => true,
					'required' => true
				),
			'_job_level' => array(
					'name' => '_job_level',
					'label' => __('Expected Job Level', 'noo'),
					'type' => 'text',
					'value' => '',
					'std' => __( 'eg. &quot;Junior&quot;, &quot;Senior&quot;', 'noo' ),
					'is_default' => true,
					'required' => false
				),
			'_job_location' => array(
					'name' => '_job_location',
					'label' => __('Job Location', 'noo'),
					'type' => 'multiple_select',
					'allowed_type' => array(
						'select'			=> __('Select', 'noo'),
						'multiple_select'	=> __( 'Multiple Select', 'noo' ),
						'radio'				=> __( 'Radio', 'noo' ),
						'checkbox'			=> __( 'Checkbox', 'noo' )
					),
					'value' => '',
					'std' => '',
					'is_default' => true,
					'is_tax' => true,
					'required' => false
				),
			'_job_type' => array(
				'name' => '_job_type',
				'label' => __('Job Type', 'noo'),
				'type' => 'multiple_select',
				'allowed_type' => array(
					'select'			=> __('Select', 'noo'),
					'multiple_select'	=> __( 'Multiple Select', 'noo' ),
					'radio'				=> __( 'Radio', 'noo' ),
					'checkbox'			=> __( 'Checkbox', 'noo' )
				),
				'value' => '',
				'std' => '',
				'is_default' => true,
				'is_tax' => true,
				'required' => false
				),
			);

		return apply_filters( 'jm_resume_default_fields', $default_fields );
	}
endif;


if( !function_exists( 'jm_resume_tax_field_get_sorted_tax' ) ) :
	function jm_resume_tax_field_get_sorted_tax( $field_name, Array &$terms, $parent_term_id = 0, $depth = 0 )  {
		// The depth is not stored anywhere yet. It would be nice if we could set $term->depth or something.
		$childterms = get_terms( $field_name, array( 'hide_empty' => 0, 'parent' => $parent_term_id ) );
		foreach ( $childterms as $key => $term ) :
			$terms[] = $term;
			jm_resume_tax_field_get_sorted_tax( $field_name, $terms, $term->term_id, $depth + 1 );
		endforeach;
	}
endif;

if( !function_exists( 'jm_resume_tax_field_params' ) ) :
	function jm_resume_tax_field_params( $args = array(), $resume_id = 0 )  {
		extract($args);

		$blank_field = array( 'name' => '', 'label' => '', 'type' => 'text', 'value' => '', 'required' => '', 'is_disabled' => '' );
		$field = is_array( $field ) ? array_merge( $blank_field, $field ) : $blank_field;

		if( in_array( $field['name'], array( '_job_category', '_job_location', '_job_type' ) ) ) {
			$field_id = $field['name'];

			$field_value = array();
			$term_id = str_replace('_job_', 'job_', $field_id);
			if ( $field_id === '_job_category' ) {
				$terms = array();
				jm_resume_tax_field_get_sorted_tax( $term_id, $terms );
			} else {
				$terms = get_terms( $term_id, array( 'hide_empty' => 0 ) );
			}
			foreach ($terms as $term) {
				if( $field_id == '_job_category' ) {
					// Display depth is indicated by number of additional '|' characters.
					// Without the taxonomy depth being passed along with the term,
					// display is currently limited to a depth of one.
					$field_value[] = $term->term_id . ($term->parent ? '||' : '|') . $term->name;
				} else {
					$field_value[] = $term->term_id . '|' . $term->name;
				}
			}
			$field['value'] = $field_value;
			$field['no_translate'] = true;

			if( !empty( $resume_id ) ) {
				$value = $resume_id ? noo_get_post_meta( $resume_id, $field_id, '' ) : $value;
				$value = noo_json_decode( $value );
			}

			if( empty( $field['type'] ) || $field['type'] == 'text' ) {
				$default_fields = jm_get_resume_default_fields();
				$field['type'] = $default_fields[$field['name']]['type'];
			}
		}

		return compact( 'field', 'field_id', 'value' );
	}

	add_filter( 'jm_resume_render_form_field_params', 'jm_resume_tax_field_params', 10, 3 );
	add_filter( 'jm_resume_render_search_field_params', 'jm_resume_tax_field_params' );
endif;