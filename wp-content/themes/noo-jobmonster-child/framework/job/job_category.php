<?php

if( !function_exists( 'jm_job_render_field_job_category' ) ) :
	function jm_job_render_field_job_category( $field = array(), $field_id = '', $value = array(), $form_type = '' )  {
		$field['type'] = 'multiple_select';
		noo_render_select2_field( $field, $field_id, $value, $form_type );
	}

	add_filter( 'noo_render_field_job_category', 'jm_job_render_field_job_category', 10, 4 );
endif;

