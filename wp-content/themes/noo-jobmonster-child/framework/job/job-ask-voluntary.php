<?php
/**
 * Return whether the Job is requesting Voluntary Questions
 *	job_id = The Job_ID you want to check
 * @author J. Jungalwala
 * @return boolean
 */
if (! function_exists('job_ask_voluntary')) {

	function job_ask_voluntary($job_id) {
		if (!isset($job_id) || ! is_numeric($job_id)) {
			return false;
		}

		$ask_voluntary = noo_get_post_meta($job_id, '_noo_job_field_job_setting_ask_voluntary', '');

		if (isset($ask_voluntary) && (is_array($ask_voluntary)) && (strcasecmp ($ask_voluntary[0],'on') == 0)) {
			return true;
		} else {
			return false;
		}
	}
}

?>