<?php

if( !function_exists('jm_job_pre_get_posts') ) :
	function jm_job_pre_get_posts($query) {
		if( is_admin() ) {
			return $query;
		}

		if( jm_is_job_query( $query) ) {
			if( $query->is_main_query() && $query->is_singular ) {
				if( !$query->is_preview && empty( $query->query_vars['post_status'] ) ) {
					// add expired to viewable link
					$post_status = array( 'publish', 'expired' );
					if( current_user_can( 'edit_posts' ) ) {
						$post_status[] = 'pending';
					}
					$query->set( 'post_status', $post_status );
				}

				return $query;
			}

			// if ( $query->is_search ) {
				$query = jm_job_query_from_request( $query, $_GET );
			// }
		}
	}

	add_action( 'pre_get_posts', 'jm_job_pre_get_posts' );
endif;

if( !function_exists('jm_is_job_query') ) :
	function jm_is_job_query( $query ) {
		return isset($query->query_vars['post_type']) && ($query->query_vars['post_type'] === 'noo_job');
	}
endif;

if( !function_exists('jm_user_job_query') ) :
	function jm_user_job_query($employer_id='',$is_paged = true,$only_publish = false){
		if(empty($employer_id)){
			$employer_id = get_current_user_ID();
		}

		$args = array(
			'post_type'=>'noo_job',
			'author'=>$employer_id,
		);

		if($is_paged){
			if( is_front_page() || is_home()) {
				$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : ( ( get_query_var( 'page' ) ) ? get_query_var( 'page' ) : 1 );
			} else {
				$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
			}
			$args['paged'] = $paged;
		}

		if($only_publish){
			$args['post_status'] = array('publish');
		} else {
			$args['post_status'] = array('publish','pending','pending_payment','expired','inactive');
		}

		$user_job_query = new WP_Query($args);

		return $user_job_query;
	}
endif;

if( !function_exists('jm_job_query_from_request') ) :
	function jm_job_query_from_request( &$query, $REQUEST = array() ) {
		if( empty( $query ) || empty( $REQUEST ) ) {
			return $query;
		}

		$tax_query = array();
		$tax_list = array(
		    'location' => 'job_location',
		    'category' => 'job_category',
		    'type' => 'job_type',
		    'tag' => 'job_tag'
		);
		/*
		 * If Job Location and Travel Distance are set, don't actually do a Job Location search yet
		 */
		if ( (! empty($REQUEST['location'])) && ( is_numeric($REQUEST['_noo_resume_field__worker_travel_distance']))) {
		    unset($tax_list['location']);
		}

		$tax_list = apply_filters( 'jm_job_query_tax_list', $tax_list );
		foreach ($tax_list as $tax_key => $term) {
			if( isset( $REQUEST[$tax_key] ) && !empty( $REQUEST[$tax_key] ) ) {
				$tax_query[] = array(
					'taxonomy'     => $term,
					'field'        => 'slug',
					'terms'        => $REQUEST[$tax_key]
				);
			}
		}

		$tax_query = apply_filters( 'jm_job_search_tax_query', $tax_query, $REQUEST );

		if( !empty( $tax_query ) ) {
			$tax_query['relation'] = 'AND';
			if( is_object( $query ) && get_class( $query ) == 'WP_Query' ) {
				$query->tax_query->queries = $tax_query;
				$query->query_vars['tax_query'] = $query->tax_query->queries;

				// tag is a reserved keyword so we'll have to remove it form the query
				unset( $query->query['tag'] );
				unset( $query->query_vars['tag'] );
				unset( $query->query_vars['tag__in'] );
				unset( $query->query_vars['tag_slug__in'] );
			} elseif( is_array( $query ) ) {
				$query['tax_query'] = $tax_query;
			}
		}

		$meta_query = array();
		$get_keys = array_keys($REQUEST);

		$job_fields = jm_get_job_search_custom_fields();
		foreach ($job_fields as $field) {
			$field_id = isset( $field['is_default'] ) && $field['is_default'] ? $field['name'] : jm_job_custom_fields_name( $field['name'] );
			if( isset( $REQUEST[$field_id] ) && !empty( $REQUEST[$field_id]) ) {
				$value = noo_sanitize_field( $REQUEST[$field_id], $field );

				if ($field_id == '_noo_job_field_job_experience_years') {
					$meta_query[] = array(
						'key' => $field_id,
						'value' => $value,
						'compare' => '>='
					);
					continue;
				}


				if(is_array($value)){
					$temp_meta_query = array( 'relation' => 'OR' );
					foreach ($value as $v) {
						if( empty( $v ) ) continue;
						$temp_meta_query[]	= array(
							'key'     => $field_id,
							'value'   => '"'.$v.'"',
							'compare' => 'LIKE'
						);
					}
					$meta_query[] = $temp_meta_query;
				} else {
					$meta_query[]	= array(
						'key'     => $field_id,
						'value'   => $value
					);
				}
			} elseif( ( isset( $field['type'] ) && $field['type'] == 'datepicker' ) && ( isset( $REQUEST[$field_id.'_start'] ) || isset( $REQUEST[$field_id.'_end'] ) ) ) {
				if( $field_id == 'date' ) {
					$date_query = array();
					if( isset( $REQUEST[$field_id.'_start'] ) && !empty( $REQUEST[$field_id.'_start'] ) ) {
						$date_query['after'] = date('Y-m-d', strtotime( $REQUEST[$field_id.'_start'] . ' -1 day' ) );
					}
					if( isset( $REQUEST[$field_id.'_end'] ) && !empty( $REQUEST[$field_id.'_end'] ) ) {
						$date_query['before'] = date('Y-m-d', strtotime( $REQUEST[$field_id.'_end'] . ' +1 day' ) );
					}

					if( is_object( $query ) && get_class( $query ) == 'WP_Query' ) {
						$query->query_vars['date_query'][] = $date_query;
					} elseif( is_array( $query ) ) {
						$query['date_query'] = $date_query;
					}
				} else {
					$value_start = isset( $REQUEST[$field_id.'_start'] ) && !empty( $REQUEST[$field_id.'_start'] ) ? noo_sanitize_field( $REQUEST[$field_id.'_start'], $field ) : 0;
					$value_end = isset( $REQUEST[$field_id.'_end'] ) && !empty( $REQUEST[$field_id.'_end'] ) ? noo_sanitize_field( $REQUEST[$field_id.'_end'], $field ) : strtotime( '2090/12/31');
					$meta_query[]	= array(
						'key'     => $field_id,
						'value'   => array( $value_start, $value_end ),
						'compare' => 'BETWEEN',
						'type' => 'NUMERIC'
					);
				}
			}
		}

		$meta_query = apply_filters( 'jm_job_search_meta_query', $meta_query, $REQUEST );

		if( !empty( $meta_query ) ) {
			$meta_query['relation'] = 'AND';
			if( is_object( $query ) && get_class( $query ) == 'WP_Query' ) {
				$query->query_vars['meta_query'][] = $meta_query;
			} elseif( is_array( $query ) ) {
				$query['meta_query'] = $meta_query;
			}
		}

		return $query;
	}
endif;

if (! function_exists('subease_jobs_search_pre_get_posts')) {

	function subease_jobs_search_pre_get_posts($query) {
		// if (is_search() && $query->is_main_query() && ! is_admin()) {
		global $the_original_paged, $processed_jobs_search;
		$post_type = isset($query->query['post_type'])?$query->query['post_type']:'';
		if (WP_DEBUG === true) {
			// error_log("CALLING: pre_get_posts: is_search(" . (int) is_search() . ") is_main_query(" . (int) $query->is_main_query() . ") is_admin(" . (int) is_admin() . ") post_type({$post_type}) DOING_AJAX=(" . DOING_AJAX . ") processed_jobs_search=(" . $processed_jobs_search . ")");
		}
		if (($post_type == 'noo_job') && (isset($_REQUEST['location'])) && (isset($_REQUEST['_noo_resume_field__worker_travel_distance'])) && (is_numeric($_REQUEST['_noo_resume_field__worker_travel_distance'])) && ($processed_jobs_search == false) && (! is_admin()) && (is_main_query())) {
			// if ((isset($_REQUEST['s'])) && (isset($_REQUEST['post_type'])) && ($_REQUEST['post_type'] == 'noo_job') && (isset($_REQUEST['location'])) && (isset($_REQUEST['_noo_resume_field__worker_travel_distance'])) && (is_numeric($_REQUEST['_noo_resume_field__worker_travel_distance']))) {
			if (WP_DEBUG === true) {
				// error_log("PROCESSING: pre_get_posts: REQUEST['location']=(" . my_print_r($_REQUEST['location']) . ") REQUEST['_noo_resume_field__worker_travel_distance']=" . my_print_r($_REQUEST['_noo_resume_field__worker_travel_distance']) . ")");
			}
			$the_original_paged = $query->get('paged') ? $query->get('paged') : 1;
			$query->set('paged', NULL);
			$query->set('nopaging', TRUE);
			// }
		}
		$processed_jobs_search = false;
	}
}
add_action('pre_get_posts', 'subease_jobs_search_pre_get_posts', 1);

if (! function_exists('subease_jobs_search_the_posts_filter')) {

	function subease_jobs_search_the_posts_filter($posts, $query) {
		global $wp_query, $the_original_paged, $processed_jobs_search;

		$post_type = isset($query->query['post_type'])?$query->query['post_type']:'';
		if (WP_DEBUG === true) {
			// error_log("CALLING: the_posts: Post_type=(" . $post_type . ") the_original_paged=(" . $the_original_paged . ") Request[location]=(" . my_print_r($_REQUEST['location']) . ") Request[_noo_resume_field__worker_travel_distance]=(" . my_print_r($_REQUEST['_noo_resume_field__worker_travel_distance']) . ") ");
		}
		if (is_null($the_original_paged) || $post_type != 'noo_job' || is_admin() || ! is_main_query()) {
			if (WP_DEBUG === true) {
				// error_log("PROCESSING: the_posts: DID NOTHING: Posts Size=(" . count($posts) . ")");
			}
			return $posts;
		}

		$processed_count = 0;
		// Only do this for noo-jobs and Only do this is we are processing Job Location and Travel Distance together
		// if ((isset($_REQUEST['s'])) && (isset($_REQUEST['post_type'])) && ($_REQUEST['post_type'] == 'noo_job') && (isset($_REQUEST['location'])) && (isset($_REQUEST['_noo_resume_field__worker_travel_distance'])) && (is_numeric($_REQUEST['_noo_resume_field__worker_travel_distance']))) {
		if ((isset($_REQUEST['location'])) && (isset($_REQUEST['_noo_resume_field__worker_travel_distance'])) && (is_numeric($_REQUEST['_noo_resume_field__worker_travel_distance']))) {
			$valid_jobs = (int) - 1; // Number of valid jobs after filters applied.

			$processed_jobs_search = true;

			// Convert query_job_location(s) to LatLon
			$query_travel_distance = (int) $_REQUEST['_noo_resume_field__worker_travel_distance'];
			$searched_job_locations = ! is_array($_REQUEST['location']) ? array(
				$_REQUEST['location']
			) : $_REQUEST['location'];
			$searched_job_loc_latlon = array();
			// Get the LatLon (try DB first then Google) for each Job Locations
			foreach ($searched_job_locations as $job_loc) {
				$t = get_term_by('slug', sanitize_title($job_loc), 'job_location');
				$searched_job_loc_latlon[$t->term_id] = get_job_location_latlon($t->term_id);
			}

			$valid_jobs = (int) 0; // Doing a filter, so set starting point to 0

			$filtered_posts = array();
			foreach ($posts as $post) {
				$processed_count ++;
				/*
				 * If this is a location search with a Travel Distance, you need to filter
				 * the results by distance between the Job-Location stored in the Job and the
				 * one passed into the query.
				 *
				 * Note: The Job can have Multiple Locations and the Search can have Multiple Locations.
				 */

				if (isset($_REQUEST['location']) && (isset($_REQUEST['_noo_resume_field__worker_travel_distance'])) && (is_numeric($_REQUEST['_noo_resume_field__worker_travel_distance']))) {
					// Get the list of Job Locations
					$job_location_latlon = array();
					$locations = get_the_terms($post->ID, 'job_location');
					if (! is_array($locations)) {
						// No job location set
						if (WP_DEBUG === true) {
							error_log("Post({$processed_count}/" . count($posts) . ") is being thrown away. Job ({$post->ID}, {$post->post_name}) with no Location set ");
						}
						continue;
					}
					$locations = ! is_array($locations) ? array(
						$locations
					) : $locations; // If not array of locations, make it one.
					foreach ($locations as $loc) {
						$job_location_latlon[$loc->term_id] = get_job_location_latlon($loc->term_id);
					}

					/*
					 * Now we have the list of job locations we are searching for 'searched_job_loc_latlon',
					 * we also have the list of Job Locations for this current job, 'job_location_latlon',
					 * now get the distance between each set
					 */

					foreach ($job_location_latlon as $jll_key => $jll) {
						foreach ($searched_job_loc_latlon as $sjll_key => $sjll) {
							$distance = geo_distance($jll['lat'], $jll['lon'], $sjll['lat'], $sjll['lon']);
							if ((round($distance) <= $query_travel_distance)) {
								// Allow to be added to the list
								$valid_jobs ++;
								$filtered_posts[] = $post;
								if (WP_DEBUG === true) {
									error_log("Post({$processed_count}/" . count($posts) . ") is good. Distance ({$distance} Job Location ({$jll_key})=({$jll['lat']})({$jll['lon']}) Search Location ({$sjll_key})=({$sjll['lat']})({$sjll['lon']})");
								}
								break 2;
							} else {
								// Skip over, didn't match the search query on distance from job location
								if (WP_DEBUG === true) {
									error_log("   Post({$processed_count}/" . count($posts) . "). Not a good match so far. Distance ({$distance} Job Location ({$jll_key})=({$jll['lat']})({$jll['lon']}) Search Location ({$sjll_key})=({$sjll['lat']})({$sjll['lon']})");
								}
								continue;
							}
						}
					} /* Outter Loop */
				}
			}

			// the wanted posts per page here setted on general settings
			$perpage = get_option('posts_per_page');
			remove_filter('the_posts', 'subease_jobs_search_the_posts_filter');

			// Reset the proper number of posts found
			$wp_query->found_posts = count($filtered_posts);
			if (WP_DEBUG === true) {
				error_log("PROCESSING: the_posts: New Posts Size=(" . $wp_query->found_posts . ") Old Posts Size=(" . count($posts) . ")");
			}

			// getting the right posts based on current page and posts per page
			$wp_query->posts = array_slice($filtered_posts, ($perpage * ($the_original_paged - 1)), $perpage);

			// set the paged and other wp_query properties to the right value, to make pagination work
			$wp_query->set('paged', $the_original_paged);
			$wp_query->post_count = count($wp_query->posts);
			$wp_query->max_num_pages = ceil($wp_query->found_posts / $perpage);

			unset($the_original_paged); // clean up global variable
			return $wp_query->posts;
		} else {
			// Do nothing
			if (WP_DEBUG === true) {
				error_log("PROCESSING: the_posts: DID NOTHING: SHOULD NEVER GET HERE: Posts Size=(" . count($posts) . ")");
			}
			return $posts;
		}
	}
}
add_filter('the_posts', 'subease_jobs_search_the_posts_filter', 10, 2);


