<?php
function find_location_form() {
    // include(ABSPATH."/wp-content/themes/YOUR_THEME_NAME/PATH_TO_FORM_FILE/findalocation.php");
    $include_filename = dirname(__FILE__) . '/findalocation.php';
    include ($include_filename);
    return $form;
}
add_shortcode('findalocation', 'find_location_form');



/**
 * * CALCULATE DISTANCE USING LAT/LONG, GIVEN A ZIP CODE **
 */
function location_posts_where($where) {
    global $wpdb;
    // Get user location from ZIP
    $geocode = file_get_contents('https://maps.google.com/maps/api/geocode/json?address=' . get_query_var('user_ZIP') . '&sensor=false&key='.GOOGLE_MAPS_API_KEY);
    $output = json_decode($geocode);
    $lat = $output->results[0]->geometry->location->lat;
    $lng = $output->results[0]->geometry->location->lng;
    $radius = get_query_var('user_radius'); // (in miles)
                                            
    // Append our radius calculation to the WHERE
    $where .= " AND $wpdb->posts.ID IN (SELECT post_id FROM 9ad_lat_lng_post WHERE 
             ( 3959 * acos( cos( radians(" . $lat . ") ) 
                            * cos( radians( lat ) ) 
                            * cos( radians( lng ) 
                            - radians(" . $lng . ") ) 
                            + sin( radians(" . $lat . ") ) 
                            * sin( radians( lat ) ) ) ) <= " . $radius . ")";
    
    // Return the updated WHERE part of the query
    return $where;
}

/**
 * * CALCULATE DISTANCE BETWEEN TWO POINTS OF LATITUDE/LONGITUDE **
 */
function distance($lat1, $lon1, $lat2, $lon2) {
    $theta = $lon1 - $lon2;
    $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
    $dist = acos($dist);
    $dist = rad2deg($dist);
    $miles = $dist * 60 * 1.1515;
    return $miles;
}

?>