<?php ob_start(); ?>
<div>
	<form method="get" action="/location_results">
		<input type="hidden" name="locationsearch" value="Y" />
		<div>
			<div>
				<label>Your ZIP code</label> <input type="number" min="0" max="99999" name="user_ZIP" id="user_ZIP" value="<?php echo get_query_var('user_ZIP');?>" />
			</div>
			<div>
				<label>Locations within:</label> <select name="user_radius" id="user_radius">
					<option <?php if(get_query_var('user_radius') == 25000) {echo ' selected="selected"';}?> value="25000">Any</option>
					<option <?php if(get_query_var('user_radius') == 5) {echo ' selected="selected"';}?> value="5">5 miles</option>
					<option <?php if(get_query_var('user_radius') == 10 || !get_query_var('user_radius')) {echo ' selected="selected"';}?> value="10">10 miles</option>
					<option <?php if(get_query_var('user_radius') == 20) {echo ' selected="selected"';}?> value="20">20 miles</option>
					<option <?php if(get_query_var('user_radius') == 50) {echo ' selected="selected"';}?> value="50">50 miles</option>
					<option <?php if(get_query_var('user_radius') == 100) {echo ' selected="selected"';}?> value="100">100 miles</option>
				</select>
			</div>
		</div>
		<div>
			<input type="submit" value="Find locations" />
		</div>
	</form>
</div>
<?php $form = ob_get_clean(); ?>
