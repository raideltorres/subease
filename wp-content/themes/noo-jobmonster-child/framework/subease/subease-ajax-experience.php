<?php

add_action( 'wp_enqueue_scripts', 'ajax_test_enqueue_scripts' );

function ajax_test_enqueue_scripts() {
	wp_register_script( 'add_experience', SUBEASE_NOO_ASSETS_URI . '/js/addExperience.js', array( 'jquery'), '1.0', true );
	wp_enqueue_script('add_experience');
	wp_localize_script( 'add_experience', 'addExperience', array('ajax_url' => admin_url( 'admin-ajax.php' )));

}

function generate_experience_fields ($experience, $index) {
	$html = array();
	$html[] = "<div class='fields-group'>";
	$html[] = "<input type='text' class='form-control' placeholder='" . __("Employer", "noo") . "' name='_experience_employer[]' value='" . esc_attr($experience["employer"][$index]) . "' />";
	$html[] = "<input type='text' class='form-control' placeholder='" . __("Job Title", "noo") . "' name='_experience_job[]' value='" . esc_attr($experience["job"][$index]) . "' />";
	$html[] = "<input type='text' class='form-control' placeholder='" . __("Start/end date", "noo") . "' name='_experience_date[]' value='" . esc_attr($experience["date"][$index]) . "' />";
	$html[] = "<textarea class='form-control form-control-editor ignore-valid' id='_experience_note' name='_experience_note[]' rows='5' placeholder='" . __("Describe what you did on the project(s)", "noo") . "'>" . html_entity_decode($experience["note"][$index]) . "</textarea>";
	$html[] = "<label for='_experience_project_image' class='control-label'>" . __("Project Images", "noo") . "</label>";
	$html[] = "<div>";
	if (! isset ($experience["project_image"])) {
		$experience["project_image"] = '';
	}
	if (! isset ($experience["project_image"][$index])) {
		$experience["project_image"][$index] = '';
	}
	ob_start();
	subease_image_upload_form_field( "_experience_project_image", $experience["project_image"][$index], 2, true, $index);
	$html[] = ob_get_clean();
	$html[] = '</div>';
	$html[] = '</div> <!-- End of Fields Group -->';

	return trim(preg_replace('/\s+/', ' ', implode (' ', $html)));
}

// add_action( 'wp_ajax_nopriv_post_add_experience', 'post_add_experience' );
add_action( 'wp_ajax_post_add_experience', 'post_add_experience' );

function post_add_experience() {

	$experience = array();
	$enable_experience = Noo_Resume::get_setting('enable_experience', '1');
	if( $enable_experience ) {
		$experience['employer'] = '';
		$experience['job'] = '';
		$experience['date'] = '';
		$experience['note'] = '';
		$experience['project_image'] = '';
	}

	$offset =  isset($_POST['offset'])?$_POST['offset'] : '';

	if (isset($offset)) {
		if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {
			echo generate_experience_fields($experience, $offset);
		}
	}
	die();
}

?>
