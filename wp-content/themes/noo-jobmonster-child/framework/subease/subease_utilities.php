<?php
if (! function_exists ( 'my_print_r' )) {

	function my_print_r($x) {
		return str_replace ( PHP_EOL, '', print_r ( $x, TRUE ) );
	}
}

if (! function_exists ( 'my_dump' )) {

	function my_dump($x) {
		echo '<pre>' . print_r ( $x, TRUE ) . '</pre>';
	}
}

if (! function_exists ( 'print_html_comment' )) {

	function print_html_comment($x) {
		echo '<!-- ' . print_r ( $x, TRUE ) . ' -->';
	}
}

/**
 * Log message to /tmp/subease.log.
 * Will prepend file and line of file calling function. Will pretty print message if not a string.
 */
if (! function_exists ( 'logme' )) {

	function logme($message) {
		$bt = debug_backtrace ();
		$space = '';
		if (gettype ( $message ) == "array" || gettype ( $message ) == "object" || gettype ( $message ) == "resource" || gettype ( $message ) == "unknown type") {
			$space = "\n";
		}
		$new_message = $bt [0] ['file'] . ' line  ' . $bt [0] ['line'] . ': ' . $space . print_r ( $message, true );
		error_log ( $new_message, 3, '/tmp/subease.log' );
	}
}

if (! function_exists ( 'is_administrator' )) {

	/**
	 * Check if current user is a Wordpress Administrator
	 *
	 * @return boolean
	 */
	function is_administrator() {
		if (current_user_can ( 'manage_options' )) {
			/* A user with admin privileges */
			return true;
		} else {
			/* A user without admin privileges */
			return false;
		}
	}
}

if (! function_exists ( 'validateDate' )) {

	function validateDate($date) {
		$d = DateTime::createFromFormat ( 'm/d/Y', $date );
		return $d && $d->format ( 'm/d/Y' ) === $date;
	}
}
if (! function_exists ( 'format_salary' )) {

	function format_salary($rate, $currency = '') {
		$locale = '';
		switch ($currency) {
			default :
			case "USD" :
				$currency = "USD";
				$locale = 'en_US';
				break;
		}
		$fmt = new NumberFormatter ( $locale, NumberFormatter::CURRENCY );
		return number_format ( $fmt->parseCurrency ( $rate, $currency ), "2" );
	}
}

/**
 * Will delete all attachments associated with post
 *
 * @param
 *        	int post_id
 *        	Post ID.
 */
if (! function_exists ( 'subease_delete_post_media' )) {

	function subease_delete_post_media($post_id) {
		if (! isset ( $post_id )) {
			// Will die in case you run a function like this: delete_post_media($post_id); if you will remove this line - ALL ATTACHMENTS WHO HAS A PARENT WILL BE DELETED PERMANENTLY!
			return;
		} elseif ($post_id == 0) {
			// Will die in case you have 0 set. there's no page id called 0 :)
			return;
		} elseif (is_array ( $post_id )) {
			return; // Will die in case you place there an array of pages.
		} else {

			$attachments = get_posts ( array (
					'post_type' => 'attachment',
					'posts_per_page' => - 1,
					'post_status' => 'any',
					'post_parent' => $post_id
			) );

			foreach ( $attachments as $attachment ) {
				if (false === wp_delete_attachment ( $attachment->ID )) {
					error_log ( "Can not delete attachment (" . $attachment->ID . ")" );
				}
			}
		}
	}
}

if (! function_exists ( 'subease_media_attach_action' )) {

	/**
	 * Encapsulate logic for Attach/Detach actions
	 *
	 *
	 * @global wpdb $wpdb WordPress database abstraction object.
	 *
	 * @param int $parent_id
	 *        	Post ID.
	 * @param int $attachment_id
	 *        	Attachment ID. Can be single Id or Array of ids
	 * @param string $action
	 *        	Optional. Attach/detach action. Accepts 'attach' or 'detach'.
	 *        	Default 'attach'.
	 */
	function subease_media_attach_action($parent_id, $attachment_id, $action = 'attach') {
		global $wpdb;

		if (! $parent_id) {
			return;
		}
		if (! $attachment_id) {
			return;
		}

		// if (! current_user_can ( 'edit_post', $parent_id )) {
		// wp_die ( __ ( 'You are not allowed to edit this post.' ) );
		// }
		$ids = array ();
		foreach ( ( array ) $attachment_id as $att_id ) {
			$att_id = ( int ) $att_id;

			// if (! current_user_can ( 'edit_post', $att_id )) {
			// continue;
			// }

			$ids [] = $att_id;
		}

		if (! empty ( $ids )) {
			$ids_string = implode ( ',', $ids );
			if ('attach' === $action) {
				$result = $wpdb->query ( $wpdb->prepare ( "UPDATE $wpdb->posts SET post_parent = %d WHERE post_type = 'attachment' AND ID IN ( $ids_string )", $parent_id ) );
			} else {
				$result = $wpdb->query ( "UPDATE $wpdb->posts SET post_parent = 0 WHERE post_type = 'attachment' AND ID IN ( $ids_string )" );
			}

			foreach ( $ids as $att_id ) {
				clean_attachment_cache ( $att_id );
			}
		}

		if (! isset ( $result ) || $result == false) {
			error_log ( "Could not attach/detach image (" . implode ( " ", $ids ) . ") to post (" . $parent_id . ")" );
		}
	}
}
?>
