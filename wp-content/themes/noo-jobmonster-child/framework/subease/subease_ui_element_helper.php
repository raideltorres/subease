<?php
if (! class_exists('Subease_UI_Element_Helper')) :
	require_once dirname(__FILE__) . '/' . 'subease_elements.php';

	/**
	 * @author jay
	 *
	 */
	class Subease_UI_Element_Helper extends Subease_Elements
	{

		public static function init() {}

		public static function display_state_select($select_name, $selected_state) {
			if (empty($selected_state)) {
				$selected_state = '';
			}

			echo '<select name="' . $select_name . '" class="form-control" id="' . $select_name . '">';
			echo '<option ';
			if ($selected_state == '') echo 'selected ';
			echo 'value="">';
			_e('Select a State', 'noo');
			echo '</option>';

			foreach (self::$states_lookup as $state_id => $state_name) {
				echo '<option ';
				if ($selected_state == $state_id) {
					echo 'selected ';
				}
				echo 'value="' . $state_id . '">';
				echo _e($state_name, 'noo');
				echo '</option>';
			}
			echo '</select>';
		}

		public static function display_phone_type_select($select_name, $selected_phone_type) {
			if (empty($selected_phone_type)) {
				$selected_phone_type = '';
			}
			echo '<select name="' . $select_name . '" class="form-control" id="' . $select_name . '">';
			echo '<option ';
			if ($selected_phone_type == '') echo 'selected ';
			echo 'value="">';
			_e('Select a Phone Type', 'noo');
			echo '</option>';

			foreach (self::$phone_type_lookup as $phone_type_id => $phone_type_name) {
				echo '<option ';
				if ($selected_phone_type == $phone_type_id) {
					echo ' selected ';
				}
				echo 'value="' . $phone_type_id . '">';
				echo _e($phone_type_name, 'noo');
				echo '</option>';
			}
			echo '</select>';
		}


		/**
		 *
		 * Displays a Select
		 *
		 * @param UI Select Type $select_type UI-Select-Type to process
		 * @param string $select_name Name of the select
		 * @param string $selected Value of the select if one is available
		 * @param string $do_label Boolean that will show a Label as first select option
		 * @param string $label Value of Label
		 */
		public static function display_select($select_type, $select_name, $selected, $do_label = false, $label = '', $class = '', $options = '') {
			if (empty($selected)) {
				$selected = '';
			}
			echo '<select name="' . $select_name . '" class="form-control ' . $class .'" id="' . $select_name . '" '. $options .'>';
			if ($do_label == true) {
				echo '<option ';
				if ($selected == '') {
					echo 'selected ';
				}
				echo 'value="">';
				echo _e($label, 'noo');
				echo '</option>';
			}

			foreach ($select_type as $key => $value) {
				echo '<option ';
				if ($selected == $key) {
					echo ' selected ';
				}
				echo 'value="' . $key . '">';
				echo _e($value, 'noo');
				echo '</option>';
			}
			echo '</select>';
		}

		public static function display_hide_profile_radio($radio_name, $radio_value) {
			if (empty($radio_value)) {
				$radio_value = '';
			}

			$tabindex = 1;

			foreach (self::$hide_profile_lookup as $hide_profile_key => $hide_profile_value) {
				echo '<input type="radio" name="' . $radio_name . '" value="' . $hide_profile_key . '" tabindex="' . $tabindex . '" class="" id="' . $radio_name . '" ';
				if ($radio_value == $hide_profile_key) {
					echo 'checked="checked"';
				}
				echo '> ';
				esc_attr_e($hide_profile_value, 'noo');
				echo '<br/>';
				$tabindex ++;
			}
		}

		public static function get_worker_status_html ($selected) {
			if (empty($selected)) {
				// Set default if nothing passed in
				$selected = self::WORKER_STATUS_ACTIVE;
			}
			return '<span class="worker_status_' . $selected . '">' . __(self::$worker_status_lookup[$selected], 'noo') .'</span>';
		}

		public static function display_worker_status_radio($radio_name, $radio_value) {
			if (empty($radio_value)) {
				// Set default if nothing passed in
				$radio_value = self::WORKER_STATUS_ACTIVE;
			}

			$tabindex = 1;

			foreach (self::$worker_status_lookup as $key => $value) {
				echo '<label class="worker_status_'. $key. '" id="' . $radio_name . '">';
				echo '<input type="radio" name="' . $radio_name . '" value="' . $key . '" tabindex="' . $tabindex . '" class="" id="' . $radio_name . '" ';
				if ($radio_value == $key) {
					echo 'checked="checked"';
				}
				echo '> ';
				echo __($value, 'noo') . '</label>';
				$tabindex ++;
			}
		}

		public static function display_allow_sms_radio($element_name, $default_element_value) {
			if (empty($default_element_value)) {
				$default_element_value = '';
			}

			$tabindex = 1;

			foreach (self::$allow_sms_lookup as $element_key => $element_value) {
				echo '<input type="radio" name="' . $element_name . '" value="' . $element_key . '" tabindex="' . $tabindex . '" class="" id="' . $element_name . '" ';
				if ($default_element_value == $element_key) {
					echo 'checked="checked"';
				}
				echo '> ';
				esc_attr_e($element_value, 'noo');
				echo '<br/>';
				$tabindex ++;
			}
		}
	}

	Subease_UI_Element_Helper::init();


endif;
