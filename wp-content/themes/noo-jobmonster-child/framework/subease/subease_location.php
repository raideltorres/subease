<?php

/**
 * * CALCULATE DISTANCE BETWEEN TWO POINTS OF LATITUDE/LONGITUDE **
 */
function geo_distance($lat1, $lon1, $lat2, $lon2) {
    $theta = $lon1 - $lon2;
    $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
    $dist = acos($dist);
    $dist = rad2deg($dist);
    $miles = $dist * 60 * 1.1515;
    return $miles;
}

if (! function_exists('store_job_location_latlon')) {

    function store_job_location_latlon($location_id, $latitude, $longitude) {
        // Check values in array
        if ( (is_numeric ($location_id) == false) || (is_numeric($latitude) == false) || (is_numeric($longitude) == false)){
            error_log('Argument passed in not numeric.');
            return false;
        }

        global $wpdb;

        // Check if we have a lat/lng stored for this property already
        $check_link = $wpdb->get_row("SELECT * FROM 9ad_subease_job_location_latlon WHERE location_id = '" . $location_id . "'");
        if ($check_link != null) {
            // We already have a lat lng for this post. Update row
            $rows = $wpdb->update(
                '9ad_subease_job_location_latlon',
                array(
                    'lat' => $latitude,
                    'lng' => $longitude
                ),
                array( 'location_id' => $location_id ),
                array(
                    '%f',
                    '%f'
                )
                );
        } else {
            // We do not already have a lat lng for this post. Insert row
            $rows = $wpdb->insert(
                '9ad_subease_job_location_latlon',
                array(
                    'location_id' => $location_id,
                    'lat' => $latitude,
                    'lng' => $longitude
                ),
                array(
                    '%d',
                    '%f',
                    '%f'
                )
           );
        }

        if (($rows == 0) || ($rows == false)) {
            error_log ("Could not update/insert Job Location");
            return false;
        } else {
            return true;
        }

    }
}


// Return Lat/Lon of Job Location
if (! function_exists('get_job_location_latlon')) {

    function get_job_location_latlon($location_id) {
        // Check values in array
        if ( (is_numeric ($location_id) == false) ){
            error_log('Argument passed in not numeric.');
            return false;
        }

        global $wpdb;
        $location_arr = array();
        // Check if we have a lat/lng stored for this property already
        $check_link = $wpdb->get_row("SELECT * FROM 9ad_subease_job_location_latlon WHERE location_id = '" . $location_id . "'");
        if ($check_link != null) {
            // Found match
            $location_arr['lat'] = $check_link->lat;
            $location_arr['lon'] = $check_link->lng;
        } else {
            // Now try and get it and add it.
            // $locations_tax			= get_the_terms( $location_id, 'job_location' );
            $locations_tax = get_term_by( 'id', sanitize_title( $location_id ), 'job_location' );

            $new_latlon = geocode_address($locations_tax->name);
            if (is_array($new_latlon)) {
                if (store_job_location_latlon($location_id, $new_latlon['lat'], $new_latlon['lon'])) {
                    $location_arr['lat'] = $new_latlon['lat'];
                    $location_arr['lon'] = $new_latlon['lon'];
                } else {
                    error_log ("Could not store Job Location: {$location_id}");
                    return false;
                }
            } else {
                error_log ("Could not find Job Location: {$location_id}");
                return false;
            }
        }

        return $location_arr;
    }
}


// function to geocode address, it will return false if unable to geocode address
if (! function_exists('geocode_address')) {

    function geocode_address($address) {
        // url encode the address
        $address = urlencode($address);

        // google map geocode api url
        $url = "https://maps.google.com/maps/api/geocode/json?address={$address}" . '&sensor=false&key=' . GOOGLE_MAPS_API_KEY;

        // get the json response
        $resp_json = file_get_contents($url);

        // decode the json
        $resp = json_decode($resp_json, true);

        // response status will be 'OK', if able to geocode given address
        if ($resp['status'] == 'OK') {

            // get the important data
            $lati = $resp['results'][0]['geometry']['location']['lat'];
            $longi = $resp['results'][0]['geometry']['location']['lng'];
            $formatted_address = $resp['results'][0]['formatted_address'];

            // verify if data is complete
            if ($lati && $longi && $formatted_address) {

                // put the data in the array
                $data_arr = array(
                    'lat' => $lati,
                    'lon' => $longi,
                    'address' => $formatted_address);

                // array_push($data_arr, $lati, $longi, $formatted_address);

                return $data_arr;
            } else {
                error_log("There was a problem with the data returned, something is missing: lati={$lati} longi={$longi} address={$formatted_address}");
                return false;
            }
        } else {
            error_log("Google API Error: " . $resp['status']);
            return false;
        }
    }
}

if (! function_exists('get_resume_latlon')) {

    function get_user_latlon($userId) {
        if (!empty( $userId )) {
            $resume_latlon = get_the_author_meta( 'latlon', $userId);
        } else {
            error_log ("Not a valid User Id");
            return false;
        }
    }
}


/* This is old and yucky, do not use this one. */
if (! function_exists('get_latlon_from_address')) {

    function get_latlon_from_address($address = '') {
        if (isset($address) && ! empty($address)) {
            // Get user location from address
            $geocode = file_get_contents('https://maps.google.com/maps/api/geocode/json?address=' . $address . '&sensor=false&key=' . GOOGLE_MAPS_API_KEY);
            $output = json_decode($geocode);
            $lat = $output->results[0]->geometry->location->lat;
            $lng = $output->results[0]->geometry->location->lng;
        }
    }
}

function add_new_job_location($new_location = '') {
	if (! empty($new_location)) {
		$result = array();
		if (($t = get_term_by('slug', sanitize_title($new_location), 'job_location'))) {
			$result['success'] = true;
			$result['location_value'] = $t->slug;
			$result['location_title'] = $t->name;
			$result['term_id'] = (string) $t->term_id;
		} else {
			$n_l = wp_insert_term($new_location, 'job_location');
			if ($n_l && ! is_wp_error($n_l) && ($loca = get_term(absint($n_l['term_id']), 'job_location'))) {
				$result['success'] = true;
				$result['location_value'] = $loca->slug;
				$result['location_title'] = $loca->name;
				$result['term_id'] = (string) $loca->term_id;
			}
		}

		// Since we created a job location, lets store it's lat/lon
		$latlon = get_job_location_latlon($result['term_id']);
		if (isset($latlon) && (is_array($latlon))) {
			$result = array_merge($result, $latlon);
		}


	}
	return $result;
}

function get_candidate_address_as_job_location ($candidate_id = '') {

		// Get the workers profile location
		if ( (isset($candidate_id)) && ($candidate_id !== '')) {
			$candidate = !empty($candidate_id) ? get_userdata($candidate_id) : false;
			if ($candidate) {
				$candidate_address = get_the_author_meta( 'address', $candidate->ID);
				if ($candidate_address !== '') { // some candidates do not have address yet.
					$job_location = subease_search_job_location($candidate_address, true);


					// Grab all the Location Ids and put into array
					$jl_array = array();
					foreach ($job_location as $jl) {
						$jl_array[] = $jl['term_id'];
					}
					$value = json_encode($jl_array);
					return $value;
				}
			}
		} else {
			return false;
		}

}

?>
