<?php
if (! class_exists('Subease_Skills')) :

	class Subease_Skills
	{

		public $skill_id = 0;
/*
 *
Budgeting
Building Codes
Building Housing Additions
Building House Foundations
Cabinet Building
Cutting
Finishing
Framing
Furniture Making
Insulation
Nailing
Paneling
Refinishing
Remodeling
Renovation
Rigging
Rough to Finish
Sawing
Trimming

 */
		public static $skills = array(
			array(
				"parent" => 0,
				"category_id" => 46,
				"skill_name" => "Alternative Energy",
				"skill_id" => ""
			),
			array(
				"parent" => 0,
				"category_id" => 47,
				"skill_name" => "Carpentry",
				"skill_id" => ""
			),
			array(
				"parent" => 47,
				"category_id" => 75,
				"skill_name" => "Cabinetmaker",
				"skill_id" => ""
			),
			array(
				"parent" => 47,
				"category_id" => 76,
				"skill_name" => "Carpentry Layout",
				"skill_id" => ""
			),
			array(
				"parent" => 47,
				"category_id" => 77,
				"skill_name" => "Carpentry Repair and Maintenance",
				"skill_id" => ""
			),
			array(
				"parent" => 47,
				"category_id" => 78,
				"skill_name" => "Carpentry Restoration",
				"skill_id" => ""
			),
			array(
				"parent" => 47,
				"category_id" => 79,
				"skill_name" => "Finish Carpenter",
				"skill_id" => ""
			),
			array(
				"parent" => 47,
				"category_id" => 80,
				"skill_name" => "Framing Carpenter",
				"skill_id" => ""
			),
			array(
				"parent" => 47,
				"category_id" => 81,
				"skill_name" => "Furniture Maker",
				"skill_id" => ""
			),
			array(
				"parent" => 47,
				"category_id" => 82,
				"skill_name" => "Historic Carpentry",
				"skill_id" => ""
			),
			array(
				"parent" => 47,
				"category_id" => 83,
				"skill_name" => "Millworker",
				"skill_id" => ""
			),
			array(
				"parent" => 47,
				"category_id" => 84,
				"skill_name" => "Rough Carpenter",
				"skill_id" => ""
			),
			array(
				"parent" => 47,
				"category_id" => 85,
				"skill_name" => "Trim Carpenter",
				"skill_id" => ""
			),
			array(
				"parent" => 47,
				"category_id" => 86,
				"skill_name" => "Woodworker",
				"skill_id" => ""
			),
			array(
				"parent" => 0,
				"category_id" => 48,
				"skill_name" => "Ceilings and Walls",
				"skill_id" => ""
			),
			array(
				"parent" => 0,
				"category_id" => 49,
				"skill_name" => "Cleaner",
				"skill_id" => ""
			),
			array(
				"parent" => 0,
				"category_id" => 50,
				"skill_name" => "Commercial Diver",
				"skill_id" => ""
			),
			array(
				"parent" => 0,
				"category_id" => 51,
				"skill_name" => "Concrete",
				"skill_id" => ""
			),
			array(
				"parent" => 0,
				"category_id" => 52,
				"skill_name" => "Construction Manager",
				"skill_id" => ""
			),
			array(
				"parent" => 0,
				"category_id" => 53,
				"skill_name" => "Conveying Systems",
				"skill_id" => ""
			),
			array(
				"parent" => 0,
				"category_id" => 54,
				"skill_name" => "Demolition and Abatement",
				"skill_id" => ""
			),
			array(
				"parent" => 0,
				"category_id" => 55,
				"skill_name" => "Electrical",
				"skill_id" => ""
			),
			array(
				"parent" => 0,
				"category_id" => 56,
				"skill_name" => "Equipment Operation",
				"skill_id" => ""
			),
			array(
				"parent" => 0,
				"category_id" => 57,
				"skill_name" => "Estimator",
				"skill_id" => ""
			),
			array(
				"parent" => 0,
				"category_id" => 58,
				"skill_name" => "Fence Erecting",
				"skill_id" => ""
			),
			array(
				"parent" => 0,
				"category_id" => 59,
				"skill_name" => "Flooring",
				"skill_id" => ""
			),
			array(
				"parent" => 0,
				"category_id" => 60,
				"skill_name" => "General labor",
				"skill_id" => ""
			),
			array(
				"parent" => 0,
				"category_id" => 61,
				"skill_name" => "HVACR",
				"skill_id" => ""
			),
			array(
				"parent" => 0,
				"category_id" => 62,
				"skill_name" => "Insulation",
				"skill_id" => ""
			),
			array(
				"parent" => 0,
				"category_id" => 63,
				"skill_name" => "Landscaping and Groundskeeping",
				"skill_id" => ""
			),
			array(
				"parent" => 0,
				"category_id" => 64,
				"skill_name" => "Masonry",
				"skill_id" => ""
			),
			array(
				"parent" => 0,
				"category_id" => 65,
				"skill_name" => "Metal Worker",
				"skill_id" => ""
			),
			array(
				"parent" => 0,
				"category_id" => 66,
				"skill_name" => "Painting and Paperhanging",
				"skill_id" => ""
			),
			array(
				"parent" => 0,
				"category_id" => 67,
				"skill_name" => "Plumbing",
				"skill_id" => ""
			),
			array(
				"parent" => 0,
				"category_id" => 68,
				"skill_name" => "Remover of hazardous materials",
				"skill_id" => ""
			),
			array(
				"parent" => 0,
				"category_id" => 69,
				"skill_name" => "Roofing and Siding",
				"skill_id" => ""
			),
			array(
				"parent" => 0,
				"category_id" => 70,
				"skill_name" => "Scaffold Erecting",
				"skill_id" => ""
			),
			array(
				"parent" => 0,
				"category_id" => 71,
				"skill_name" => "Thermal and Moisture Protection",
				"skill_id" => ""
			),
			array(
				"parent" => 0,
				"category_id" => 72,
				"skill_name" => "Transportation",
				"skill_id" => ""
			),
			array(
				"parent" => 0,
				"category_id" => 73,
				"skill_name" => "Welder",
				"skill_id" => ""
			),
			array(
				"parent" => 0,
				"category_id" => 74,
				"skill_name" => "Windows and Doors",
				"skill_id" => ""
			)
		)
		;

		public function __construct() {
			echo 'The class "', __CLASS__, '" was initiated!<br />';
		}

		public function __destruct() {
			echo 'The class "', __CLASS__, '" was destroyed.<br />';
		}

		public function __toString() {
			echo "Using the toString method: ";
			return $this->getProperty();
		}

		public function setProperty($newval) {
			$this->prop1 = $newval;
		}

		private function getProperty() {
			return $this->prop1 . "<br />";
		}

		public static function plusOne() {
			return "The count is " . ++ self::$count . ".<br />";
		}
	} // class definition
	Subease_Skills::init();

endif;
