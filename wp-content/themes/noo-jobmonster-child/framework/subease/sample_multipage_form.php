<?php
// ALl the code below will be moved to their own files, I am just using this space
// now to speed up my development.  Jay
add_shortcode('multipage_form_sc','multipage_form');
function multipage_form(){
	global $wpdb;
	$this_page = $_SERVER['REQUEST_URI'];
	$page = $_POST['page'];
	if ( $page == NULL ) {
		echo '

<form action="' . $this_page .'" method="post"><label id="first_name" for="first_name">First Name: </label>
<input id="first_name" type="text" name="first_name" />
<label id="last_name" for="last_name">Last Name: </label>
<input id="last_name" type="text" name="last_name" />
<label id="email" for="email">Email: </label>
<input id="email" type="text" name="email" />
<label id="phone" for="phone">Phone: </label>
<input id="phone" type="text" name="phone" />
<label id="first_name" for="first_name">Zip Code: </label>
<input id="zip_code" type="text" name="zip_code" />
<input type="hidden" name="page" value="1" />
<input type="submit" /></form>';
	}//End Page 1 of Form
	//	Start Page 2 of Form
	elseif ( $page == 1 ) {
		//	Grab the POST data that the user provided
		$first_name	=	$_POST['first_name'];
		$last_name	=	$_POST['last_name'];
		$email	=	$_POST['email'];
		$phone	=	$_POST['phone'];
		$zip_code	=	$_POST['zip_code'];
		//	Assign the table and inputs for our upcoming INSERT function
		$page_one_table = 'subeasec_9ad.shopping_preferences';
		$page_one_inputs = array(
			'first_name' => $first_name,
			'last_name' => $last_name,
			'email' => $email,
			'phone' => $phone,
			'zip_code' => $zip_code,
			'page' => $page
		);
		//	Insert the data into a new row
		$insert_page_one	=	$wpdb->insert($page_one_table, $page_one_inputs);
		echo '<pre>' . print_r($insert_page_one, true) . '</pre>';

		//	Grab the ID of the row we inserted for later use
		$form_id = $wpdb->insert_id;
		echo '<h3>You made it to the 2nd page!</h3>
Here are your form inputs:
First Name: ' . $first_name . '
Last Name: ' . $last_name . '
Email: ' . $email . '
Phone: ' . $phone . '
Zip Code: ' . $zip_code . '
Form ID: ' . $form_id .'';
		echo '<form method="post" action="' . $this_page .'">
<label for="gender" id="gender">Gender: </label>
<select name="gender" />
<option value="nothing" selected>Select Gender</option>
<option value="0">Female</option>
<option value="1">Male</option>
</select>
<label for="age" id="age">Age: </label>
<input type="text" name="age" id="age" />
<label for="education" id="education">Education: </label>
<select name="education" />
<option value="nothing" selected>Select Level of Education</option>
<option value="some_high_school">Some High School</option>
<option value="all_high_school">High School Diploma/GED</option>
<option value="some_college">Some College</option>
<option value="all_colleg">College Degree</option>
<option value="some_grad">Some Graduate School</option>
<option value="all_grad">Graduate</option>
<option value="some_doc">Some Post Graduate</option>
<option value="all_doc">Doctorate</option>
</select>
<label for="income" id="income">Income: </label>
<select name="income" />
<option value="nothing" selected>Select Income Range</option>
<option value="10000" selected>Less than $10,000</option>
<option value="25000" selected>$10,000 - $25,000</option>
<option value="50000" selected>$25,000 - $50,000</option>
<option value="75000" selected>$50,000 - $75,000</option>
<option value="max" selected>More than $75,000</option>
</select>
<input type="hidden" value="2" name="page" />
<input type="hidden" value="' . $form_id . '" name="form_id" />
<input type="submit" />
</form>';
	} //End Page 2 of Form
	// Start Page 3 of Form
	elseif( $page == 2 ) {

		$gender = $_POST['gender'];
		$age = $_POST['age'];
		$education = $_POST['education'];
		$income = $_POST['income'];
		$page = $_POST['page'];
		$form_id = $_POST['form_id'];

		$page_two_table = 'subeasec_9ad.shopping_preferences';
		$page_two_inputs = array(
			'gender' => $gender,
			'age' => $age,
			'education' => $education,
			'income' => $income,
			'page' => $page
		);
		$page_two_where = array(
			'id' => $form_id
		);

		$insert_page_two = $wpdb->update($page_two_table, $page_two_inputs, $page_two_where);

		echo '<form method="post" action="' . $this_page .'">
<label for="location" id="location">How do you like to shop?</label>
<select name="location" />
<option value="nothing" selected>Pick Your Favorite</option>
<option value="ebay">Online � Ebay</option>
<option value="eretailer">Online � Retailers</option>
<option value="classifieds">Online � Classifieds</option>
<option value="store">Physical Store</option>
</select>
';

		if ( $gender == "nothing" ) {
			echo
			'<label for="category" id="category">What do you shop for most?</label>
<select name="category" />
<option value="nothing" selected>Pick Your Favorite</option>
<option value="ebay">Clothes</option>
<option value="eretailer">Shoes</option>
<option value="classifieds">Jewelry</option>
<option value="classifieds">Cooking Equipment</option>
<option value="store">Sports Gear</option>
<option value="classifieds">Computers � Desktop</option>
<option value="classifieds">Computers � Laptop</option>
<option value="classifieds">Computers � Software</option>
</select>';
		}
		if ( $gender == 0 ) {
			echo
			'<label for="category" id="category">What do you shop for most?</label>
<select name="category" />
<option value="nothing" selected>Pick Your Favorite</option>
<option value="store">Sports Gear</option>
<option value="classifieds">Computers � Desktop</option>
<option value="classifieds">Computers � Laptop</option>
<option value="classifieds">Computers � Software</option>
</select>';
		}
		if ( $gender == 1 ) {
			echo
			'<label for="category" id="category">What do you shop for most?</label>
<select name="category" />
<option value="nothing" selected>Pick Your Favorite</option>
<option value="store">Sports Gear</option>
<option value="classifieds">Computers � Desktop</option>
<option value="classifieds">Computers � Laptop</option>
<option value="classifieds">Computers � Software</option>
</select>';
		}

		echo '
<input type="hidden" value="3" name="page" />
<input type="hidden" value="' . $form_id . '" name="form_id" />

<input type="submit" />
</form>';

	};// End Page 3 of Form

	// Let's check our data
	$data_check = $wpdb->get_row("SELECT * FROM subeasec_9ad.shopping_preferences WHERE id = '$form_id'");

	echo '
<p> id: ' . $data_check->id . '</p>
<p> first_name: ' . $data_check->first_name . '</p>
<p> last_name: ' . $data_check->last_name . '</p>
<p> email: ' . $data_check->email . '</p>
<p> phone: ' . $data_check->phone . '</p>
<p> zip_code: ' . $data_check->zip_code . '</p>
<p> gender: ' . $data_check->gender . '</p>
<p> age: ' . $data_check->age . '</p>
<p> education: ' . $data_check->education . '</p>
<p> income: ' . $data_check->income . '</p>
<p> location: ' . $data_check->location . '</p>
<p> categories: ' . $data_check->categories . '</p>
<p> page: ' . $data_check->page . '</p>
<p> timestamp: ' . $data_check->timestamp . '</p>';
}

?>