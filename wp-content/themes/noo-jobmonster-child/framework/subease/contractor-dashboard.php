<?php
if (! function_exists('application_listing')) {

	function application_listing() {
		if (! isset($job_ids)) $job_ids = array();
		global $wpdb;




		// start applications list, todo: move to helper file

		if( is_front_page() || is_home()) {
			$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : ( ( get_query_var( 'page' ) ) ? get_query_var( 'page' ) : 1 );
		} else {
			$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
		}
		$job_filter = isset($_POST['job']) ? absint($_POST['job']) : 0;
		$jobs_list = get_posts(array(
			'post_type'=>'noo_job',
			'post_status'=>array('publish','pending','expired'),
			'author'=>get_current_user_id(),
			'posts_per_page'=>-1,
			'suppress_filters' => false
		));
		$job_ids = array_map(create_function('$o', 'return $o->ID;'), $jobs_list);
		$args = array(
			'post_type'=>'noo_application',
			'paged' => $paged,
			'post_parent__in' => array_merge(isset($job_ids) ? $job_ids : array(), array(0)), // make sure return zero application if there's no job.

			// 'post_parent__in'=>array_merge($job_ids, array(0)), // make sure return zero application if there's no job.
			'post_status'=>array('publish','pending','rejected'),
		);
		if(!empty($job_filter) && in_array($job_filter, $job_ids)){
			$args['post_parent__in'] = array($job_filter);
		}
		$applicationsResult = new WP_Query($args);

		// end applications list



		$output = '<style>.full_width{width:100%;float:left;}</style><div><h3>Applications</h3></div>';
		if ($applicationsResult->have_posts()) :
			while ($applicationsResult->have_posts()) :
				$applicationsResult->the_post();
				global $post;
				$candidate_email = noo_get_post_meta($post->ID, '_candidate_email');

				$candidate_link = apply_filters('noo_application_candidate_link', '', $post->ID, $candidate_email);
				$output .= '<div class="full_width"><strong>Worker Name</strong>: <span class="candidate-name">';
				if (! empty($candidate_link)) echo '<a href="' . $candidate_link . '">';
				$output .= get_the_title();
				if (! empty($candidate_link)) echo '</a>';
				$output .= '</span></div>';

				$output .= '<div class="full_width"><strong>Job Name:</strong> ';
				$parent_job = get_post($post->post_parent);

				if ($parent_job && $parent_job->post_type === 'noo_job') {
					$output .= '<a href="' . get_permalink($parent_job->ID) . '">' . $parent_job->post_title . '</a>';
				}

				elseif ($parent_job = noo_get_post_meta($post->ID, '_job_applied_for', true)) {
					$output .= esc_html($parent_job);
				} else {
					$output .= '<span class="na">&ndash;</span>';
				}

				$output .= '</div>';
				$output .= '<div class="full_width"><strong>Applied On</strong>: <span><i class="fa fa-calendar"></i> <em>' . date_i18n(get_option('date_format'), strtotime($post->post_date)) . '</em></span></div>';
			endwhile
			;
		 else :
			$output .= '<p class="no-plan-package text-center">No Application</p>';
		endif;
		return $output;
	}
}
add_shortcode('application_listing', 'application_listing');

if (! function_exists('profile_completeness')) {

	function profile_completeness() {
		$company_id = isset($_GET['company_id']) ? absint($_GET['company_id']) : 0;
		$company = get_post($company_id);
		$user_ID = get_current_user_id();
		$field_array = array(
			$company->post_title,
			noo_get_post_meta($company_id, '_website'),
			$company->post_content,
			noo_get_post_meta($company_id, '_logo'),
			noo_get_post_meta($company_id, '_cover_image
'),
			noo_get_post_meta($company_id, '_googleplus'),
			noo_get_post_meta($company_id, '_facebook'),
			noo_get_post_meta($company_id, '_linkedin'),
			noo_get_post_meta($company_id, '_twitter'),
			noo_get_post_meta($company_id, '_instagram'),
			noo_get_post_meta($company_id, '_phone')
		);
		$total_fields = count($field_array);
		$filled_fields = count(array_filter($field_array));
		$percentage = ($filled_fields / $total_fields) * 100;
		$percentage = round($percentage);
		$output = '<div><h3>Profile Completeness</h3></div><div  style="text-indent:' . ($percentage - 10) . '%">' . $percentage . '%</div>
                           <div class="progress"> <div class="progress-bar" role="progressbar" aria-valuenow="' . $percentage . '" aria-valuemin="0" aria-valuemax="100" style="width:' . $percentage . '%">
                                        <span class="sr-only">' . $percentage . ' Complete</span>
                                </div>
                        </div>
                        <ul>
                                <li>Next Step: <a href="' . Noo_Member::get_company_profile_url() . '">fill in a thing!</a></li>
                        </ul>';
		return $output;
	}
}

add_shortcode('profile_completeness', 'profile_completeness');

add_filter('widget_text', 'do_shortcode', 11);

if (! function_exists('job_listing')) {
	function job_listing() {
		$args = array(
			'post_type' => 'noo_job',
			'author' => get_current_user_ID(),
			'post_status' => 'publish',
			'posts_per_page' => 3
		);
		$r = new WP_Query($args);
		// $args['post_status'] = array();
		// $args['post_status'] = array('publish','pending','pending_payment','expired','inactive');
		// $r = jm_user_job_query();
		$jobs_list = get_posts(array(
			'post_type' => 'noo_job',
			'post_status' => array(
				'publish',
				'pending',
				'expired'
			),
			'author' => get_current_user_id(),
			'posts_per_page' => 3,
			'suppress_filters' => false
		));
		$job_ids = array_map(create_function('$o', 'return $o->ID;'), $jobs_list);
		$args = array(
			'post_type' => 'noo_application',
			'paged' => 1,
			'post_parent__in' => array_merge($job_ids, array(
				0
			)), // make sure return zero application if there's no job.
			'post_status' => array(
				'publish',
				'pending',
				'rejected'
			),
			'posts_per_page' => 3
		);
		$applicationsResult = new WP_Query($args);
		$job_need_approve = jm_get_job_setting('job_approve', '') == 'yes';
		$output = '<div><h3>Job Listings</h3></div><div class="member-manage HEREIAM">';

		if ($r->have_posts()) :
			$output .= '<div style="display: none">' . wp_nonce_field("job-manage-action") . '</div>';
			$output .= '<div class="member-manage-table">';
			while ($r->have_posts()) :
				$r->the_post();
				global $post;
				$status = $status_class = jm_correct_job_status($post->ID, $post->post_status);
				$statuses = jm_get_job_status();
				$status_text = '';

				if (isset($statuses[$status])) {
					$status_text = $statuses[$status];
				} else {
					$status_text = __('Inactive', 'noo');
					$status_class = 'inactive';
				}

				$output .= '<div class="floatwidth headingbottomborder paddingtop-bottom">
                                                        <style>
                                                        .headingbottomborder{
                                                                border-bottom: 1px solid #ddd;
                                                                }
                                                        .floatwidth{
                                                                float:left;}
                                                                .width{
                                                                        width:100%;
                                                                        }
                                                        .paddingtop-bottom{
                                                                padding-top:10px;
                                                                padding-bottom:10px;
                                                                }
                                                        </style>';
				$output .= '<div claas="width"><strong>Job Name:</strong>';

				if ($status == 'pending' || 'pending_payment') :
					$output .= '<a href="' . esc_url(add_query_arg("job_id", get_the_ID(), Noo_Member::get_endpoint_url("edit-job"))) . '">' . get_the_title() . '</a>';
				 else :
					$output .= '<a href="' . the_permalink() . '">' . get_the_title() . '</a>';
				endif;
				$output .= '</div><div class="width"><strong>Project Name: </strong></div><div class="floatwidth width"><div class="hidden-xs hidden-sm "><strong>Location:</strong> <i class="fa fa-map-marker"></i>&nbsp;<em>' . get_the_term_list(get_the_ID(), "job_location", "", ", ") . '</em></div></div><div class="floatwidth width"><strong>Number of Application(s):</strong> ';
				$applications = get_posts(array(
					'post_type' => 'noo_application',
					'posts_per_page' => 3,
					'post_parent' => $post->ID,
					'post_status' => array(
						'publish',
						'pending',
						'rejected'
					),
					'suppress_filters' => false
				));
				$output .= absint(count($applications));
				$output .= '</div></div>';
			endwhile
			;
			$output .= '</div>
                        <div class="member-manage-toolbar bottom-toolbar clearfix">
                        </div>';
		 else :
			$output .= '<p class="no-plan-package text-center">"You have no job, why dont you start posting one. TESTING"</p>
                <p>
                        <a href="' . Noo_Member::get_post_job_url() . '" class="btn btn-primary">Post Job</a>
                </p>';
		endif;
		$output .= '</div>';
		return $output;
	}


}

add_shortcode('job_listing', 'job_listing');

if (! function_exists('Plan_Summary')) {

	function Plan_Summary() {
		$output = '<style>
                .fleft{float:left;}
                .fright{float:right;}
                .my-button a{
                            display: inline-block;
                                margin-bottom: 0;
                                text-align: center;
                                cursor: pointer;
                                background-image: none;
                                border: 1px solid transparent;
                                white-space: nowrap;
                                font-family: "Montserrat", sans-serif;
                                padding: 0.71428571em 2.28571429em;
                                font-size: 14px;
                                border-radius: 4px;
                                -webkit-user-select: none;
                                -moz-user-select: none;
                                -ms-user-select: none;
                                user-select: none;
                                -webkit-transition: border-color color 0.2s ease;
                                -o-transition: border-color color 0.2s ease;
                                transition: border-color color 0.2s ease;
                                color: #44494b;
                                background-color: #e6b706;
                                border-color: transparent;
                                line-height: 1.3 !important;
                                vertical-align: inherit;
                                margin-top:10px;
                                font-weight: bold;}
                </style><div><h3>Plan Summary</h3></div>';
		$package = array();
		$package_page_id = '';
		if (Noo_Member::is_employer()) {
			$package = jm_get_job_posting_info();
			$package_page_id = Noo_Job_Package::get_setting('package_page_id');
		} elseif (Noo_Member::is_candidate()) {
			$package = jm_get_resume_posting_info();
			$package_page_id = Noo_Resume_Package::get_setting('resume_package_page_id');
		}
		$job_added = jm_get_job_posting_added();
		$feature_job_remain = jm_get_feature_job_remain();
		$output .= '<div class="row">';
		if (empty($package)) :
			$output .= '<p class="no-plan-package text-center">No Package</p><div class="member-plan-choose"><a class="btn btn-lg btn-primary" href="' . esc_url(get_permalink($package_page_id)) . '
">Choose a Package</a></div>';
		 else :
			$output .= '<div class="row">';
			if (Noo_Member::is_employer()) :
				$output .= '<div class="col-xs-12 col-md-6"><strong>Job Limit:</strong></div><div class="col-xs-12 col-md-6">' . sprintf(__("%s job(s)", "noo"), $package["job_limit"] == 99999999 ? __("Unlimited", "noo") : $package["job_limit"]) . '</div></div><div class="row">
                                <div class="col-xs-12 col-md-6"><strong>Job Added: </strong></div>
                                <div class="col-xs-12 col-md-6">' . sprintf(__("%s job(s)", "noo"), jm_get_job_posting_added()) . '</div>';


            endif;
			$output .= '</div><div class="row">';
			$total_jobs = sprintf(__('%s ', 'noo'), $package['job_limit'] == 99999999 ? __('Unlimited', 'noo') : $package['job_limit']);
			$used_jobs = sprintf(__('%s ', 'noo'), jm_get_job_posting_added());
			$usedjobpercentage = round(($used_jobs / $total_jobs) * 100);
			$usedjobs = $total_jobs - $used_jobs;
			$output .= '<strong class="col-xs-12  col-md-6">Unusedjobs: </strong><span class=" col-md-6">' . $usedjobs . ' job(s)</span><br />';
			$output .= '<strong class="col-xs-12  col-md-12">UsedJob(%): </strong><br />';
			$usedjobpercentage = round($usedjobpercentage);
			$output .= '<div  style="text-indent:' . ($usedjobpercentage - 10) . '%">' . $usedjobpercentage . '%</div>
                           <div class="progress"> <div class="progress-bar" role="progressbar" aria-valuenow="' . $usedjobpercentage . '" aria-valuemin="0" aria-valuemax="100" style="width:' . $usedjobpercentage . '%">
                                        <span class="sr-only">' . $usedjobpercentage . ' Complete</span>
                                </div>
                        </div>';

			$unusedjobs = round(($usedjobs / $total_jobs) * 100);
			$output .= '<strong class="col-xs-12  col-md-12 ">Unusedjobs(%): </strong>';
			$unusedjobs = round($unusedjobs);
			$output .= '<div  style="text-indent:' . ($unusedjobs - 10) . '%">' . $unusedjobs . '%</div>
                           <div class="progress"> <div class="progress-bar" role="progressbar" aria-valuenow="' . $unusedjobs . '" aria-valuemin="0" aria-valuemax="100" style="width:' . $unusedjobs . '%">
                                        <span class="sr-only">' . $unusedjobs . ' Complete</span>
                                </div>
                        </div>';
			$output .= '</div>';
			$output .= '<div class="my-button"><a href="' . get_site_url() . '/member-2/manage-plan/">Manage Plan</a></div>';


		endif;
		$output .= '</div>';
		return $output;
	}
}
add_shortcode('plan_summary', 'Plan_Summary');

if (! function_exists('Job_search_statistics')) {

	function Job_search_statistics() {
		$r = jm_user_job_query();
		$output = '<div><h3>Job Search Statistics</h3></div>';
		if ($r->have_posts()) :

			while ($r->have_posts()) :
				$r->the_post();
				global $post;
				$status = $status_class = jm_correct_job_status($post->ID, $post->post_status);
				$statuses = jm_get_job_status();
				$status_text = '';
				if (isset($statuses[$status])) {
					$status_text = $statuses[$status];
				} else {
					$status_text = __('Inactive', 'noo');
					$status_class = 'inactive';
				}
				if ($status_text == 'Active') {
					$page_view = get_post_meta(get_the_ID(), '_noo_views_count', true);
					if ($status == 'pending' || 'pending_payment') :
						$output .= '<a href="' . esc_url(add_query_arg('job_id', get_the_ID(), Noo_Member::get_endpoint_url('preview-job'))) . '"><strong>' . get_the_title() . '</
strong></a>';
					 else :
						$output .= '<a href="' . get_permalink() . '"><strong>' . get_the_title() . '</strong></a>';
					endif;
					$output .= " : " . $page_view . "<br/>";
				}
			endwhile
			;
		 else :

			$output .= '<p class="no-plan-package text-center">You have no job, why don\'t you start posting one. TESTING</p><p>
                        <a href="' . Noo_Member::get_post_job_url() . '" class="btn btn-primary">Post Job</a>
                </p>';
		endif;

		return $output;
	}
}
add_shortcode('Job_search_statistics', 'Job_search_statistics');
?>
