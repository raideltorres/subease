<?php
if( !class_exists('Subease_Elements') ) :
  class Subease_Elements {

    public static $states_lookup = array(
      "Alabama" => "Alabama",
      "Alaska" => "Alaska",
      "Arizona" => "Arizona",
      "Arkansas" => "Arkansas",
      "California" => "California",
      "Colorado" => "Colorado",
      "Connecticut" => "Connecticut",
      "Delaware" => "Delaware",
      "District of Columbia" => "District of Columbia",
      "Florida" => "Florida",
      "Georgia" => "Georgia",
      "Hawaii" => "Hawaii",
      "Idaho" => "Idaho",
      "Illinois" => "Illinois",
      "Indiana" => "Indiana",
      "Iowa" => "Iowa",
      "Kansas" => "Kansas",
      "Kentucky" => "Kentucky",
      "Louisiana" => "Louisiana",
      "Maine" => "Maine",
      "Maryland" => "Maryland",
      "Massachusetts" => "Massachusetts",
      "Michigan" => "Michigan",
      "Minnesota" => "Minnesota",
      "Mississippi" => "Mississippi",
      "Missouri" => "Missouri",
      "Montana" => "Montana",
      "Nebraska" => "Nebraska",
      "Nevada" => "Nevada",
      "New Hampshire" => "New Hampshire",
      "New Jersey" => "New Jersey",
      "New Mexico" => "New Mexico",
      "New York" => "New York",
      "North Carolina" => "North Carolina",
      "North Dakota" => "North Dakota",
      "Ohio" => "Ohio",
      "Oklahoma" => "Oklahoma",
      "Oregon" => "Oregon",
      "Pennsylvania" => "Pennsylvania",
      "Rhode Island" => "Rhode Island",
      "South Carolina" => "South Carolina",
      "South Dakota" => "South Dakota",
      "Tennessee" => "Tennessee",
      "Texas" => "Texas",
      "Utah" => "Utah",
      "Vermont" => "Vermont",
      "Virginia" => "Virginia",
      "Washington" => "Washington",
      "West Virginia" => "West Virginia",
      "Wisconsin" => "Wisconsin",
      "Wyoming" => "Wyoming"
    );

    CONST PHONE_TYPE_MOBILE = '1';
    CONST PHONE_TYPE_HOME = '2';
    public static $phone_type_lookup = array(
      self::PHONE_TYPE_MOBILE => "Mobile",
      self::PHONE_TYPE_HOME => "Home"
    );

    CONST WORKER_STATUS_ACTIVE = 'active';
    CONST WORKER_STATUS_LOOKING = 'looking';
    CONST WORKER_STATUS_DONT_BOTHER = 'dont_bother';
    public static $worker_status_lookup = array(
    	self::WORKER_STATUS_ACTIVE => "Seeking work",
    	self::WORKER_STATUS_LOOKING => "Just looking",
    	self::WORKER_STATUS_DONT_BOTHER => "Don't contact me",
    );

    CONST HIDE_PROFILE_NOHIDE = 'nohide';
    CONST HIDE_PROFILE_CURRENT = 'current';
    CONST HIDE_PROFILE_ALL = 'all';
    public static $hide_profile_lookup = array(
        self::HIDE_PROFILE_NOHIDE => "Viewable by all employers",
        self::HIDE_PROFILE_CURRENT => "Hide from current employer",
        self::HIDE_PROFILE_ALL => "Hide from all employers"
    );

    CONST ALLOW_SMS_YES = 'true';
    CONST ALLOW_SMS_NO = 'false';
    public static $allow_sms_lookup = array (
        self::ALLOW_SMS_YES => "Yes",
        self::ALLOW_SMS_NO => "No"
    );


    CONST CANDIDATE_ETHNICITY_WHITE = 'white';
    CONST CANDIDATE_ETHNICITY_LATINO = 'latino';
    CONST CANDIDATE_ETHNICITY_BLACK = 'black';
    CONST CANDIDATE_ETHNICITY_ASIAN = 'asian';
    CONST CANDIDATE_ETHNICITY_PACIFIC_ISLANDER = 'pacific_islander';
    CONST CANDIDATE_ETHNICITY_AMERICAN_INDIAN = 'american_indian';
    CONST CANDIDATE_ETHNICITY_TWO_OR_MORE = 'two_or_more';
    CONST CANDIDATE_ETHNICITY_NO_ANSWER = 'no_answer';
    public static $candidate_ethnicity_lookup = array (
    	self::CANDIDATE_ETHNICITY_WHITE => "White",
    	self::CANDIDATE_ETHNICITY_LATINO => "Latino",
    	self::CANDIDATE_ETHNICITY_BLACK => "Black or African American",
    	self::CANDIDATE_ETHNICITY_ASIAN => "Asian",
    	self::CANDIDATE_ETHNICITY_PACIFIC_ISLANDER => "Native Hawaiian or Other Pacific Islander",
    	self::CANDIDATE_ETHNICITY_AMERICAN_INDIAN => "American Indian or Alaska Native",
    	self::CANDIDATE_ETHNICITY_TWO_OR_MORE => "Two or More Races",
    	self::CANDIDATE_ETHNICITY_NO_ANSWER => "I do not wish to answer"
    );

    CONST CANDIDATE_GENDER_MALE = 'male';
    CONST CANDIDATE_GENDER_FEMALE = 'female';
    CONST CANDIDATE_GENDER_NO_ANSWER = 'no_answer';
    public static $candidate_gender_lookup = array (
    	self::CANDIDATE_GENDER_MALE => "Male",
    	self::CANDIDATE_GENDER_FEMALE => "Female",
    	self::CANDIDATE_GENDER_NO_ANSWER => "I do not wish to answer"
    );

    CONST CANDIDATE_VETERAN_PROTECTED = 'protected';
    CONST CANDIDATE_VETERAN_PROTECTED_NOCLASS = 'protected_noclass';
    CONST CANDIDATE_VETERAN_PROTECTED_NOT = 'protected_not';
    CONST CANDIDATE_VETERAN_VETERAN_NOT = 'veteran_not';
    CONST CANDIDATE_VETERAN_NO_ANSWER = 'no_answer';
    public static $candidate_veteran_lookup = array (
    	self::CANDIDATE_VETERAN_PROTECTED => "I am a Protected Veteran of one or more of the classifications listed below",
    	self::CANDIDATE_VETERAN_PROTECTED_NOCLASS => "I am a Protected Veteran, but I choose not to self-identify the classification to which I belong",
    	self::CANDIDATE_VETERAN_PROTECTED_NOT => "I am not a Protected Veteran",
    	self::CANDIDATE_VETERAN_VETERAN_NOT => "I am not a Veteran",
    	self::CANDIDATE_VETERAN_NO_ANSWER => "I do not wish to answer"
    );


    CONST CANDIDATE_DISABILITY_YES = 'yes';
    CONST CANDIDATE_DISABILITY_NO = 'no';
    CONST CANDIDATE_DISABILITY_NO_ANSWER = 'no_answer';
    public static $candidate_disability_lookup = array (
    	self::CANDIDATE_DISABILITY_YES => "Yes, I have a disability (or previously had a disability)",
    	self::CANDIDATE_DISABILITY_NO => "No, I do not have a disability",
    	self::CANDIDATE_DISABILITY_NO_ANSWER => "I do not wish to answer"
    );


    CONST CANDIDATE_WORK_AUTHORIZATION_AUTHORIZED = 'authorized';
    CONST CANDIDATE_WORK_AUTHORIZATION_VISA = 'visa';
    CONST CANDIDATE_WORK_AUTHORIZATION_SPONSORSHIP = 'sponsorship';
    CONST CANDIDATE_WORK_AUTHORIZATION_NO_ANSWER = 'no_answer';
    public static $candidate_work_authorization_lookup = array (
    	self::CANDIDATE_WORK_AUTHORIZATION_AUTHORIZED => "I am authorized to work in the United States for any employer.",
    	self::CANDIDATE_WORK_AUTHORIZATION_VISA => "I am authorized to work in the United States solely for my present employer.",
    	self::CANDIDATE_WORK_AUTHORIZATION_SPONSORSHIP => "I require sponsorship to work in the United States.",
    	self::CANDIDATE_WORK_AUTHORIZATION_NO_ANSWER => "I do not wish to answer"
    );


    public static function init(){

    }

    public static function get_state_name ($selected) {
    	if (empty($selected)) {
    		return '';
    	} else {

      return self::$states_lookup[$selected];
    	}
    }

    public static function get_worker_status_name ($selected) {
    	if (empty($selected)) {
    		return '';
    	} else {
    		return self::$worker_status_lookup[$selected];
    	}
    }


    public static function get_phone_type_name ($selected) {
    	if (empty($selected)) {
    		return '';
    	} else {
      		return self::$phone_type_lookup[$selected];
    	}
    }

    public static function get_candidate_ethnicity_name ($selected) {
    	$value = self::$candidate_ethnicity_lookup[$selected];
    	if (empty($value)) {
    		$value = "Not Valid";
    	}
    	return $value;
    }

    public static function get_candidate_gender_name ($selected) {
    	$value = self::$candidate_gender_lookup[$selected];
    	if (empty($value)) {
    		$value = "Not Valid";
    	}
    	return $value;
    }

    public static function get_candidate_veteran_name ($selected) {
    	$value = self::$candidate_veteran_lookup[$selected];
    	if (empty($value)) {
    		$value = "Not Valid";
    	}
    	return $value;
    }

    public static function get_candidate_disability_name ($selected) {
    	$value = self::$candidate_disability_lookup[$selected];
    	if (empty($value)) {
    		$value = "Not Valid";
    	}
    	return $value;
    }

    public static function get_candidate_work_authorization_name ($selected) {
    	$value = self::$candidate_work_authorization_lookup[$selected];
    	if (empty($value)) {
    		$value = "Not Valid";
    	}
    	return $value;
    }


    public static function is_hide_profile_all($subeaseuser) {
      // if (empty( $subeaseuser ) || !is_object( $subeaseuser )) {
      if (!empty( $subeaseuser ) && is_object( $subeaseuser )) {
        if (get_the_author_meta( 'hide_profile', $subeaseuser->ID) == self::HIDE_PROFILE_ALL) {
          return true;
        } else {
          return false;
        }
      } else {
        error_log ("is_hide_profile_all::Not a valid User object");
        return false;
      }
    }

    public static function is_hide_profile_current($subeaseuser) {
      if (!empty( $subeaseuser ) && is_object( $subeaseuser )) {
        if (get_the_author_meta( 'hide_profile', $subeaseuser->ID) == self::HIDE_PROFILE_CURRENT) {
          return true;
        } else {
          return false;
        }
      } else {
        error_log ("is_hide_profile_current::Not a valid User object");
        return false;
      }
    }

    public static function is_hide_profile_nohide($subeaseuser) {
      if (!empty( $subeaseuser ) && is_object( $subeaseuser )) {
        if (get_the_author_meta( 'hide_profile', $subeaseuser->ID) == self::HIDE_PROFILE_NOHIDE) {
          return true;
        } else {
          return false;
        }
      } else {
        error_log ("is_hide_profile_nohide::Not a valid User object");
        return false;
      }
    }


  } // class definition
  Subease_Elements::init();
endif;
