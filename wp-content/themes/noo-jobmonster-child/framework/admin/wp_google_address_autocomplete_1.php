<?php
if (! function_exists('gmw_include_google_places_api')) {

    function gmw_include_google_places_api() {
        
        // register google maps api if not already registered
        if (! wp_script_is('google-maps', 'registered')) {
            wp_register_script('google-maps', (is_ssl() ? 'https' : 'http') . '://maps.googleapis.com/maps/api/js?libraries=places&region=US&key=' . GOOGLE_MAPS_API_KEY, array(
                'jquery'
            ), false);
        }
        
        // enqueue google maps api if not already enqueued
        if (! wp_script_is('google-maps', 'enqueued')) {
            wp_enqueue_script('google-maps');
        }
    }
    add_action('wp_enqueue_scripts', 'gmw_include_google_places_api');
}



?>
