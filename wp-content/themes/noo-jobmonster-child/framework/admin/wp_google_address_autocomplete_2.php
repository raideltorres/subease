<?php
if (! function_exists('gmw_google_address_autocomplete')) {

    function gmw_google_address_autocomplete() {
        ?>
<script>
	
	jQuery(document).ready(function($) {

		//Array of input fields ID. 
		var gacFields = ["address"];
	
		$.each( gacFields, function( key, field ) {

			var input = document.getElementById(field);
			console.log(input);

			//varify the field
			if ( input != null ) {
				
				//basic options of Google places API. 
				//see this page https://developers.google.com/maps/documentation/javascript/places-autocomplete
				//for other avaliable options
				var options = {
					types: ['geocode'],
					componentRestrictions: {country: "us"},
				};
				 
				var autocomplete = new google.maps.places.Autocomplete(input, options);
   				google.maps.event.addListener(autocomplete, 'place_changed', function() {
					jQuery(input).val(autocomplete.getPlace().formatted_address.replace(/,[^,]+$/, ""));
    				})

			}
		});
	});
    </script>
<?php
    }
    add_action('wp_footer', 'gmw_google_address_autocomplete');
}
?>
