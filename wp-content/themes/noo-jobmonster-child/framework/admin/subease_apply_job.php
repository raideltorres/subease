<?php

add_shortcode('subease_apply_job_form_sc', 'subease_apply_job_form');

if (! function_exists('subease_apply_job_form')) {

	function subease_apply_job_form() {
		do_action('subease_before_subease_apply_job');

		include (locate_template("layouts/subease_apply_job_form.php"));

		do_action('subease_after_subease_apply_job');
	}
}

add_action('subease_before_subease_apply_job', 'noo_message_print', 10);
add_action('subease_after_subease_apply_job', 'noo_message_clear', 10);

?>
