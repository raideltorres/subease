<?php
// Creating the Apply for Job widgets
// Author: Jay Jungalwala
// Created: 6/1/2016
//
class subease_job_posting_widget extends WP_Widget
{

	function __construct() {
		parent::__construct(
			// Base ID of your widget
			'subease_job_posting_widget',

			// Widget name will appear in UI
			__('Job Posting (subease)', 'subease_job_posting_widget_domain'),

			// Widget description
			array(
				'description' => __('Sidebar Widget to show job posting', 'subease_job_posting_widget_domain')
			));
	}

	// Creating widget front-end
	// This is where the action happens
	public function widget($args, $instance) {
		$title = apply_filters('widget_title', $instance['title']);
		// before and after widget arguments are defined by themes
		echo "<!-- Start of subease_job_posting_widget -->";
		echo $args['before_widget'];
		if (! empty($title)) echo $args['before_title'] . $title . $args['after_title'];

		// This is where you run the code and display the output
		echo __(self::render_job_listing(), 'subease_job_posting_widget_domain');
		echo $args['after_widget'];
		echo "<!-- End of subease_job_posting_widget -->";
	}

	// Widget Backend
	public function form($instance) {
		if (isset($instance['title'])) {
			$title = $instance['title'];
		} else {
			$title = __('New title', 'subease_job_posting_widget_domain');
		}
		// Widget admin form
		?>
<p>
	<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
</p>
<?php
	}

	// Updating widget replacing old instances with new
	public function update($new_instance, $old_instance) {
		$instance = array();
		$instance['title'] = (! empty($new_instance['title'])) ? strip_tags($new_instance['title']) : '';
		return $instance;
	}

	function render_job_listing() {

		// Check to see if job_id passed in.
		if ((isset($_REQUEST['job_id'])) && (! empty($_REQUEST['job_id'])) && (is_numeric($_REQUEST['job_id']))) {
			$job_id = $_REQUEST['job_id'];
		} else {
			return "<div>ERROR: No job_id was found.</div>";
		}

		$post = get_post($job_id);
		$output = '<div class="member-manage col-md-8">';

		if (is_object($post) && (get_post_status($post) == 'publish')) {
			$output .= '<div style="display: none"><!-- start wp_nonce_field-->' . wp_nonce_field("job-posting-action", "_application_job_listing", false, false) . '<!-- end wp_nonce_field--></div>';
			$output .= '<div class="member-manage-table">';

			$logo_company = '';
			$company_id = jm_get_job_company($post);
			$locations = get_the_terms(get_the_ID(), 'job_location');
			$list_job_meta = array(
				'show_company' => true,
				'show_type' => true,
				'show_location' => true,
				'show_date' => true,
				'show_closing_date' => false,
				'show_category' => true,
				'job_id' => '',
				'schema' => false
			);

			if (! empty($company_id)) {
				if (noo_get_option('noo_jobs_show_company_logo', true)) {
					$logo_company = Noo_Company::get_company_logo($company_id);
				}
			}
			ob_start();
			?>
<div class="floatwidth headingbottomborder paddingtop-bottom">
	<style>
.headingbottomborder {
	border-bottom: 1px solid #ddd;
}

.floatwidth {
	float: left;
}

.width {
	width: 100%;
}

.paddingtop-bottom {
	padding-top: 10px;
	padding-bottom: 10px;
}
</style>

	<!-- JOB LISTING -->
	<article noo_job data-url="<?php the_permalink(); ?>">
		<div class="loop-item-wrap">
				<?php
			if (! empty($logo_company)) {
				?>
					<div class="item-featured">
				<a href="<?php the_permalink(); ?>" title="<?php  the_title() ?>"><?php echo $logo_company ?></a>
			</div>
				<?php } ?>

				<div class="loop-item-content">
				<h2 class="loop-item-title">
					<a href="<?php the_permalink(); ?>" title="<?php  esc_attr( sprintf( __( 'Permanent link to: "%s"','noo' ), the_title_attribute( 'echo=0' ) ) ) ?>"><?php the_title() ?></a>
				</h2>

				<?php jm_the_job_meta($list_job_meta, $post); ?>
				<p> <?php echo $post->post_content; ?></p>
			</div>
		</div>
	</article>
	<!-- END OF JOB LISTING -->

				<?php
			$output .= ob_get_clean();
			// Job Categories

			// $output .= '<div class="floatwidth width"><div class="hidden-xs hidden-sm "><strong>Location:</strong> <i class="fa fa-map-marker"></i>&nbsp;<em>' . get_the_term_list(get_the_ID(), "job_location", "", ", ") . '</em></div></div><div class="floatwidth width">';

			$output .= '</div><div class="member-manage-toolbar bottom-toolbar clearfix"></div>';
		} else {
			$output .= '<h4>Could not find job listing.</h4>';
		}
		$output .= '</div>';
		return $output;
	}
}
// Class subease_job_posting_widget ends here

// Register and load the widget
function subease_job_posting_load_widget() {
	register_widget('subease_job_posting_widget');
}
add_action('widgets_init', 'subease_job_posting_load_widget');

// add_shortcode('render_job_listing', 'render_job_listing');
?>
