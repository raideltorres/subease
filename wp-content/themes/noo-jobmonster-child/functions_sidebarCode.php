<?php
error_reporting(E_ALL);
//ini_set( 'display_errors','1');
/**
 * Theme functions for NOO JobMonster Child Theme.
 *
 * @package    NOO JobMonster Child Theme
 * @version    1.0.0
 * @author     Kan Nguyen <khanhnq@nootheme.com>
 * @copyright  Copyright (c) 2014, NooTheme
 * @license    http://opensource.org/licenses/gpl-2.0.php GPL v2 or later
 * @link       http://nootheme.com
 */
define('GOOGLE_MAPS_API_KEY', 'AIzaSyCQrtGwuLwuYNvYQnTnOY3DkfYLIPShriQ');

// If you want to override function file ( like noo_job.php, noo_resume.php ... ),
// you should copy function file to the same folder ( like /framework/admin/ ) on child theme, then use similar require_one
// statement like this code.
// require_once dirname(__FILE__) . '/framework/admin/noo_job.php';

// admin
require_once dirname(__FILE__) . '/framework/admin/noo_application.php';
require_once dirname(__FILE__) . '/framework/admin/noo_company.php';
require_once dirname(__FILE__) . '/framework/admin/noo_form_handler.php';
require_once dirname(__FILE__) . '/framework/admin/noo_member.php';
require_once dirname(__FILE__) . '/framework/admin/noo_resume_package.php';
require_once dirname(__FILE__) . '/framework/admin/noo_resume.php';
require_once dirname(__FILE__) . '/framework/admin/wp_google_address_autocomplete_1.php';
require_once dirname(__FILE__) . '/framework/admin/wp_google_address_autocomplete_2.php';

// inc
require_once dirname(__FILE__) . '/framework/jm-inc/render-fields.php';

// location
require_once dirname(__FILE__) . '/framework/location/subease_location.php';
require_once dirname(__FILE__) . '/framework/subease/subease_location.php';

// job
require_once dirname(__FILE__) . '/framework/job/job-template.php';
require_once dirname(__FILE__) . '/framework/job/job-query.php';

// resume
require_once dirname(__FILE__) . '/framework/resume/admin-settings.php';
require_once dirname(__FILE__) . '/framework/resume/resume-custom-fields.php';
require_once dirname(__FILE__) . '/framework/resume/resume-query.php';

// subease
require_once dirname(__FILE__) . '/framework/subease/subease_ui_element_helper.php';
require_once dirname(__FILE__) . '/framework/subease/subease_utilities.php';

// widgets
require_once dirname(__FILE__) . '/widgets/widgets.php';

// require_once dirname(__FILE__) . '/layouts/noo_company-info.php';
// require_once dirname(__FILE__) . '/layouts/candidate_profile.php';
// require_once dirname(__FILE__) . '/layouts/manage-application.php';
// require_once dirname(__FILE__) . '/layouts/resume_candidate_profile.php';

// add_action('wp', function(){ echo '<pre>';print_r($GLOBALS['wp_filter']); echo '</pre>';exit; } );
// add_action( 'init', 'setting_my_first_cookie' );
// function setting_my_first_cookie() {
// setcookie( 'start_debug', '1', 30 * DAYS_IN_SECONDS, COOKIEPATH, COOKIE_DOMAIN );
// }


function application_listing(){
	global $wpdb;
	$output = '<style>
	.full_width{width:100%;float:left;}
	</style>';
	$args = array(
	'post_type'=>'noo_application',
	'post_parent__in'=>array_merge($job_ids, array(0)), // make sure return zero application if there's no job.
	'post_status'=>array('publish'),
	'posts_per_page' => 3 
);
$r = new WP_Query($args);
 if($r->have_posts()):?>
<?php
	while ($r->have_posts()):
	$r->the_post();
	global $post;
	$candidate_email = noo_get_post_meta($post->ID,'_candidate_email');
	?>
<?php  $candidate_link = apply_filters( 'noo_application_candidate_link', '', $post->ID, $candidate_email ); ?>
<?php  $output .= '<div class="full_width"><strong>Worker Name</strong>: <span class="candidate-name">'; ?>
<?php  if(!empty($candidate_link)) echo '<a href="'. $candidate_link . '">'; ?>
<?php  $output .= get_the_title(); ?>
<?php  if(!empty($candidate_link)) echo '</a>'; ?>
<?php  $output .='</span></div>'; ?>
<?php
	$output .= '<div class="full_width"><strong>Job Name:</strong> ';
	$parent_job = get_post( $post->post_parent );
	
	if ( $parent_job && $parent_job->post_type === 'noo_job' ) {
		$output .='<a href="' . get_permalink( $parent_job->ID ) . '">' . $parent_job->post_title . '</a>';
	}

	elseif ( $parent_job = noo_get_post_meta( $post->ID, '_job_applied_for', true ) ) {
		$output .=  esc_html( $parent_job );
	} else {
		$output .= '<span class="na">&ndash;</span>';
	}

	$output.='</div>';
	$output .='<div class="full_width"><strong>Applied On</strong>: <span><i class="fa fa-calendar"></i> <em>'. date_i18n( get_option('date_format'), strtotime( $post->post_date ) ).'</em></span></div>';
	?>
<?php  endwhile; ?>
<?php  else : ?>
<?php  $output .='<h3>'; ?>
<?php  $output .= _e('No Application','noo') ?>
<?php  $output .='</h3>'; ?>
<?php 
	endif;
	return $output;
}
add_shortcode('application_listing','application_listing');

function profile_completeness(){
	$company_id = isset($_GET['company_id']) ? absint($_GET['company_id']) : 0;
	$company = get_post($company_id);
	$user_ID = get_current_user_id(); 
	$field_array = array($company->post_title,noo_get_post_meta($company_id,'_website'),$company->post_content,noo_get_post_meta($company_id,'_logo'), noo_get_post_meta($company_id,'_cover_image'),noo_get_post_meta($company_id,'_googleplus'),noo_get_post_meta($company_id,'_facebook'), noo_get_post_meta($company_id,'_linkedin'),noo_get_post_meta($company_id,'_twitter'),noo_get_post_meta($company_id,'_instagram'),noo_get_post_meta($company_id,'_phone'));
	$total_fields = count($field_array);
	$filled_fields =  count(array_filter($field_array));
	$percentage =($filled_fields /$total_fields)*100;
	$percentage = round($percentage);
	$output = '<div  style="text-indent:'.($percentage - 10).'%">'.$percentage.'%</div>
			   <div class="progress"> <div class="progress-bar" role="progressbar" aria-valuenow="'.$percentage.'" aria-valuemin="0" aria-valuemax="100" style="width:'.$percentage.'%">
					<span class="sr-only">'.$percentage.' Complete</span>
				</div>
			</div>
			<ul>
				<li>Next Step: <a href="'.Noo_Member::get_company_profile_url().'">fill in a thing!</a></li>
			</ul>';
		return  $output;
	}
	
add_shortcode('profile_completeness','profile_completeness');

add_filter( 'widget_text', 'do_shortcode', 11);

function job_listing(){
$args = array(
			'post_type'=>'noo_job',
			'author'=>get_current_user_ID(),
			'post_status' => 'publish',
			'posts_per_page' => 3
		);
		$r = new WP_Query($args);
	//	$args['post_status'] = array();
	//$args['post_status'] = array('publish','pending','pending_payment','expired','inactive');
	//	$r = jm_user_job_query();
	$jobs_list = get_posts(array(
	'post_type'=>'noo_job',
	'post_status'=>array('publish','pending','expired'),
	'author'=>get_current_user_id(),
	'posts_per_page'=>3,
	'suppress_filters' => false
	));
	$job_ids = array_map(create_function('$o', 'return $o->ID;'), $jobs_list);
	$args = array(
		'post_type'=>'noo_application',
		'paged' => $paged,
		'post_parent__in'=>array_merge($job_ids, array(0)), // make sure return zero application if there's no job.
		'post_status'=>array('publish','pending','rejected'),
		'posts_per_page'=>3,
	);
	$applicationsResult = new WP_Query($args);
	$job_need_approve = jm_get_job_setting( 'job_approve','' ) == 'yes';
?>
<?php	
	$output = '<div class="member-manage col-md-8">';
	
	if($r->have_posts()):
	$output .='<div style="display: none">'.wp_nonce_field("job-manage-action").'</div>';
	$output .='<div class="member-manage-table">';
	while ($r->have_posts()):
	$r->the_post();
	global $post;
	$status = $status_class = jm_correct_job_status( $post->ID, $post->post_status );
	$statuses = jm_get_job_status();
	$status_text = '';
	
	if ( isset( $statuses[ $status ] ) ) {
		$status_text = $statuses[ $status ];
	} else {
		$status_text = __( 'Inactive', 'noo' );
		$status_class = 'inactive';
	}

	$output .='<div class="floatwidth headingbottomborder paddingtop-bottom">
							<style> 
							.headingbottomborder{
								border-bottom: 1px solid #ddd;
								}
							.floatwidth{
								float:left;}
								.width{
									width:100%;
									}
							.paddingtop-bottom{
								padding-top:10px;
								padding-bottom:10px;
								}			
							</style>';
	$output .='<div claas="width"><strong>Job Name:</strong>';
	
	if( $status == 'pending' || 'pending_payment' ) :
	$output .='<a href="'.esc_url(add_query_arg( "job_id", get_the_ID(), Noo_Member::get_endpoint_url("edit-job") )).'">'.get_the_title().'</a>'; else :
	$output .='<a href="'.the_permalink().'">'.get_the_title().'</a>';
	endif;
	$output .='</div>
<div claas="width"><strong>Project Name: </strong>
								</div>
<div class="floatwidth width"><div class="hidden-xs hidden-sm "><strong>Location:</strong> <i class="fa fa-map-marker"></i>&nbsp;<em>'.get_the_term_list(get_the_ID(),"job_location","",", ").'</em></div></div>
<div class="floatwidth width">
	<strong>Apps:</strong> ';
	$applications = get_posts(array('post_type' => 'noo_application','posts_per_page'=>3,'post_parent'=>$post->ID,'post_status'=>array('publish','pending','rejected'),'suppress_filters' => false));
	$output .=absint(count($applications));
	$output .='</div></div>';
	endwhile;
	$output .='</div>
			<div class="member-manage-toolbar bottom-toolbar clearfix">
			</div>'; else :
	$output .='<h4>"You have no job, why dont you start posting one. TESTING"</h4>
		<p>
			<a href="'.Noo_Member::get_post_job_url().'" class="btn btn-primary">Post Job</a>
		</p>';
	endif;
	$output .='</div>';
	return $output;
} 
add_shortcode('job_listing','job_listing'); 
	
function Plan_Summary(){
		$output ='<style>
		.fleft{float:left;}
		.fright{float:right;}
		</style>';
	$package = array();
	$package_page_id = '';
	if( Noo_Member::is_employer() ) {
		$package = jm_get_job_posting_info();
		$package_page_id = Noo_Job_Package::get_setting( 'package_page_id' );
	} elseif( Noo_Member::is_candidate() ) {
		$package = jm_get_resume_posting_info();
		$package_page_id = Noo_Resume_Package::get_setting( 'resume_package_page_id' );
	}
	$job_added = jm_get_job_posting_added();
	$feature_job_remain = jm_get_feature_job_remain();
$output .='<div style="width:100%; float:left;">';
 if(empty($package)) : 
		$output .='<p class="no-plan-package text-center">No Package</p><div class="member-plan-choose"><a class="btn btn-lg btn-primary" href="'.esc_url(get_permalink( $package_page_id )).'">Choose a Package</a></div>';
           else : 
			$output .='<div class="row">';
if( Noo_Member::is_employer() ) : 
			$output .='<div class="col-xs-12 col-md-6"><strong>Job Limit:</strong></div><div class="col-xs-12 col-md-6">'.sprintf(__("%s job(s)","noo"),$package["job_limit"] == 99999999 ? __("Unlimited","noo") : $package["job_limit"] ).'</div></div><div class="row">
				<div class="col-xs-12 col-md-6"><strong>Job Added: </strong></div>
				<div class="col-xs-12 col-md-6">'.sprintf(__("%s job(s)","noo"),jm_get_job_posting_added()).'</div>';
            endif;
          	$output .='</div><div class="row">';
			$total_jobs= sprintf(__('%s ','noo'), $package['job_limit'] == 99999999 ? __('Unlimited', 'noo') : $package['job_limit'] );
			$used_jobs= sprintf(__('%s ','noo'), jm_get_job_posting_added());
			$usedjobpercentage=round(($used_jobs/$total_jobs)*100);
			$usedjobs=$total_jobs-$used_jobs;
			$output .='<strong class="col-xs-12  col-md-6">Unusedjobs: </strong><span class=" col-md-6">'.$usedjobs.' job(s)</span><br />';
			$output .='<strong class="col-xs-12  col-md-6">UsedJob(%): </strong><span class=" col-md-6">'.$usedjobpercentage.'%</span><br />';
			$unusedjobs=round(($usedjobs/$total_jobs)*100); 
			$output .='<strong class="col-xs-12  col-md-6 ">Unusedjobs(%): </strong><span class="col-xs-12  col-md-6">'.$unusedjobs.'%</span>
			</div>';
		endif; 
		$output .='</div>';
		return $output;
}
add_shortcode('plan_summary','Plan_Summary');

function job_search(){
	$company_id = get_user_meta(get_current_user_id(),'employer_company',true);
					$job_ids = Noo_Company::get_company_jobs( $company_id, array(), -1 );
					$args = array(
							'post_type'=>'noo_job',
							'post__in' => array_merge( $job_ids, array( 0 ) ),
							'post_status' => 'publish'
					);

					$r = new WP_Query($args);
				/*	echo "<pre>";
					print_R($r);
					echo "</pre>";
					exit;*/
					
					//print_R($r->query_var);
				
					jm_job_loop(array(
						'query'=>$r,
						'title'=>sprintf(__('%s has posted %s jobs', 'noo'), get_the_title(), '<span class="text-primary">'.$r->found_posts.'</span>'),
						'no_content' => __('This company has no active jobs', 'noo')
					));

	}
	add_shortcode('job_search','job_search');
	
	
	
