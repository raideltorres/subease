<?php

/**
 * Theme functions for NOO JobMonster Child Theme.
 *
 * @package    NOO JobMonster Child Theme
 * @version    1.0.0
 * @author     Kan Nguyen <khanhnq@nootheme.com>
 * @copyright  Copyright (c) 2014, NooTheme
 * @license    http://opensource.org/licenses/gpl-2.0.php GPL v2 or later
 * @link       http://nootheme.com
 */

define('GOOGLE_MAPS_API_KEY', 'AIzaSyCQrtGwuLwuYNvYQnTnOY3DkfYLIPShriQ');

if (! defined('SUBEASE_NOO_FRAMEWORK')) {
	define( 'SUBEASE_NOO_FRAMEWORK', get_template_directory() . '/framework' );
}
if (! defined('SUBEASE_NOO_ASSETS_URI')) {
	define( 'SUBEASE_NOO_ASSETS_URI', get_stylesheet_directory_uri() . '/assets' );
}

if (! defined('SUBEASE_FRAMEWORK_URI')) {
	define( 'SUBEASE_FRAMEWORK_URI', get_template_directory_uri() . '-child/framework/subease' );
}


// If you want to override function file ( like noo_job.php, noo_resume.php ... ),
// you should copy function file to the same folder ( like /framework/admin/ ) on child theme, then use similar require_one
// statement like this code.
// require_once dirname(__FILE__) . '/framework/admin/noo_job.php';



// admin

require_once dirname(__FILE__) . '/framework/admin/noo_application.php';
require_once dirname(__FILE__) . '/framework/admin/noo_company.php';
require_once dirname(__FILE__) . '/framework/admin/noo_form_handler.php';
require_once dirname(__FILE__) . '/framework/admin/noo_member.php';
require_once dirname(__FILE__) . '/framework/admin/noo_resume_package.php';
require_once dirname(__FILE__) . '/framework/admin/noo_resume.php';
require_once dirname(__FILE__) . '/framework/admin/wp_google_address_autocomplete_1.php';
require_once dirname(__FILE__) . '/framework/admin/wp_google_address_autocomplete_2.php';
require_once dirname(__FILE__) . '/framework/admin/subease_apply_job.php';
require_once dirname(__FILE__) . '/framework/admin/subease_apply_job_widgets.php';
require_once dirname(__FILE__) . '/framework/admin/meta-boxes/generate-meta-box.php';
// errors out
//require_once get_template_directory() . '/framework/admin/customizer/class-helper.php';
//require_once dirname(__FILE__) . '/framework/admin/customizer/options.php';


// inc
require_once dirname(__FILE__) . '/framework/jm-inc/render-fields.php';
require_once dirname(__FILE__) . '/framework/jm-inc/custom-fields.php';
require_once dirname(__FILE__) . '/framework/jm-inc/jm-upload.php';


// location
require_once dirname(__FILE__) . '/framework/location/subease_location.php';
require_once dirname(__FILE__) . '/framework/subease/subease_location.php';



// job
require_once dirname(__FILE__) . '/framework/job/job_location.php';
require_once dirname(__FILE__) . '/framework/job/job_category.php';
require_once dirname(__FILE__) . '/framework/job/job-enqueue.php';
require_once dirname(__FILE__) . '/framework/job/job-template.php';
require_once dirname(__FILE__) . '/framework/job/job-query.php';
require_once dirname(__FILE__) . '/framework/job/job-custom-fields.php';
require_once dirname(__FILE__) . '/framework/job/admin-job-edit.php';
require_once dirname(__FILE__) . '/framework/job/job-ask-voluntary.php';
require_once dirname(__FILE__) . '/framework/job/job-default-fields.php';

// resume
require_once dirname(__FILE__) . '/framework/resume/admin-settings.php';
require_once dirname(__FILE__) . '/framework/resume/admin-resume-edit.php';
require_once dirname(__FILE__) . '/framework/resume/resume-default-fields.php';
require_once dirname(__FILE__) . '/framework/resume/resume-custom-fields.php';
require_once dirname(__FILE__) . '/framework/resume/resume-query.php';


// subease
require_once dirname(__FILE__) . '/framework/subease/subease_ui_element_helper.php';
require_once dirname(__FILE__) . '/framework/subease/subease_utilities.php';
require_once dirname(__FILE__) . '/framework/subease/contractor-dashboard.php';
require_once dirname(__FILE__) . '/framework/subease/subease-ajax-experience.php';

// widgets
require_once dirname(__FILE__) . '/widgets/widgets.php';

// functions
require_once dirname(__FILE__) . '/framework/functions/noo-utilities.php';


// errors out
//require_once dirname(__FILE__) . '/framework/functions/noo-wp-style.php';

function __TEST__ () {
	/* I am using this function to test some code */
  print_r(apache_get_modules());

}
add_shortcode('__TEST__','__TEST__');

// Hook the appropriate WordPress action
add_action('init', 'prevent_wp_login');

function prevent_wp_login() {
    // WP tracks the current page - global the variable to access it
    global $pagenow;
    // Check if a $_GET['action'] is set, and if so, load it into $action variable
    $action = (isset($_GET['action'])) ? $_GET['action'] : '';
    // Check if we're on the login page, and ensure the action is not 'logout' (or 'lostpassword' or 'rp')
    if( $pagenow == 'wp-login.php' && ( ! $action || ( $action && ! in_array($action, array('logout'))))) {
        // Load the home page url
        $page = get_bloginfo('url');
        // Redirect to the home page
        wp_redirect($page);
        // Stop execution to prevent the page loading for any reason
        exit();
    }
}


if ( function_exists('register_sidebar') )
    register_sidebar(array(
      'name' => 'job_search_heading_area',
      'before_widget' => '<div class = "widgetizedArea sb-find-jobs-heading">',
      'after_widget' => '</div>',
      'before_title' => '<h3>',
      'after_title' => '</h3>',
    )
);

