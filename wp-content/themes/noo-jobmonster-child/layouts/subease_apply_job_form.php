<?php
global $post;
if (is_user_logged_in()) {
	$user = wp_get_current_user();
	$candidate_name = $user->display_name;
	$candidate_email = $user->user_email;
} else {
	$candidate_name = '';
	$candidate_email = '';
}
$member_apply = jm_get_setting('noo_job_linkedin', 'member_apply', jm_get_job_setting('member_apply', ''));
$disable_member_upload = jm_get_setting('noo_job_linkedin', 'disable_member_upload', jm_get_job_setting('member_upload', ''));
$show_button = true;

if (is_user_logged_in() && Noo_Member::is_candidate() && jm_resume_enabled()) :
	$args = apply_filters('noo_application_resume_query_args', array(
		'post_type' => 'noo_resume',
		'posts_per_page' => - 1,
		'post_status' => array(
			'publish'
		),
		'author' => get_current_user_id()
	));
	$r = new WP_Query($args);
 else :
	$r = false;
endif;

$can_upload = $member_apply != 'yes' || $disable_member_upload != 'yes' || ($r && $r->found_posts);

$allowed_file_types = Noo_Resume::get_setting('extensions_upload_resume', 'doc,pdf');
$allowed_file_types = ! empty($allowed_file_types) ? explode(',', $allowed_file_types) : array();
$allowed_exts = array();
foreach ($allowed_file_types as $type) {
	$type = trim($type);
	if (empty($type)) continue;
	$type = substr($type, 0) != '.' ? '.' . $type : $type;
	$allowed_exts[] = $type;
}

// Get the original Referrer
$orig_ref = '';
if (isset($_REQUEST['orig_ref'])) {
	$orig_ref = $_REQUEST['orig_ref'];
} else {
	$orig_ref = $_SERVER["REQUEST_URI"];
	// echo '<pre>'.print_r($_REQUEST,true).'</pre>';
}
// Get the Job ID
$job_id = isset($_REQUEST['job_id'])? $_REQUEST['job_id'] : '';
if (empty($job_id) || ! is_numeric($job_id)) {
	echo "<div>Invalid Job Id</div>";
	return;
}
$fast_apply = isset($_REQUEST['fast_apply']) && $_REQUEST['fast_apply'] != 0;

$ask_voluntary = job_ask_voluntary($job_id);

// For form validation and speed up submitting to multiple jobs
$application_message = isset($_COOKIE['application_message']) ? $_COOKIE['application_message'] : '';
$resume_id = isset($_COOKIE['resume_id']) ? $_COOKIE['resume_id'] : '';
$candidate_work_authorization = isset($_COOKIE['candidate_work_authorization']) ? $_COOKIE['candidate_work_authorization'] : '';
$candidate_ethnicity = isset($_COOKIE['candidate_ethnicity']) ? $_COOKIE['candidate_ethnicity'] : '';
$candidate_gender = isset($_COOKIE['candidate_gender']) ? $_COOKIE['candidate_gender'] : '';
$candidate_veteran = isset($_COOKIE['candidate_veteran']) ? $_COOKIE['candidate_veteran'] : '';
$candidate_veteran_disabled = isset($_COOKIE['candidate_veteran_disabled']) && ($_COOKIE['candidate_veteran_disabled'] == "on") ? 'checked' : '';
$candidate_veteran_separated = isset($_COOKIE['candidate_veteran_separated']) && ($_COOKIE['candidate_veteran_separated'] == "on") ? 'checked' : '';
$candidate_veteran_discharged = isset($_COOKIE['candidate_veteran_discharged']) ? $_COOKIE['candidate_veteran_discharged'] : '';
$candidate_veteran_active = isset($_COOKIE['candidate_veteran_active']) && ($_COOKIE['candidate_veteran_active'] == "on") ? 'checked' : '';
$candidate_veteran_medal = isset($_COOKIE['candidate_veteran_medal']) && ($_COOKIE['candidate_veteran_medal'] == "on") ? 'checked' : '';
$candidate_disability = isset($_COOKIE['candidate_disability']) ? $_COOKIE['candidate_disability'] : '';

// Used to help render elements that are not defined in a database
$subease_ui_helper = new Subease_UI_Element_Helper();


?>

<div>
	<!--  id="applyJobModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="applyJobModalLabel" aria-hidden="true" -->
	<div>
		<!--  class="modal-dialog" -->
		<div>
			<!-- class="modal-content"  -->
			<div>
				<!-- class="modal-header" -->
				<h4 class="modal-title" id="applyJobModalLabel"><?php esc_html_e('Apply For Job','noo')?></h4>
			</div>
			<div>
				<!--  class="modal-body" -->
				<?php if( $can_upload ) : ?>
					<form id="subease_apply_job_form" class="form-horizontal jform-validate" method="post" enctype="multipart/form-data">
					<div style="display: none">
						<input type="hidden" name="action" value="subease_apply_job">
						<input type="hidden" name="job_id" value="<?php echo esc_attr($job_id)?>">
						<input type="hidden" name="orig_ref" value="<?php echo esc_attr($orig_ref)?>">
						<?php wp_nonce_field('subease-apply-job')?>
					</div>
					<div class="form-group text-center noo-ajax-result" style="display: none"></div>

					<div class="app_form" id="page1">

						<div class="form-group">
							<label for="candidate_name" class="control-label"><?php _e('Name','noo')?></label> <input type="text" class="form-control jform-validate" id="candidate_name" value="<?php echo esc_attr($candidate_name)?>" name="candidate_name" autofocus required placeholder="<?php echo esc_attr__('Name','noo')?>">
						</div>
						<div class="form-group">
							<label for="candidate_email" class="control-label"><?php _e('Email','noo')?></label> <input type="email" class="form-control jform-validate jform-validate-email" id="candidate_email" value="<?php echo esc_attr($candidate_email)?>" name="candidate_email" required placeholder="<?php echo esc_attr__('Email','noo')?>">
						</div>
						<?php
							if ($fast_apply) {
								// Waiting to see what we really expect from applicants
								//$field = array('');
						?>
						<div class="form-group">
							<label for="application_message" class="control-label"><?php _e('Message','noo')?></label>

							<textarea class="form-control jform-validate" id="application_message" name="application_message" rows="5" placeholder="<?php esc_attr_e('Your cover letter/message sent to the employer','noo')?>"><?php echo esc_attr__($application_message,'noo')?></textarea>
						</div>
						<?php
							} else {
						?>
						<div class="form-group">
							<label for="application_message" class="control-label"><?php _e('Message','noo')?></label>
							<textarea class="form-control jform-validate" id="application_message" name="application_message" rows="5" placeholder="<?php esc_attr_e('Your cover letter/message sent to the employer','noo')?>"><?php echo esc_attr__($application_message,'noo')?></textarea>
						</div>
						<div class="form-group">
							<div class="row">
									<?php if( $member_apply != 'yes' || $disable_member_upload != 'yes' ) { ?>
									<div class="col-sm-6">
										<label for="application_attachment" class="control-label"><?php _e('Upload to CV','noo')?></label>
										<div class="form-control-flat">
											<label class="form-control-file"> <span class="form-control-file-button"><i class="fa fa-folder-open"></i> <?php _e('Browse','noo')?></span> <input type="file" name="application_attachment" class="jform-validate-uploadcv" accept="<?php echo implode(',', $allowed_exts); ?>"> <input type="text" readonly value="" class="form-control" autocomplete="off"></label>
											<?php $max_upload_size = wp_max_upload_size();
												if (! $max_upload_size) {
													$max_upload_size = 0;
												}
											?>
										</div>
										<p class="help-block"><?php printf( __( 'Maximum upload file size: %s', 'noo' ), esc_html( size_format( $max_upload_size ) ) ); ?></p>
										<?php /*
							       			   * <div class="upload-to-cv clearfix">
							       			   * <?php //noo_plupload_form('cv','pdf,doc,jpg,gif,png') ?>
							       			   * </div>
							       			   */
											?>
									</div>
									<?php } ?>
									<?php if($r && $r->post_count) { ?>
										<div class="col-sm-6">
											<label for="email" class="control-label"><?php _e('Select Resume','noo')?></label>
											<div class="form-control-flat">
												<select name="resume">
													<option value=""><?php _e('-Select-','noo')?></option>
													<?php while ($r->have_posts()): $r->the_post()?>
													<option value="<?php echo get_the_ID()?>" <?php if (get_the_ID() == $resume_id) {  echo 'Selected';} ?>><?php echo get_the_title(get_the_ID())?></option>
													<?php endwhile;?>
												</select> <i></i>
											</div>
										</div>
									<?php } ?>
								</div>
						</div>
						<?php
							}
						?>
					</div> <!--  End of Page 1 -->
					<?php
						if ($ask_voluntary == true) {
							include(locate_template("layouts/ask_voluntary_form.php"));
						}
					?>


					<?php do_action( 'after_apply_job_form' ); ?>
					<?php if ( $show_button == true ) { ?>
						<div class="modal-actions">
						<?php if ($ask_voluntary == true && $fast_apply !== true) { ?>
							<button type="button" class="btn btn-primary" id="prev"><?php _e('Previous','noo')?></button>
							<button type="button" class="btn btn-primary" id="next"><?php _e('Next','noo')?></button>
						<?php } ?>
							<button type="submit" class="btn btn-primary" id="apply"><?php _e('Send application','noo')?></button>
						</div>
					<?php } ?>
					</form>
				<?php else : ?>
					<h4><?php echo __("You have no resume for application, please create a new resume first.",'noo')?></h4>
				<p>
					<a href="<?php echo Noo_Member::get_post_resume_url(); ?>" class="btn btn-primary"><?php _e('Create New Resume', 'noo'); ?></a>
				</p>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>

