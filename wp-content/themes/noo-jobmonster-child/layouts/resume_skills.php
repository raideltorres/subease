<?php 
if( Noo_Resume::enable_resume_detail() ) :
	$resume_id = isset($_GET['resume_id']) ?absint($_GET['resume_id']) : 0;
	$resume = $resume_id ? get_post($resume_id) : '';

	$skill = array();
	$enable_skill = Noo_Resume::get_setting('enable_skill', '1');
	if( $enable_skill ) {
		$skill['name'] = noo_json_decode( noo_get_post_meta( $resume_id, '_skill_name', '' ) );
		$skill['percent'] = noo_json_decode( noo_get_post_meta( $resume_id, '_skill_percent', '' ) );
	}
	?>
	<?php do_action('noo_post_resume_detail_before'); ?>
	<div class="resume-form">
		<div class="resume-form-detail">

		<?php if( $enable_skill ) : ?>
			<div class="form-group row">
				<label class="col-sm-3 control-label"><?php _e('Summary of Skill','noo')?></label>
				<div class="col-sm-9">
					<div class="noo-metabox-addable" data-name="_skill" >
						<div class="noo-addable-fields">
							<input type="hidden" value="" name="_skill_name" />
							<input type="hidden" value="" name="_skill_percent" />
							<input type="hidden" value="" name="_skill_date" />
							<input type="hidden" value="" name="_skill_note" />
							<?php
							if( isset( $skill['name'] ) && is_array( $skill['name'] ) && count($skill['name'] ) ) :
								foreach( $skill['name'] as $index => $name ) : 
								?>
									<div class="fields-group row">
										<div class="col-sm-9 col-xs-6">
											<input type="text" class="form-control" placeholder="<?php _e('Skill Name', 'noo'); ?>" name='_skill_name[]' value="<?php echo esc_attr($skill['name'][$index]); ?>" />
										</div>
										<div class="col-sm-3 col-xs-6">
											<input type="text" class="form-control" name='_skill_percent[]' value="<?php echo esc_attr($skill['percent'][$index]); ?>" />
											<span class="percent-text"><?php _e('% (1 to 100)', 'noo'); ?></span>
										</div>
									</div>
								<?php endforeach; ?>
							<?php else : ?>
								<div class="fields-group row">
									<div class="col-sm-9 col-xs-6">
										<input type="text" class="form-control" placeholder="<?php _e('Skill Name', 'noo'); ?>" name='_skill_name[]' />
									</div>
									<div class="col-sm-3 col-xs-6">
										<input type="text" class="form-control" name='_skill_percent[]' />
										<span class="percent-text"><?php _e('% (1 to 100)', 'noo'); ?></span>
									</div>
								</div>
							<?php endif; ?>
						</div>
						<div class="noo-addable-actions">
							<a href="javascript:void()" class="noo-clone-fields pull-left" data-template="<div class='fields-group row'><div class='col-sm-9 col-xs-6'><input type='text' class='form-control' placeholder='<?php _e('Skill Name', 'noo'); ?>' name='_skill_name[]' /></div><div class='col-sm-3 col-xs-6'><input type='text' class='form-control' name='_skill_percent[]' /><span class='percent-text'><?php _e('% (1 to 100)', 'noo'); ?></span></div></div>">
								<i class="fa fa-plus-circle text-primary"></i>
								<?php _e('Add Skill', 'noo'); ?>
							</a>
							<a href="javascript:void()" class="noo-remove-fields pull-right">
								<i class="fa fa-times-circle"></i>
								<?php _e('Delete', 'noo'); ?>
							</a>
						</div>
					</div>
				</div>
			</div>
		<?php endif; ?>
		</div>
	</div>
	<?php do_action('noo_post_resume_detail_after'); ?>
<?php endif; ?>