<?php
	$noo_single_jobs_layout = noo_get_option('noo_single_jobs_layout', 'right_company');
?>

<div class="<?php noo_main_class(); ?>" role="main">
	<div class="job-desc" itemprop="description">
		<?php do_action( 'jm_job_detail_content_before' ); ?>
		<?php the_content(); ?>
		<?php do_action( 'jm_job_detail_content_after' ); ?>
	</div>
	<div class="job-action hidden-print clearfix">
		<?php if( 'expired' == get_post_status( $job_id ) ) : ?>
			<div class="noo-messages noo-message-error">
				<ul>
					<li><?php echo __('This job is expired!', 'noo'); ?></li>
				</ul>
			</div>
		<?php else : ?>
			<?php
				$login_apply = jm_get_setting('noo_job_linkedin', 'member_apply', jm_get_job_setting( 'member_apply','')) == 'yes';
				$is_logged_in = Noo_Member::is_logged_in();
				$is_candidate = Noo_Member::is_candidate();
				$has_applied = $is_candidate ? Noo_Application::has_applied( 0, $job_id ) : false;
				$custom_apply_link = jm_get_setting('noo_job_linkedin', 'custom_apply_link' );
				$apply_url = !empty( $custom_apply_link ) ? noo_get_post_meta( $job_id, '_custom_application_url', '' ) : '';
			?>
			<?php if( $is_candidate ) : ?>
				<div class="noo-ajax-result" style="display: none"></div>
			<?php endif; ?>
			<?php if( $login_apply && !$is_logged_in ) : ?>
				<a class="btn btn-primary member-login-link" data-target="#memberModalLogin" href="#" data-toggle="modal"><?php _e('Login to apply','noo');?></a>
				<a class="btn btn-primary" href="/application/?job_id=<?php echo $job_id; ?>&fast_apply=1"><?php _e('Fast apply','noo');?></a>
			<?php elseif ( $is_candidate ) : ?>
				<?php if( $has_applied ) : ?>
					<div class="noo-messages noo-message-notice pull-left">
						<ul>
							<li><?php echo __('You have already applied for this job', 'noo'); ?></li>
						</ul>
					</div>
				<?php else: ?>

					<?php /* Original Clause to do Modal Job Application Form
					<?php if( empty( $apply_url ) ) : ?>
						<a class="btn btn-primary" data-target="#applyJobModal" href="#" data-toggle="modal"><?php _e('Apply for this job','noo');?></a>
						<?php //noo_get_layout('apply_job_form');
							include(locate_template("layouts/apply_job_form.php"));
						?>
						*/ ?>

					<?php if( empty( $apply_url ) ) : ?>
						<a class="btn btn-primary" href="/application?job_id=<?php echo $job_id; ?>"><?php _e('Apply for this job','noo');?></a>
						<a class="btn btn-primary" href="/application/?job_id=<?php echo $job_id; ?>&fast_apply=1"><?php _e('Fast apply','noo');?></a>
						<?php
							// include(locate_template("layouts/apply_job_form.php"));
						?>
					<?php else : ?>
						<a class="btn btn-primary" href="<?php echo esc_url( $apply_url ); ?>" target="_blank" ><?php _e('Apply for this job','noo');?></a>
						<a class="btn btn-primary" href="/application/?job_id=<?php echo $job_id; ?>&fast_apply=1"><?php _e('Fast apply','noo');?></a>
					<?php endif; ?>
					<?php
						if(jm_get_setting('noo_job_linkedin','use_apply_with_linkedin') == 'yes'):
							// noo_get_layout('apply_job_via_linkedin_form');
							include(locate_template("layouts/apply_job_via_linkedin_form.php"));
						endif;
					?>
				<?php endif; ?>
			<?php elseif ( wp_get_referer() ) : ?>
				<a class="btn btn-primary" href="<?php echo wp_get_referer() ?>"><?php _e('Go back','noo');?></a>
			<?php else : ?>
				<a class="btn btn-primary" href="<?php echo get_home_url() ?>"><?php _e('Go home','noo');?></a>
			<?php endif; ?>
			<?php if( $is_candidate ) : ?>
				<a class="bookmark-job <?php echo ( jm_is_job_bookmarked(0, $job_id) ? 'bookmarked' : '' ); ?> pull-right" href="javascript:void(0);" data-toggle="tooltip" data-job-id="<?php echo esc_attr($job_id); ?>" data-action="noo_bookmark_job" data-security="<?php echo wp_create_nonce( 'noo-bookmark-job' );?>" title="<?php _e('Bookmark Job', 'noo'); ?>"><i class="fa fa-heart"></i></a>
			<?php endif; ?>
		<?php endif; ?>
	</div>
	<?php jm_the_job_tag(); ?>
	<?php
		//  -- Check display company
		if ( $noo_single_jobs_layout == 'left_sidebar' || $noo_single_jobs_layout == 'fullwidth' || $noo_single_jobs_layout == 'sidebar' ) :

			// -- Job Social Share
			jm_the_job_social($job_id, __('Share this job','noo'));

			// -- check option turn on/off show company info
			if( noo_get_option('noo_company_info_in_jobs', true) ) :
				Noo_Company::display_sidebar($company_id, true);
			endif;

		endif;
	?>
	<?php if (  noo_get_option( 'noo_job_related', true ) ) : ?>
		<?php jm_related_jobs($job_id, __('Related Jobs','noo')); ?>
	<?php endif; ?>
	<?php if ( noo_get_option( 'noo_job_comment', false ) && comments_open() ) : ?>
		<?php comments_template( '', true ); ?>
	<?php endif; ?>
</div> <!-- /.main -->
<?php if( $noo_single_jobs_layout != 'fullwidth' ) : ?>
<div class="<?php noo_sidebar_class(); ?> hidden-print">
	<div class="noo-sidebar-wrap">
	<?php
		//  -- Check display company
		if ( $noo_single_jobs_layout != 'left_sidebar' && $noo_single_jobs_layout != 'sidebar' ) :

			// -- Job Social Share
				jm_the_job_social($job_id, __('Share this job','noo'));

			// -- show company info
				Noo_Company::display_sidebar($company_id, true);

		else :
			// -- show siderbar
				if ( ! function_exists( 'dynamic_sidebar' ) || ! dynamic_sidebar( ) ) :
					dynamic_sidebar( noo_get_option('noo_single_jobs_sidebar', true) );
				endif;
		endif;
	?>
	</div>
</div>
<?php endif; ?>
