<?php
if( Noo_Resume::enable_resume_detail() ) :
	$resume_id = isset($_GET['resume_id']) ?absint($_GET['resume_id']) : 0;
	$resume = $resume_id ? get_post($resume_id) : '';

	$experience = array();
	$enable_experience = Noo_Resume::get_setting('enable_experience', '1');
	if( $enable_experience ) {
		$experience['employer'] = noo_json_decode( noo_get_post_meta( $resume_id, '_experience_employer', '' ) );
		$experience['job'] = noo_json_decode( noo_get_post_meta( $resume_id, '_experience_job', '' ) );
		$experience['date'] = noo_json_decode( noo_get_post_meta( $resume_id, '_experience_date', '' ) );
		$experience['note'] = noo_json_decode( noo_get_post_meta( $resume_id, '_experience_note', '' ) );
		$experience['project_image'] = noo_json_decode( noo_get_post_meta( $resume_id, '_experience_project_image', '' ) );
	}



	?>
	<?php do_action('noo_post_resume_detail_before'); ?>
	<div class="resume-form">
		<div class="resume-form-detail">
		<?php if( $enable_experience ) : ?>
			<div class="form-group row">
				<label class="col-sm-3 control-label"><?php _e('Work Experience','noo')?></label>
				<div class="col-sm-9">
					<div class="noo-metabox-addable" data-name="_experience" >
						<div class="noo-addable-fields">
							<input type="hidden" value="" name="_experience_employer" />
							<input type="hidden" value="" name="_experience_job" />
							<input type="hidden" value="" name="_experience_date" />
							<input type="hidden" value="" name="_experience_note" />
							<input type="hidden" value="" name="_experience_project_image" />
							<?php
							if( isset( $experience['employer'] ) && is_array( $experience['employer'] ) && count( $experience['employer'] ) )  {
								foreach( $experience['employer'] as $index => $employer ) {
									echo generate_experience_fields($experience, $index);
								}
							} else {
								echo generate_experience_fields($experience, 0);
							}
							?>

						</div>
						<div class="noo-addable-actions">
						    <a href="javascript:void(0)" class="pull-left add-experience">
								<i class="fa fa-plus-circle text-primary"></i>
								<?php _e('Add Experience', 'noo'); ?>
							</a>
							<a href="javascript:void()" class="noo-remove-fields pull-right">
								<i class="fa fa-times-circle"></i>
								<?php _e('Delete', 'noo'); ?>
							</a>
						</div>
					</div>
				</div>
			</div>
		<?php endif; ?>
	</div>
	<?php do_action('noo_post_resume_detail_after'); ?>
<?php endif; ?>