					<div class="app_form" id="page2">
					<!-- SUBEASE: New fields -->
						<div class="form-group">
							<label for="candidate_work_authorization" class="control-label"><?php _e('Work Authorization','noo')?></label>
							<div class="form-control-flat">
	              				<?php $subease_ui_helper->display_select($subease_ui_helper::$candidate_work_authorization_lookup,"candidate_work_authorization",$candidate_work_authorization,true,'Select Work Authorization','jform-validate','autofocus required')?>
	              			</div>
						</div>
					</div> <!-- End of Page 2 -->
					<div class="app_form" id="page3">
						<h5 class="modal-title" id="applyJobModalLabel"><?php esc_html_e('Self Identification Form','noo')?></h5>
						<div>
							<p>To comply with the regulations for equal employment opportunity and affirmative action (EEO/AA),
							we must track our applicants by gender and race/ethnicity and the position they applied for to the government.
							We are an organization that values diversity and encourages women and minorities to apply.
							For this reason, we invite you to indicate your gender and race/ethnicity below.
							<strong>This information is kept separate from your application.</strong></p>
							<p>Submission of this information is voluntary and refusal to provide it will not subject you to any adverse treatment.
							Responses will remain confidential within the Human Resources Department; and will be used only for the
							necessary information to include in our Affirmative Action Program and reporting requirements to the government.
							When reported, data will not identify any specific individuals.</p>
						</div>
						<div class="form-group">
							<label for="candidate_ethnicity" class="control-label"><?php _e('Ethnicity','noo')?></label> (<a href="#ethnic_defs" id="ethnic_defs">Definitions</a>)
							<div class="form-control-flat">
	              				<?php $subease_ui_helper->display_select($subease_ui_helper::$candidate_ethnicity_lookup,"candidate_ethnicity",$candidate_ethnicity,true,'Select Ethnicity','jform-validate','autofocus required')?>
	              			</div>
	              			<div class="ethnic_definition">
	              			<h6>Definitions of race/ethnic categories</h6>
		              			<p>
		              				<ul>
		              					<li>
											<strong>Hispanic of Latino</strong> - A person of Cuban, Mexican, Puerto Rican, South or Central American, or other Spanish culture or origin regardless of race.
										</li>
										<li>
											<strong>White (Not Hispanic or Latino)</strong> - A person having origins in any of the original peoples of Europe, the Middle East, or North Africa.
										</li>
										<li>
											<strong>Black or African American (Not Hispanic or Latino)</strong> - A person having origins in any of the black racial groups of Africa.
										</li>
										<li>
											<strong>Native Hawaiian or Other Pacific Islander (Not Hispanic or Latino)</strong> - A person having origins in any of the peoples of Hawaii, Guam, Samoa, or other Pacific Islands.
										</li>
										<li>
											<strong>Asian (Not Hispanic or Latino)</strong> - A person having origins in any of the original peoples of the Far East, Southeast Asia, or the Indian Subcontinent, including, for example, Cambodia, China, India, Japan, Korea, Malaysia, Pakistan, the Philippine Islands, Thailand, and Vietnam.
										</li>
										<li>
											<strong>American Indian or Alaska Native (Not Hispanic or Latino)</strong> - A person having origins in any of the original peoples of North and South America (including Central America), and who maintain tribal affiliation or community attachment.
										</li>
										<li>
											<strong> Two or More Races (Not Hispanic or Latino)</strong> - All persons who identify with more than one of the above five races.
										</li>
									</ul>
								</p>
	              			</div>
						</div>
						<div class="form-group">
							<label for="candidate_gender" class="control-label"><?php _e('Gender','noo')?></label>
							<div class="form-control-flat">
	               				<?php $subease_ui_helper->display_select($subease_ui_helper::$candidate_gender_lookup,"candidate_gender",$candidate_gender,true,'Select Gender','jform-validate','autofocus required')?>
	              			</div>
						</div>
					</div> <!-- End of Page 3 -->
					<div class="app_form" id="page4">
						<h5 class="modal-title" id="applyJobModalLabel"><?php esc_html_e('Veteran Self Identification Form','noo')?></h5>
						<div class="form-group">
							<div>
								<p>If you believe you belong to any of the categories of protected veterans listed here, please indicate by selecting the appropriate box below.
								We request this information in order to measure the effectiveness of the outreach and positive recruitment efforts we undertake pursuant to VEVRAA.
								</p>
							</div>
							<label for="candidate_veteran" class="control-label"><?php _e('Veteran','noo')?></label>
							<div class="form-control-flat">
								<?php $subease_ui_helper->display_select($subease_ui_helper::$candidate_veteran_lookup,"candidate_veteran",$candidate_veteran,true,'Select Veteran Status','jform-validate','autofocus')?>
							</div>
							<div class="form-control-flat">
								<p><b>Protected Veteran Classifications</b> (<a href="#protected_vets_defs" id="protected_vets_defs">Definitions</a>)</p>
								<ul>
									<li style="list-style-type: none;"><label for="candidate_veteran_disabled"><input style="white-space: nowrap !important; display: -moz-inline-stack; display: inline-block !important; height: auto !important; width: auto !important" type="checkbox" id="candidate_veteran_disabled" name="candidate_veteran_disabled" <?php echo $candidate_veteran_disabled; ?>> <?php _e('Disabled veteran','noo')?></label></li>
									<li style="list-style-type: none;">
										<label for="candidate_veteran_separated">
											<input type="checkbox" id="candidate_veteran_separated" style="white-space: nowrap !important; display: -moz-inline-stack; display: inline-block !important; height: auto !important; width: auto !important" name="candidate_veteran_separated" <?php echo $candidate_veteran_separated; ?>> <?php _e('Recently separated veteran','noo')?>
										</label>
									</li>
									<li style="list-style-type: none;">
										<ul>
											<li style="list-style-type: none;">
												<label for="candidate_veteran_discharged"><?php _e('Date of discharge','noo')?> (mm/dd/yyyy)
													<input type="text" id="candidate_veteran_discharged" class="jform-validate" name="candidate_veteran_discharged" placeholder="mm/dd/yyyy" value="<?php echo $candidate_veteran_discharged; ?>" style="white-space: nowrap !important; display: -moz-inline-stack; display: inline-block !important; height: auto !important; width: auto !important">
												</label>
											</li>
										</ul>
									</li>
									<li style="list-style-type: none;"><label for="candidate_veteran_active"><input style="white-space: nowrap !important; display: -moz-inline-stack; display: inline-block !important; height: auto !important; width: auto !important" type="checkbox" id="candidate_veteran_active" name="candidate_veteran_active" <?php echo $candidate_veteran_active; ?>> <?php _e('Active wartime or campaign badge veteran','noo')?></label></li>
									<li style="list-style-type: none;"><label for="candidate_veteran_medal"><input style="white-space: nowrap !important; display: -moz-inline-stack; display: inline-block !important; height: auto !important; width: auto !important" type="checkbox" id="candidate_veteran_medal" name="candidate_veteran_medal" <?php echo $candidate_veteran_medal; ?>> <?php _e('Armed forces service medal veteran','noo')?></label></li>
								</ul>
							</div>
							<div class="protected_veteran_definition">
								<h6>Protected Veteran Classifications</h6>
								<ul>
									<li><strong>Disabled Veteran</strong> - is one of the following:
										<ul>
											<li>A veteran of the U.S. Military ground, naval or air service who is entitled to compensation (or who, but for the receipt of military
 retire pay, would be entitle to compensation) under laws administered by the Secretary of Veterans Affairs; or
											</li>
											<li>A person who was discharged or released from active duty because of a service-connected disability.
											</li>
										</ul>
									</li>
									<li><strong>Recently Separated Veteran</strong> - any veteran during the three-year period beginning on the date of such veteran's discharge or release
from active duty in the U.S. Military ground, naval or air service.</li>
									<li><strong>Active Duty Wartime or Campaign Badge Veteran</strong> - a veteran who served on active duty in the U. S. Military ground, naval, or air
service during a war, or in a campaign or expedition for which a campaign badge has been authorized under the laws administered by the
Department of Defense.</li>
									<li><strong>Armed Forces Service Medal Veteran</strong> - a veteran who, while serving on active duty in the U. S. Military ground, naval, or air service,
participated in a United States military operation for which an Armed Forces service medal was awarded pursuant to Executive Order 12985.</li>
								</ul>

								<p>Protected veterans may have additional rights under USERRA - the Uniformed Services Employment and Reemployment Rights Act. In particular, if you were absent from employment in order to perform service in the uniformed service, you may be entitled to be reemployed by your employer in the position you would have obtained with reasonable certainty if not for the absence due to service. For more information, call the U. S. Department of Labor's Veterans Employment and Training Service (VETS), toll-free, at 1-866-4-USA-DOL.</p>

							</div>
						</div>
					</div> <!-- End of Page 4 -->
					<div class="app_form" id="page5">
						<h5 class="modal-title" id="applyJobModalLabel"><?php esc_html_e('Voluntary Self-Idenfication of Disability','noo')?></h5>
						<div>
							<p>To help us measure how well we are doing, we are asking you to tell us if you have a disability or if you every had a disability.  Completeing this form is voluntary, but we hope that you will choose to fill it out.  If you are applying for a job, any answer you give will be kept private and will not be used against you in any way.</p>
							<p>You are considered to have a disability if you have a physical or mental impairment or medical condition that substantially limits a major life activity, or if you have a history or record of such an impairment or medical condition.</p>
							<br/>
							<p>Disabilities include, but are not limited to:
								<ul>
									<li>Blindness</li>
									<li>Deafness</li>
									<li>Cancer</li>
									<li>Diabetes</li>
									<li>Epilepsy</li>
									<li>Autisum</li>
									<li>Cerebral palsy</li>
									<li>HIV/AIDS</li>
									<li>Schizophrenia</li>
									<li>Muscular dystrophy</li>
									<li>Bipolar disorder</li>
									<li>Major depression</li>
									<li>Multiple scierosis (MS)</li>
									<li>Missing limbs or partially missing limbs</li>
									<li>Post-traumatic stress disorder (PTSD)</li>
									<li>Obsessive compulsive disorder</li>
									<li>Impairments requiring the use of a wheelchair</li>
									<li>Intellectual disability (previously called mental retardation)</li>
								</ul>

							</p>
						</div>
						<div class="form-group">
							<label for="candidate_disability" class="control-label"><?php _e('Candidate Disability','noo')?></label>
							<div class="form-control-flat">
	              				<?php $subease_ui_helper->display_select($subease_ui_helper::$candidate_disability_lookup,"candidate_disability",$candidate_disability,true,'Select Disability Status','jform-validate','autofocus required')?>
	              			</div>
						</div>
					</div> <!-- End of Page 5 -->

<script>
jQuery(document).ready(function(){
	var getUrlParameter = function getUrlParameter(sParam) {
	    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
	        sURLVariables = sPageURL.split('&'),
	        sParameterName,
	        i;

	    for (i = 0; i < sURLVariables.length; i++) {
	        sParameterName = sURLVariables[i].split('=');

	        if (sParameterName[0] === sParam) {
	            return sParameterName[1] === undefined ? true : sParameterName[1];
	        }
	    }
	};
	var fast_apply = getUrlParameter('fast_apply');

	jQuery('.app_form').each(function(e) {
        if (e != 0) {
        	jQuery(this).hide();
        } else {
        	jQuery("#prev").hide();
        	if (!fast_apply || fast_apply == 0) {
			jQuery("#apply").hide();
		}
        	jQuery(".ethnic_definition").hide();
        	jQuery(".protected_veteran_definition").hide();
        }
    });

	jQuery('#ethnic_defs').on('click', function() {
		jQuery(".ethnic_definition").toggle();
		window.scrollTo(0,0);
		jQuery("#candidate_ethnicity").focus();
	});

	jQuery('#protected_vets_defs').on('click', function() {
		jQuery(".protected_veteran_definition").toggle();
		window.scrollTo(0,0);
		jQuery("#candidate_veteran").focus();
	});


 	jQuery.validator.addMethod("cRequired", jQuery.validator.methods.required, "Field is requiredX");
 	jQuery.validator.addMethod("chkRequired", function(element){
		if ( jQuery('#candidate_veteran').val() === 'protected' ) {
			var chk1 = jQuery("#candidate_veteran_disabled").is(':checked');
			var chk2 = jQuery("#candidate_veteran_separated").is(':checked');
			var chk3 = jQuery("#candidate_veteran_active").is(':checked');
			var chk4 = jQuery("#candidate_veteran_medal").is(':checked');
			return (chk1 || chk2 || chk3 || chk4);
		} else {
			return true;
		}
	  	},  "Please select one of the checkboxes below");



	jQuery("#next").click(function(){
		var valid_form = true;
		jQuery('.app_form:visible .jform-validate').each(function(){
			jQuery(this).validate({
				onkeyup: false,
				onfocusout: false,
				onclick: false,
				errorClass: "jform-error",
				validClass: "jform-valid",
				errorElement: "span",
				ignore: ":hidden:not(.ignore-valid)",
				errorPlacement: function(error, element) {
					if ( element.is( ':radio' ) || element.is( ':checkbox' ) || element.is( ':file' ) )
						error.appendTo( element.parent().parent() );
					else
						error.appendTo( element.parent());

					console.log("Sending Error");
				}
			});

		 	jQuery('#candidate_veteran_discharged').rules( "add", {
	 		  required: "#candidate_veteran_separated:checked",
	 		  date: true,
	 		  messages: {
	 		    required: "Field Required if Recently Separated",
	 		    date: "Please enter a valid date"
	 		  }
	 		});

		 	jQuery('#candidate_veteran_separated').rules( "add", {
		 		  required: function(element){
		 	            return jQuery("#candidate_veteran_discharged").val()!="";
		 	        },
		 		  messages: {
		 		    required: "Discharge Date Required"
		 		  }
		 	});
		 	jQuery( "#candidate_veteran" ).rules( "add", { cRequired: true, chkRequired: true });

			if (jQuery(this).valid() == false) {
				valid_form=false;
			}
		});

		if (valid_form == true) {

	        if ((jQuery(".app_form:visible").next().next().length != 0) && (jQuery(".app_form:visible").next().next().attr('class') == 'app_form')) {
	            jQuery(".app_form:visible").next().show().prev().hide();
	        } else {
	            jQuery("#next").hide();
	            jQuery(".app_form:visible").next().show().prev().hide();
	            jQuery("#apply").show();
	        }
	        jQuery("#prev").show();
	        window.scrollTo(0,0);
		}
        return false;
    });

    jQuery("#prev").click(function(){
		jQuery("#candidate_veteran_discharged").validate({
			  rules : {
				  candidate_veteran_discharged:{
					  required:"#candidate_veteran_separated:checked",
					  date: true
				  }
	 		  }
		});
		var valid_form = true;
		jQuery('.app_form:visible .jform-validate').each(function(){
			jQuery(this).validate({
				onkeyup: false,
				onfocusout: false,
				onclick: false,
				errorClass: "jform-error",
				validClass: "jform-valid",
				errorElement: "span",
				ignore: ":hidden:not(.ignore-valid)",
				errorPlacement: function(error, element) {
					if ( element.is( ':radio' ) || element.is( ':checkbox' ) || element.is( ':file' ) )
						error.appendTo( element.parent().parent() );
					else
						error.appendTo( element.parent());

					console.log("Sending Error");
				}
			});

		 	jQuery('#candidate_veteran_discharged').rules( "add", {
		 		  required: "#candidate_veteran_separated:checked",
		 		  date: true,
		 		  messages: {
		 		    required: "Field Required if Recently Separated",
		 		    date: "Please enter a valid date"
		 		  }
		 		});

		 	jQuery('#candidate_veteran_separated').rules( "add", {
			 		  required: function(element){
			 	            return jQuery("#candidate_veteran_discharged").val()!="";
			 	        },
			 		  messages: {
			 		    required: "Discharge Date Required"
			 		  }
			 	});
		 	jQuery( "#candidate_veteran" ).rules( "add", { cRequired: true, chkRequired: true });


			if (jQuery(this).valid() == false) {
				valid_form=false;
			}
		});

		if (valid_form == true) {
	        if ((jQuery(".app_form:visible").prev().prev().length != 0) && (jQuery(".app_form:visible").prev().prev().attr('class') == 'app_form')) {
	        	jQuery(".app_form:visible").prev().show().next().hide();
	            jQuery("#apply").hide();
	        } else {
	            jQuery("#prev").hide();
	            jQuery(".app_form:visible").prev().show().next().hide();
	        }
	        jQuery("#next").show();
	        window.scrollTo(0,0);
		}
        return false;
    });
});
</script>
