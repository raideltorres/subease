<?php
// $candidate_id = isset($_GET['candidate_id']) ? absint($_GET['candidate_id']) : get_current_user_id();
// $candidate = !empty($candidate_id) ? get_userdata($candidate_id) : null;

$current_user = wp_get_current_user();

$subease_ui_helper = new Subease_UI_Element_Helper();

$first_name = empty( $current_user->user_firstname  ) ? '' : $current_user->user_firstname ;
$last_name  = empty( $current_user->user_lastname  ) ? '' : $current_user->user_lastname ;
$name       = empty( $current_user->display_name ) ? $current_user->user_login : $current_user->display_name;
$email      = $current_user->user_email;
$latlon_arr = get_the_author_meta( 'latlon', $current_user->ID);

if (isset($latlon_arr['lat'])) {
	 $latlon_str = number_format($latlon_arr['lat'],7,'.','');
} else {
	$latlon_str = '';
}
if (isset($latlon_arr['lon'])) {
	if ( !empty ($latlon_str)) {
		$latlon_str .= ', ';
	}
	$latlon_str .= number_format($latlon_arr['lon'],7,'.','');
}

// if ( ! delete_user_meta($current_user->ID, 'phone_type') ) {
//   echo "<pre>Error with delete_user_meta</pre>";
// }


?>
<div class="candidate-profile-form row">
	<div class="col-sm-6">
		<!-- <div class="form-group">
			<label for="first_name" class="col-sm-4 control-label"><?php _e('First Name','noo')?></label>
			<div class="col-sm-8">
		    	<input type="text" class="form-control" required id="first_name" value="<?php echo esc_attr($first_name)?>" name="first_name" placeholder="<?php echo esc_attr__('Your first name','noo')?>">
		    </div>
		</div>
		<div class="form-group">
			<label for="last_name" class="col-sm-4 control-label"><?php _e('Last Name','noo')?></label>
			<div class="col-sm-8">
		    	<input type="text" class="form-control" required id="last_name" value="<?php echo esc_attr($last_name)?>" name="last_name" placeholder="<?php echo esc_attr__('Your last name','noo')?>">
		    </div>
		</div> -->
		<div class="form-group">
			<label for="name" class="col-sm-4 control-label"><?php _e('Full Name','noo')?></label>
			<div class="col-sm-8">
		    	<input type="text" class="form-control" required id="name" value="<?php echo esc_attr($name)?>" name="name" placeholder="<?php echo esc_attr__('Your name','noo')?>">
		    </div>
		</div>
		<div class="form-group">
			<label for="email" class="col-sm-4 control-label"><?php _e('Email','noo')?></label>
			<div class="col-sm-8">
		    	<input type="email" class="form-control" required id="email" value="<?php esc_attr_e($email)?>" name="email" placeholder="<?php echo esc_attr__('Your email','noo')?>">
		    </div>
		</div>
		<div class="form-group">
			<label for="current_job" class="col-sm-4 control-label"><?php _e('Current Job','noo')?></label>
			<div class="col-sm-8">
		    	<input type="text" class="form-control" id="current_job" value="<?php esc_attr_e(get_user_meta( $current_user->ID, 'current_job', true ));?>" name="current_job">
		    </div>
		</div>
		<div class="form-group">
			<label for="current_company" class="col-sm-4 control-label"><?php _e('Current Company','noo')?></label>
			<div class="col-sm-8">
		    	<input type="text" class="form-control" id="current_company" value="<?php esc_attr_e(get_user_meta( $current_user->ID, 'current_company', true ));?>" name="current_company">
		    </div>
		</div>
		<div class="form-group">
			<label for="birthday" class="col-sm-4 control-label"><?php _e('Birthday','noo')?></label>
			<div class="col-sm-8">
		    	<input type="text" class="form-control jform-datepicker" id="birthday" value="<?php esc_attr_e(get_user_meta( $current_user->ID, 'birthday', true ));?>" name="birthday">
		    </div>
		</div>

		<div class="form-group">
			<label for="address" class="col-sm-4 control-label"><?php _e('Address','noo')?></label>
			<div class="col-sm-8">
		    	<input type="text" class="form-control" id="address" value="<?php esc_attr_e(get_user_meta( $current_user->ID, 'address', true ));?>" name="address">
		    </div>
		</div>

		<?php if (is_administrator() == true) {?>
			<div class="form-group">
				<label for="latlon" class="col-sm-4 control-label"><?php _e('Lat/Lon','noo')?></label>
				<div class="col-sm-8">
					<div class="form-control-flat" style="width: 200px;">
						<input type="text" readonly class="form-control" id="latlon" value="<?php esc_attr_e( $latlon_str );?>" name="latlon">
					</div>
			    </div>
			</div>
		<?php } ?>

<!--
	<div class="form-group">
		<label for="city" class="col-sm-4 control-label"><?php _e('City','noo')?></label>
		<div class="col-sm-8">
				<input type="text" class="form-control" id="city" value="<?php esc_attr_e(get_user_meta( $current_user->ID, 'city', true ));?>" name="city">
			</div>
	</div>
	<div class="form-group">
		<label for="state" class="col-sm-4 control-label"><?php _e('State','noo')?></label>
		<div class="col-sm-8">
			<div class="form-control-flat" style="width: 200px;">
        <?php $subease_ui_helper->display_state_select('state', get_user_meta( $current_user->ID, 'state', true)); ?>
				<i  class="fa fa-caret-down"></i>
			</div>
		</div>
	</div>
 -->

		<div class="form-group">
			<label for="phone" class="col-sm-4 control-label"><?php _e('Phone Number','noo')?></label>
			<div class="col-sm-8">
		    	<input type="text" class="form-control" id="phone" value="<?php esc_attr_e(get_user_meta( $current_user->ID, 'phone', true ));?>" name="phone">
		    </div>
		</div>
		<!-- Subease: New field phone_type -->
		<div class="form-group">
			<label for="phone_type" class="col-sm-4 control-label"><?php _e('Phone Type','noo')?></label>
			<div class="col-sm-8">
				<div class="form-control-flat" style="width: 200px;">
					  <?php $subease_ui_helper->display_phone_type_select('phone_type', get_user_meta( $current_user->ID, 'phone_type', true)); ?>
					<i  class="fa fa-caret-down"></i>
				</div>
		    </div>
		</div>
		<div class="form-group">
			<label for="allow_sms" class="col-sm-4 control-label"><?php _e('Allow SMS','noo')?></label>
			<div class="col-sm-8">
				<div class="" style="width: 200px;">
					  <?php $subease_ui_helper->display_allow_sms_radio('allow_sms', get_user_meta( $current_user->ID, 'allow_sms', true)); ?>
				</div>
		  </div>
		</div>
		<div class="form-group">
			<label for="facebook" class="col-sm-4 control-label"><?php _e('Facebook','noo')?></label>
			<div class="col-sm-8">
		    	<input type="text" class="form-control" id="facebook" value="<?php esc_attr_e(get_user_meta( $current_user->ID, 'facebook', true)); ?>"  name="facebook" placeholder="<?php echo esc_attr__('http://','noo')?>">
		    </div>
		</div>
		<div class="form-group">
			<label for="twitter" class="col-sm-4 control-label"><?php _e('Twitter','noo')?></label>
			<div class="col-sm-8">
		    	<input type="text" class="form-control" id="twitter" value="<?php esc_attr_e(get_user_meta( $current_user->ID, 'twitter', true)); ?>"  name="twitter" placeholder="<?php echo esc_attr__('http://','noo')?>">
		    </div>
		</div>
		<div class="form-group">
			<label for="linkedin" class="col-sm-4 control-label"><?php _e('LinkedIn','noo')?></label>
			<div class="col-sm-8">
		    	<input type="text" class="form-control" id="linkedin" value="<?php esc_attr_e(get_user_meta( $current_user->ID, 'linkedin', true)); ?>"  name="linkedin" placeholder="<?php echo esc_attr__('http://','noo')?>">
		    </div>
		</div>
		<div class="form-group">
			<label for="behance" class="col-sm-4 control-label"><?php _e('Behance','noo')?></label>
			<div class="col-sm-8">
		    	<input type="text" class="form-control" id="behance" value="<?php esc_attr_e(get_user_meta( $current_user->ID, 'behance', true)); ?>"  name="behance" placeholder="<?php echo esc_attr__('http://','noo')?>">
		    </div>
		</div>
		<div class="form-group">
			<label for="instagram" class="col-sm-4 control-label"><?php _e('Instagram','noo')?></label>
			<div class="col-sm-8">
		    	<input type="text" class="form-control" id="instagram" value="<?php esc_attr_e(get_user_meta( $current_user->ID, 'instagram', true)); ?>"  name="instagram" placeholder="<?php echo esc_attr__('http://','noo')?>">
		    </div>
		</div>

	</div>

	<div class="col-sm-6">
		<div class="form-group">
		    <label for="description" class="control-label"><?php _e('Introduce Yourself','noo')?> <small><?php _e('(Optional)','noo')?></small></label>
		    <textarea class="form-control form-control-editor" id="description" name="description" rows="8"><?php esc_html_e( $current_user->description ); ?></textarea>
		</div>
		<div class="form-group">
			<label for="profile_image" class="col-sm-3 control-label"><?php _e('Profile Image','noo')?></label>
			<div class="col-sm-9">
				<?php
					$profile_image = ($current_user->ID ? get_user_meta( $current_user->ID, 'profile_image', true) : '');
				noo_image_upload_form_field( 'profile_image', $profile_image );
				?>
			</div>
			<!--
			<label for="profile_image" class="col-sm-3 control-label"><?php // _e('Profile Video','noo')?></label>
			<div class="col-sm-9"> <?php // echo do_shortcode("[vidrack]", false); ?></div>
 			-->
			<label for="worker_status" class="col-sm-3 control-label"><?php _e('Status','noo')?></label>
			<div class="form-group">
				<div class="col-sm-8"><?php $subease_ui_helper->display_worker_status_radio('worker_status', get_user_meta( $current_user->ID, 'worker_status', true)); ?></div>
			</div>

			<label for="hide_profile" class="col-sm-3 control-label"><?php _e('Hide Profile','noo')?></label>
			<div class="form-group">
				<div class="col-sm-8"><?php $subease_ui_helper->display_hide_profile_radio('hide_profile', get_user_meta( $current_user->ID, 'hide_profile', true)); ?></div>
			</div>
		</div>
	</div>
</div>
