<?php
if( Noo_Resume::enable_resume_detail() ) :
	$resume_id = isset($_GET['resume_id']) ?absint($_GET['resume_id']) : 0;
	$resume = $resume_id ? get_post($resume_id) : '';
	$fields = jm_get_resume_custom_fields( true );

	$education = array();
	$enable_education = Noo_Resume::get_setting('enable_education', '1');
	if( $enable_education ) {
		$education['school'] = noo_json_decode( noo_get_post_meta( $resume_id, '_education_school', '' ) );
		$education['qualification'] = noo_json_decode( noo_get_post_meta( $resume_id, '_education_qualification', '' ) );
		$education['date'] = noo_json_decode( noo_get_post_meta( $resume_id, '_education_date', '' ) );
		$education['note'] = noo_json_decode( noo_get_post_meta( $resume_id, '_education_note', '' ) );
	}

	?>
	<?php do_action('noo_post_resume_detail_before'); ?>
	<div class="resume-form">
		<div class="resume-form-detail">
		<?php if( $enable_education ) : ?>
			<div class="form-group row">
				<label class="col-sm-3 control-label"><?php _e('Education','noo')?></label>
				<div class="col-sm-9">
					<div class="noo-metabox-addable" data-name="_education" >
						<div class="noo-addable-fields">
							<input type="hidden" value="" name="_education_school" />
							<input type="hidden" value="" name="_education_qualification" />
							<input type="hidden" value="" name="_education_date" />
							<input type="hidden" value="" name="_education_note" />
							<?php
							if( isset( $education['school'] ) && is_array( $education['school'] ) && count($education['school']) ) :
								foreach( $education['school'] as $index => $school ) :
								?>
									<div class="fields-group">
										<input type="text" class="form-control" placeholder="<?php _e('School name', 'noo'); ?>" name='_education_school[]' value="<?php echo esc_attr($education['school'][$index]); ?>" />
										<input type="text" class="form-control" placeholder="<?php _e('Qualification(s)', 'noo'); ?>" name='_education_qualification[]' value="<?php echo esc_attr($education['qualification'][$index]); ?>" />
										<input type="text" class="form-control" placeholder="<?php _e('Start/end date', 'noo'); ?>" name='_education_date[]' value="<?php echo esc_attr($education['date'][$index]); ?>" />
										<textarea class="form-control form-control-editor ignore-valid" id="_education_note" name="_education_note[]" rows="5" placeholder="<?php _e('Note', 'noo'); ?>"><?php echo html_entity_decode($education['note'][$index])?></textarea>
									</div>
								<?php endforeach; ?>
							<?php else : ?>
								<div class="fields-group">
									<input type="text" class="form-control" placeholder="<?php _e('School name', 'noo'); ?>" name='_education_school[]' />
									<input type="text" class="form-control" placeholder="<?php _e('Qualification(s)', 'noo'); ?>" name='_education_qualification[]' />
									<input type="text" class="form-control" placeholder="<?php _e('Start/end date', 'noo'); ?>" name='_education_date[]' />
									<textarea class="form-control form-control-editor ignore-valid" id="_education_note" name="_education_note[]" rows="5" placeholder="<?php _e('Note', 'noo'); ?>"></textarea>
								</div>
							<?php endif; ?>
						</div>
						<div class="noo-addable-actions">
							<a href="javascript:void()" class="noo-clone-fields pull-left" data-template="<div class='fields-group'><input type='text' class='form-control' placeholder='<?php _e('School name', 'noo'); ?>' name='_education_school[]' /><input type='text' class='form-control' placeholder='<?php _e('Qualification(s)', 'noo'); ?>' name='_education_qualification[]' /><input type='text' class='form-control' placeholder='<?php _e('Start/end date', 'noo'); ?>' name='_education_date[]' /><textarea class='form-control form-control-editor ignore-valid' id='_education_note' name='_education_note[]' rows='5' placeholder='<?php _e('Note', 'noo'); ?>'></textarea></div>">
								<i class="fa fa-plus-circle text-primary"></i>
								<?php _e('Add Education', 'noo'); ?>
							</a>
							<a href="javascript:void()" class="noo-remove-fields pull-right">
								<i class="fa fa-times-circle"></i>
								<?php _e('Delete', 'noo'); ?>
							</a>
						</div>
					</div>
				</div>
			</div>
		<?php endif; ?>


		<div class="form-group row">
			<label class="col-sm-3 control-label"><?php _e('Associations','noo')?></label>
			<div class="col-sm-9">
				<div class="noo-metabox-addable" data-name="_associations" >
					<div class="noo-addable-fields"><?php subease_resume_render_form_field_no_label( $fields['_associations'], $resume_id ); ?></div>
				</div>
			</div>
		</div>

		<div class="form-group row">
			<label class="col-sm-3 control-label"><?php _e('Certifications','noo')?></label>
			<div class="col-sm-9">
				<div class="noo-metabox-addable" data-name="_certifications" >
					<div class="noo-addable-fields"><?php subease_resume_render_form_field_no_label( $fields['_certifications'], $resume_id ); ?></div>
				</div>
			</div>
		</div>






<!--
		<div class="form-group row">
			<label for="url_video" class="col-sm-5 control-label"><?php _e( 'Video URL', 'noo' ); ?></label>
			<div class="col-sm-7">
		    	<input type="text" value="<?php echo noo_get_post_meta( $resume_id, '_noo_url_video' ) ?>" class="form-control" id="url_video"  name="url_video" placeholder="<?php _e( 'Youtube or Vimeo link', 'noo' ); ?>" >
		    </div>
		</div>
 -->
		</div>
	</div>
	<?php do_action('noo_post_resume_detail_after'); ?>
<?php endif; ?>