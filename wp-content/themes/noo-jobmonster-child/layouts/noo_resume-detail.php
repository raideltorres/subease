<?php
wp_enqueue_script('noo-timeline-vendor');
wp_enqueue_script('noo-timeline');

$subease_ui_helper = new Subease_UI_Element_Helper();

$enable_education = Noo_Resume::get_setting('enable_education', '1');
$enable_experience = Noo_Resume::get_setting('enable_experience', '1');
$enable_skill = Noo_Resume::get_setting('enable_skill', '1');
$enable_video = (bool) Noo_Resume::get_setting('enable_video', '');
$display_video_resume = Noo_Resume::get_setting('display_video_resume', 'before_content');
$hide_profile = isset( $hide_profile ) ? $hide_profile : false;

// SUBEASE: Get the application_id from URL so we can pull that date_format
$application_id = isset($_GET['application_id']) ? absint($_GET['application_id']) : 0;
if (isset ($application_id) && ($application_id > 0)) {
	$candidate_work_authorization = noo_get_post_meta($application_id,'_candidate_work_authorization');
	$candidate_ethnicity = noo_get_post_meta($application_id,'_candidate_ethnicity');
	$candidate_gender = noo_get_post_meta($application_id,'_candidate_gender');
	$candidate_veteran = noo_get_post_meta($application_id,'_candidate_veteran');
	$candidate_veteran_disabled = noo_get_post_meta($application_id,'_candidate_veteran_disabled');
	$candidate_veteran_separated = noo_get_post_meta($application_id,'_candidate_veteran_separated');
	$candidate_veteran_discharged = noo_get_post_meta($application_id,'_candidate_veteran_discharged');
	$candidate_veteran_active = noo_get_post_meta($application_id,'_candidate_veteran_active');
	$candidate_veteran_medal = noo_get_post_meta($application_id,'_candidate_veteran_medal');
	$candidate_disability = noo_get_post_meta($application_id,'_candidate_disability');
	// print "<pre>application_id="; print_r($application_id, FALSE); print " candidate_ethnicity="; print_r ($candidate_ethnicity.FALSE); print "</pre>";
}

if (! function_exists('subease_render_element_read_only')) :

    function subease_render_element_read_only($field = array(), $resume_id = '') {
        if (! isset($field['name']) || empty($field['name']))
            return;
        $field['type'] = isset($field['type']) ? $field['type'] : 'text';
        $id = '_noo_resume_field_' . sanitize_title($field['name']);
        if (isset($field['is_default'])) {
            if (isset($field['is_disabled']) && ($field['is_disabled'] == 'yes'))
                return;
            $id = $field['name'];
        }
        $value = $resume_id ? noo_get_post_meta($resume_id, $id, '') : '';
        $label = isset($field['label_translated']) ? $field['label_translated'] : $field['label'];

        $value = ! is_array($value) ? trim($value) : $value;
        if (empty($value))
            return;

        $value =  noo_convert_custom_field_value ($field, $value);
        if (in_array($field['type'], array( 'multiple_select', 'checkbox', 'radio' ))) {
            // $value = implode(', ', $value);
            $newvalue = '';
            foreach ($value as $v) {
                $newvalue .= esc_html($v);
                $newvalue .= '<br/>';
            }
            $value = $newvalue;
        }

/*         if ($field['type'] === 'checkbox' || $field['type'] === 'radio' || $field['type'] === 'multiple_select') {
            if ( ! is_array($value) ) {
                $value = array ($value);
            }
            // $value = ! is_array($value) ? array( $value ) : $value;
            // $value = implode(', ', $value);
            $newvalue = '';
            foreach ($value as $v) {
                $newvalue .= esc_html($v);
                $newvalue .= '<br/>';
            }
            $value = $newvalue;

        } */

        if (! empty($value)) {
            echo '<li><span class="label-' . $id . '">';
            esc_html_e($label);
            echo '</span><span>';
            echo $value;
            echo '</span></li>';
        }
    }

endif;

while ($query->have_posts()): $query->the_post(); global $post;
	if( Noo_Resume::can_view_resume( $post->ID ) ) :
		$resume_id = $post->ID;
		$fields = jm_get_resume_custom_fields();

		$education					= array();
		if( $enable_education ) {
			$education['school']		= noo_json_decode( noo_get_post_meta( $resume_id, '_education_school' ) );
			$education['qualification']	= noo_json_decode( noo_get_post_meta( $resume_id, '_education_qualification' ) );
			$education['date']			= noo_json_decode( noo_get_post_meta( $resume_id, '_education_date' ) );
			$education['note']			= noo_json_decode( noo_get_post_meta( $resume_id, '_education_note' ) );
		}

		$experience					= array();
		if( $enable_experience ) {
			$experience['employer']		= noo_json_decode( noo_get_post_meta( $resume_id, '_experience_employer' ) );
			$experience['job']			= noo_json_decode( noo_get_post_meta( $resume_id, '_experience_job' ) );
			$experience['date']			= noo_json_decode( noo_get_post_meta( $resume_id, '_experience_date' ) );
			$experience['note']			= noo_json_decode( noo_get_post_meta( $resume_id, '_experience_note' ) );
			$experience['project_image']	= noo_json_decode( noo_get_post_meta( $resume_id, '_experience_project_image' ) );
		}

		$skill						= array();
		if( $enable_skill ) {
			$skill['name']				= noo_json_decode( noo_get_post_meta( $resume_id, '_skill_name' ) );
			$skill['percent']			= noo_json_decode( noo_get_post_meta( $resume_id, '_skill_percent' ) );
		}


?>
		<article id="post-<?php the_ID(); ?>" class="resume">
			<?php if( !apply_filters( 'jm_resume_hide_candidate_contact', $hide_profile, $resume_id ) ) noo_get_layout('resume_candidate_profile'); ?>
			<div class="resume-content">
				<div class="row">
					<div class="col-md-12">
						<div class="resume-desc">
							<div class="resume-general row">
								<div class="col-sm-3">
									<h3 class="title-general">
									<span><?php _e('General Infomation','noo');?></span>
									</h3>
								</div>
								<div class="col-sm-9">
									<ul>
										<?php
										if($fields) : foreach ($fields as $field) :
											if( !isset( $field['name'] ) || empty( $field['name'] )) continue;
											$field['type'] = isset( $field['type'] ) ? $field['type'] : 'text';
											$id = '_noo_resume_field_'.sanitize_title($field['name']);
											if( isset( $field['is_default'] ) ) {
												if( isset( $field['is_disabled'] ) && ($field['is_disabled'] == 'yes') )
													continue;
												$id = $field['name'];
											}
											$value = $resume_id ? noo_get_post_meta($resume_id, $id, '') : '';
											$label = isset( $field['label_translated'] ) ? $field['label_translated'] : $field['label'];
											if( $id == '_job_category' ) {
												if( !empty( $value ) ) {
													$value = noo_json_decode($value);
													$category_terms = empty( $value ) ? array() : get_terms( 'job_category', array('include' => array_merge($value, array(-1)), 'hide_empty' => 0, 'fields' => 'names') );
													$value = implode(', ', $category_terms);
												}
											} elseif( $id == '_job_location' ) {
												if( !empty( $value ) ) {
													$value = noo_json_decode($value);
													$location_terms = empty( $value ) ? array() : get_terms( 'job_location', array('include' => array_merge($value, array(-1)), 'hide_empty' => 0, 'fields' => 'names') );
													$value = implode(', ', $location_terms);
												}
											} elseif( $id == '_job_type' ) {
													if( !empty( $value ) ) {
															$value = "";
															$resume_job_types = subease_get_job_type_from_resume( $post );
															if( !empty( $resume_job_types ) ) {
																foreach ($resume_job_types as $resume_job_type) {
																	$value .= '<a href="'.get_term_link($resume_job_type['object'],'job_type').'" style="white-space: nowrap; margin-right: 3px; color: '.$resume_job_type['color'].'"><i class="fa fa-bookmark"></i>' .$resume_job_type['object']->name. '</a>';
																}
															}
													}
											} elseif( $id == '_noo_resume_field__associations' ) {
											    // Ignore here, render below
											    continue;
											} elseif( $id == '_noo_resume_field__certifications' ) {
											    // Ignore here, render below
											    continue;
											} else {
												$value = !is_array($value) ? trim($value) : $value;
												if( empty( $value ) ) continue;

												$value = noo_convert_custom_field_value( $field, $value );
												if( in_array( $field['type'], array( 'multiple_select', 'checkbox', 'radio' ) ) ) {
													$value = implode(', ', $value);
												}

												if ( $field['type'] === 'checkbox' || $field['type'] === 'radio' || $field['type'] === 'multiple_select' ) :
													$value = !is_array( $value ) ? array( $value ) : $value;
													$value = implode(', ', $value);
												endif;
											}

											if( empty($value) ) continue;
											?>
											<li>
												<span class="label-<?php echo $id; ?>"><?php esc_html_e($label)?></span>
													<?php //echo esc_html( $value );
														echo $value;
													?>
											</li>

										<?php endforeach; endif; ?>
									</ul>
								</div>
								<div class="resume-description col-sm-offset-3 col-sm-9">
									<?php
										$url_video = $enable_video ? noo_get_post_meta($post->ID, '_noo_url_video') : '';
										if ( $display_video_resume == 'after_content' ) the_content();
										if ( !empty( $url_video ) ) :
											global $wp_embed;
											?>
											<div class="resume-video">
												<?php echo wp_oembed_get( $url_video, array( 'width' => 800 ));  ?>
											</div>
											<?php
										endif;
										if ( $display_video_resume == 'before_content' ) the_content();
									?>
								</div>
							</div>
							<?php if( $enable_experience ) : ?>
								<?php $experience['employer'] = isset( $experience['employer'] ) ? array_filter( $experience['employer'] ) : array(); ?>
								<?php if( !empty( $experience['employer'] ) ) : ?>
									<div class="resume-timeline row">
										<div class="col-md-3 col-sm-12">
											<h3 class="title-general">
												<span><?php _e('Work Experience','noo');?></span>
											</h3>
										</div>
										<div class="col-md-9 col-sm-12">
											<div id="experience-timeline" class="timeline-container experience">
												<?php $experience_count = count( $experience['employer'] );
													for( $index = 0; $index < $experience_count; $index++ ) :
														if( empty( $experience['employer'][$index] ) ) continue;
														?>
														<div class="timeline-wrapper <?php echo ( $index == ( $experience_count - 1 ) ) ? 'last' : ''; ?>">
															<div class="timeline-time"><span><?php esc_attr_e( $experience['date'][$index] ); ?></span></div>
															<dl class="timeline-series">
																<span class="tick tick-before"></span>
																<dt id="<?php echo 'experience'.$index ?>" class="timeline-event"><a><?php esc_attr_e( $experience['employer'][$index] ); ?><span class="tick tick-after"><?php esc_attr_e( $experience['job'][$index] ); ?></span></a></dt>

																<dd class="timeline-event-content" id="<?php echo 'experience'.$index.'EX' ?>">
																	<div><?php echo wpautop( html_entity_decode( $experience['note'][$index] ) ); ?></div>
																<br class="clear">
																<?php
																if (isset($experience['project_image'][$index])) {
																	$image_list = implode(",",$experience['project_image'][$index]);
																	if (! empty($image_list)) {
																		echo do_shortcode('[gallery type="rectangular" columns=4 type="thumbnail" orderby="ID ASC" size="thumbnail" id="' . $resume_id . '" include="' . $image_list . '"]' );
																	}
																}
																?>
																</dd><!-- /.timeline-event-content -->
															</dl><!-- /.timeline-series -->
														</div><!-- /.timeline-wrapper -->
												<?php endfor; ?>
											</div>
										</div>
									</div>
								<?php endif; ?>
							<?php endif; ?>


							<?php if( $enable_education ) : ?>
								<?php $education['school'] = isset( $education['school'] ) ? array_filter( $education['school'] ) : array(); ?>
								<?php if( !empty( $education['school'] ) ) : ?>
									<div class="resume-timeline row">
										<div class="col-md-3 col-sm-12">
											<h3 class="title-general">
												<span><?php _e('Education','noo');?></span>
											</h3>
										</div>
										<div class="col-md-9 col-sm-12">
											<div id="education-timeline" class="timeline-container education">
													<?php $education_count = count( $education['school'] );
													for( $index = 0; $index < $education_count; $index++ ) :
														if( empty( $education['school'][$index] ) ) continue;
														?>
														<div class="timeline-wrapper <?php echo ( $index == ( $education_count - 1 ) ) ? 'last' : ''; ?>">
															<div class="timeline-time"><span><?php esc_attr_e( $education['date'][$index] ); ?></span></div>
															<dl class="timeline-series">
																<span class="tick tick-before"></span>
																<dt id="<?php echo 'education'.$index ?>" class="timeline-event"><a><?php esc_attr_e( $education['school'][$index] ); ?><span><?php esc_attr_e( $education['qualification'][$index] ); ?></span></a></dt>
																<span class="tick tick-after"></span>
																<dd class="timeline-event-content" id="<?php echo 'education'.$index.'EX' ?>">
																	<div><?php echo wpautop( html_entity_decode( $education['note'][$index] ) ); ?></div>
																<br class="clear">
																</dd><!-- /.timeline-event-content -->
															</dl><!-- /.timeline-series -->
														</div><!-- /.timeline-wrapper -->
													<?php endfor; ?>
											</div>
										</div>
									</div>
									<div class="resume-general row">
										<div class="col-sm-3">
											<h3 class="title-general"><span></span></h3>
										</div>
										<div class="col-sm-9">
											<ul><?php subease_render_element_read_only($fields['_associations'], $resume_id); ?></ul>
										</div>
									<div class="resume-general row">
										<div class="col-sm-3">
											<h3 class="title-general"><span></span></h3>
										</div>
										<div class="col-sm-9">
											<ul><?php subease_render_element_read_only($fields['_certifications'], $resume_id); ?></ul>
										</div>
									</div>
								<?php endif; ?>
							<?php endif; ?>




							<?php if( $enable_skill ) : ?>
								<?php $skill['name'] = isset( $skill['name'] ) ? array_filter( $skill['name'] ) : array(); ?>
								<?php if( !empty( $skill['name'] ) ) : ?>
									<div class="resume-timeline row">
										<div class="col-md-3 col-sm-12">
											<h3 class="title-general">
												<span><?php _e('Summary of Skills','noo');?></span>
											</h3>
										</div>
										<div class="col-md-9 col-sm-12">
											<div id="skill" class="skill">
												<?php $skill_count = count( $skill['name'] );
													for( $index = 0; $index < $skill_count; $index++ ) :
														if( empty( $skill['name'][$index] ) ) continue;
														$skill_value = min( intval( $skill['percent'][$index] ), 100 );
														$skill_value = max( $skill_value, 0 );
														?>
													<div class="pregress-bar clearfix">
														<div class="progress_title"><span><?php esc_attr_e( $skill['name'][$index] ); ?></span></div>
														<div class="progress">
															<div aria-valuemax="100" aria-valuemin="0" aria-valuenow="70" class="progress-bar progress-bar-bg" data-valuenow="<?php esc_attr_e( $skill_value ); ?>" role="progressbar" style="width: <?php esc_attr_e( $skill_value ); ?>%;">
																<div class="progress_label" style="opacity: 1;"><span><?php esc_attr_e( $skill_value ); ?></span><?php _e('%', 'noo'); ?></div>
															</div>
														</div>
													</div>
												<?php endfor; ?>
											</div>
										</div>
									</div>
								<?php endif; ?>
							<?php endif; ?>

							<!-- Subease Additional Application Data -->
							<?php if (isset ($application_id) && ($application_id > 0)) { ?>
							<div class="resume-general row">
								<div class="col-sm-3">
								<h3 class="title-general">
								<span><?php _e('Additional Infomation','noo');?></span>
								</h3>
								</div>
								<div class="col-sm-9">
									<ul>
										<?php if ( ! empty ($candidate_work_authorization)) {?>
											<li>
												<span class="label-candidate_work_authorization"><?php esc_html_e("Work Authorization") ?></span>
												<?php echo esc_html( $subease_ui_helper->get_candidate_work_authorization_name($candidate_work_authorization) ); ?>
											</li>
										<?php } ?>
										<?php if ( ! empty ($candidate_ethnicity)) {?>
											<li>
												<span class="label-candidate_ethnicity"><?php esc_html_e("Candidate Ethnicity") ?></span>
												<?php echo esc_html( $subease_ui_helper->get_candidate_ethnicity_name ($candidate_ethnicity) ); ?>
											</li>
										<?php } ?>
										<?php if ( ! empty ($candidate_gender)) {?>
											<li>
												<span class="label-candidate_gender"><?php esc_html_e("Candidate Gender") ?></span>
												<?php echo esc_html( $subease_ui_helper->get_candidate_gender_name($candidate_gender) ); ?>
											</li>
										<?php } ?>
										<?php if ( ! empty ($candidate_disability)) {?>
											<li>
												<span class="label-candidate_gender"><?php esc_html_e("Candidate Disability") ?></span>
												<?php echo esc_html( $subease_ui_helper->get_candidate_disability_name($candidate_disability) ); ?>
											</li>
										<?php } ?>
										<?php if ( ! empty ($candidate_veteran)) {?>
											<li>
												<span class="label-candidate_veteran"><?php esc_html_e("Veteran Status") ?></span>
												<span style="font-weight:normal;width:50%;line-height:1em;">
												<?php echo esc_html( $subease_ui_helper->get_candidate_veteran_name($candidate_veteran) ); ?><?php if ($candidate_veteran == Subease_Elements::CANDIDATE_VETERAN_PROTECTED) { echo ": "; }?>

												<br/>
												<?php if ($candidate_veteran == $subease_ui_helper::CANDIDATE_VETERAN_PROTECTED) { ?>
													<ul style="list-style:disc outside none; padding: 10px 10px 10px 25px;">
														<?php if ( ! empty ($candidate_veteran_disabled)) {?>
															<li style="line-height:1em;"><?php echo esc_html( 'Disabled veteran' ); ?></li>
														<?php } ?>
														<?php if ( ! empty ($candidate_veteran_separated)) {?>
															<li style="line-height:1em;"><?php echo esc_html( 'Recently separated veteran' ); ?></li>
														<?php } ?>
														<?php if ( ! empty ($candidate_veteran_discharged)) {?>
															<li style="line-height:1em;"><?php echo esc_html( 'Date of discharge' ); echo ": " . $candidate_veteran_discharged; ?></li>
														<?php } ?>
														<?php if ( ! empty ($candidate_veteran_active)) {?>
															<li style="line-height:1em;"><?php echo esc_html( 'Active wartime or campaign badge veteran' ); ?></li>
														<?php } ?>
														<?php if ( ! empty ($candidate_veteran_medal)) {?>
															<li style="line-height:1em;"><?php echo esc_html( 'Armed forces service medal veteran' ); ?></li>
														<?php } ?>
													</ul>
												<?php } //EndIf CANDIDATE_VETERAN_PROTECTED ?>
												</span>
											</li>
										<?php } // EndIf cadidate_veteran ?>

									</ul>
								</div>
							</div>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
		</article> <!-- /#post- -->
	<?php else: ?>
		<?php
			list($title, $link) = jm_get_cannot_view_resume_message( $post->ID );
		?>
		<article id="post-<?php the_ID(); ?>" class="resume">
			<h3><?php echo $title; ?></h3>
			<?php if( !empty( $link ) ) echo $link; ?>
		</article>
	<?php endif; ?>
<?php
endwhile;
?>
<script>
	jQuery(document).ready(function() {
//		jQuery.timeliner({
//			timelineContainer:'.resume-timeline .timeline-container',
//		});
		jQuery('.venobox').venobox();
	});
	// $(document).ready(function() {
	// 	$.timeliner({
	// 		timelineContainer:'#timeline',
	// 	});
	// 	$('.venobox').venobox();
	// });

</script>
