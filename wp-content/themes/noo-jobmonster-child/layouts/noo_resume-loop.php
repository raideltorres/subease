<?php
if($wp_query->have_posts() || !$is_shortcode):
	$title = empty($title) ? __('Resumes', 'noo') : $title;

	// We need to figure out which columns to display based on query.
	//   Some columns are fixed, "avatar, Candidate Name, Resume Title, Category",
	//   others are dynamic, "Total years of experience, Lanuage, Certifications, Associations"
	$is_job_type_set = (bool) isset($_REQUEST['_job_type']) && !empty($_REQUEST['_job_type']);
	$is_experience_year_set = (bool) isset($_REQUEST['_experience_year']) && !empty($_REQUEST['_experience_year']);
	$is_language_set = (bool) isset ($_REQUEST['_language']) && !empty ($_REQUEST['_language']);
	$is_noo_resume_field__certifications_set = (bool) isset ($_REQUEST['_noo_resume_field__certifications']) && ! empty($_REQUEST['_noo_resume_field__certifications']);
	$is_noo_resume_field__associations_set = (bool) isset ($_REQUEST['_noo_resume_field__associations']) && !empty  ($_REQUEST['_noo_resume_field__associations']);

	// if (($is_noo_resume_field__associations_set) || ($is_noo_resume_field__certifications_set)) {
	    $fields = jm_get_resume_custom_fields( true );
	// }

// 	echo '<pre>is_experience_year_set=' . print_r($is_experience_year_set, true) . '</pre>';
// 	echo '<pre>is_language_set=' . print_r($is_language_set, true) .  '</pre>';
// 	echo '<pre>is_noo_resume_field__associations_set=' . print_r ($is_noo_resume_field__associations_set, true) . '</pre>';
// 	echo '<pre>is_noo_resume_field__certifications_set=' . print_r ($is_noo_resume_field__certifications_set, true) . '</pre>';
?>
	<?php // echo '<pre>' . print_r($_REQUEST,true) . '</pre>'; ?>
	<?php if(!$ajax_item || $ajax_item == null )://ajax item ?>
		<div class="resumes posts-loop" data-paginate="<?php echo $paginate; ?>">
			<div class="posts-loop-title">
				<h3><?php echo $title; ?></h3>
			</div>
			<div class="posts-loop-content resume-table">
				<table class="table">
					<thead>
						<tr>
							<th><?php _e('Candidate','noo')?></th>
							<th><?php _e('Resume Title', 'noo'); ?></th>
							<th class="hidden-xs"><?php _e('Category','noo')?></th>
							<th class="hidden-xs"><?php _e('Salary','noo')?></th>
							<?php if ($is_job_type_set): ?>  <th class="hidden-xs"><?php _e('Job Type','noo')?></th><?php endif;?>
							<?php if ($is_experience_year_set): ?>  <th class="hidden-xs"><?php _e('Years of Experience','noo')?></th><?php endif;?>
							<?php if ($is_language_set): ?>  <th class="hidden-xs"><?php _e('Languages','noo')?></th><?php endif;?>
							<?php if ($is_noo_resume_field__certifications_set): ?>  <th class="hidden-xs"><?php _e('Certifications','noo')?></th><?php endif;?>
							<?php if ($is_noo_resume_field__associations_set): ?>  <th class="hidden-xs"><?php _e('Associations','noo')?></th><?php endif;?>
						</tr>
					</thead>
					<tbody class="<?php echo $paginate; ?>-wrap">
	<?php endif; ?>

	<?php
	   $valid_resumes = (int) -1; // Number of valid resumes after filters applied.
    	// Get the list of Job Locations from Query
        if (isset($_REQUEST['_job_location']) && (isset($_REQUEST['_noo_resume_field__worker_travel_distance']))) {
            $query_travel_distance = (int) $_REQUEST['_noo_resume_field__worker_travel_distance'];
	        $searched_job_locations = ! is_array($_REQUEST['_job_location']) ? array($_REQUEST['_job_location']) : $_REQUEST['_job_location'];
	        $searched_job_loc_latlon = array();
	        // Get the LatLon (try DB first then Google) for each Job Locations
	        foreach ($searched_job_locations as $job_loc ) {
	           $searched_job_loc_latlon[$job_loc] = get_job_location_latlon($job_loc);
	        }

	        $valid_resumes = (int) 0; // Doing a filter, so set starting point to 0
        }


	?>
	<?php if($wp_query->have_posts()):?>
		<?php if( Noo_Resume::can_view_resume(null,true) ):?>
			<?php while ($wp_query->have_posts()): $wp_query->the_post();global $post;?>
						<?php
							$candidate_avatar	= '';
							$candidate_name	= '';
							if( !empty( $post->post_author ) ) :
								$candidate_avatar 	= noo_get_avatar( $post->post_author, 40 );
								$candidate = get_user_by( 'id', $post->post_author );

								// echo "<pre>" . print_r($post, true) . "</pre>";
								if (Subease_Elements::is_hide_profile_all($candidate->ID)) {
								    // Skip post, profile is hidden
								    continue;
								}

								// Now calculate distance between Job Locations in Query and Resume Worker Distance Travel and see if matches
								// Get the WorkerID of the Resume ($candidate->ID)
								// Get the Resume->Travel Distance
								// Get the Worker->Address_Lat_lon
								// Foreach Job Location
								//   Calculate distance between Query Job Location and Worker Address
								//   If Distance <= Query-Travel Distance AND Distance <= Resume Travel Distance
								//     Allow resume to be displayed
								if (isset($_REQUEST['_job_location']) && (isset($_REQUEST['_noo_resume_field__worker_travel_distance']))) {
    								$resume_latlon = get_the_author_meta( 'latlon', $candidate->ID);
    								if (empty($resume_latlon)) {
    								    // no latlon set for resume/worker, dump out
    								    continue;
    								}
    								$resume_travel_distance = (int) noo_get_post_meta($post->ID,'_noo_resume_field__worker_travel_distance','');
    								foreach ($searched_job_loc_latlon as $job_loc ) {
    								    $distance = geo_distance($job_loc['lat'], $job_loc['lon'], $resume_latlon['lat'],$resume_latlon['lon']);
    								    if ((round($distance) <= $query_travel_distance) && (round($distance) <= $resume_travel_distance)) {
    								        // Allow to be added to the list
    								        $valid_resumes++;
    								        continue;
    								    } else {
    								        // Skip over, didn't match the search query on distance from job location
    								        continue 2;
    								    }
    								}
								}


								if (Subease_Elements::is_hide_profile_current($candidate->ID)) {
								    // Now check if candidate's current employer matches the user
								    $candidate_current_company = preg_replace('/\s+/', '', get_user_meta( $candidate->ID, 'current_company', true ));
								    $company_id = get_user_meta(get_current_user_id(),'employer_company',true);
								    $company = get_post($company_id);
								    $company_name = preg_replace('/\s+/', '', $company->post_title);

								    if (strcasecmp($candidate_current_company, $company_name) == 0 ) {
								        // Skip post, profile wants to be hidden from company
								        continue;
								    }
								}

								$candidate_name = $candidate->display_name;
								$candidate_link = esc_url( apply_filters( 'noo_resume_candidate_link', get_the_permalink(), $post->ID, $post->post_author ) );
								$worker_status = Subease_UI_Element_Helper::get_worker_status_html(get_the_author_meta( 'worker_status', $candidate->ID));

							?>
    				<tr>
    					<td class="resume-name">
							<div class="loop-item-wrap">
							    <div class="item-featured">
									<a href="<?php echo $candidate_link; ?>">
										<?php echo $candidate_avatar;?>
									</a>
								</div>

								<div class="loop-item-content">
									<h2 class="loop-item-title">
										<a href="<?php echo $candidate_link;; ?>" ><?php echo esc_html( $candidate_name ); ?></a>
									</h2>
								</div>
							</div>
							<div style="font-size: 10px; display: block !important;"><?php echo $worker_status ?></div>
						<?php endif; ?>
					</td>
					<td><a href="<?php the_permalink()?>"><strong><?php the_title()?></strong></a></td>
					<td class="hidden-xs"><strong><?php
						$job_category = noo_get_post_meta($post->ID,'_job_category','');
						if( !empty( $job_category ) ) {
							$json_category = noo_json_decode($job_category);
							$job_categories = empty( $json_category ) ? array() : get_terms( 'job_category', array('include' => array_merge( $json_category, array(-1) ), 'hide_empty' => 0, 'fields' => 'names') );
							echo implode(', ', $job_categories );
						}
					?></strong></td>

					<td class="hidden-xs"><?php
						$desired_salary = subease_render_resume_element_text_only($fields['_desired_salary'], $post->ID, '', true);
						if( !empty( $desired_salary ) ) {
							echo '<span style="color: #216C2A;">';
							echo '<span style="font-weight: bold; font-size: 150%;">$</span>' . format_salary($desired_salary) . '/hr';
							echo '</span>';
						}
					?></td>

					<?php // Now start building the dynamic fields based on search criteria ?>
					<?php if ($is_job_type_set): ?>
    					<td class="hidden-xs">
    					<?php
							$value = "";
							$resume_job_types = subease_get_job_type_from_resume( $post );
							if( !empty( $resume_job_types ) ) {
								foreach ($resume_job_types as $resume_job_type) {
									$value .= '<a href="'.get_term_link($resume_job_type['object'],'job_type').'" style="white-space: nowrap; color: '.$resume_job_type['color'].'"><i class="fa fa-bookmark"></i>' .$resume_job_type['object']->name. '</a><br/>';
								}
							}
							echo $value;
    				    ?>
    					</td>
					<?php endif; // EndIf Experience Year ?>
					<?php if ($is_experience_year_set): ?>
    					<td class="hidden-xs">
    					<?php
    						$candidate_experience_year = noo_get_post_meta($post->ID,'_experience_year','');
    						echo $candidate_experience_year;
    				    ?>
    					</td>
					<?php endif; // EndIf Experience Year ?>

					<?php if ($is_language_set): ?>
						<td class="hidden-xs">
    					<?php
    						$candidate_language = noo_get_post_meta($post->ID,'_language','');
    						$candidate_languages = array();
    						if( !empty( $candidate_language ) ) :
    							$json_langauge = noo_json_decode($candidate_language);
    							if (empty( $json_langauge )) {
    							    $candidate_languages = array();
    							} else {
    							    $candidate_languages = array_map('ucfirst', $json_langauge);
    							}
    						?>
    						<?php echo implode(', ', $candidate_languages ); ?>
    						<?php endif; ?>
						</td>
					<?php endif; // EndIf Language ?>


					<?php if ($is_noo_resume_field__certifications_set): ?>
						<td class="hidden-xs"><?php subease_render_resume_element_text_only($fields['_certifications'], $post->ID, ', '); ?></td>
					<?php endif; // EndIf Certification ?>

					<?php if ($is_noo_resume_field__associations_set): ?>
						<td class="hidden-xs"><?php subease_render_resume_element_text_only($fields['_associations'], $post->ID, ', '); ?></td>
					<?php endif; // EndIf Associations ?>
				</tr>
			<?php endwhile;
			 if ($valid_resumes == (int) 0) { ?>
			     <tr>
			     	<td colspan="6"><h3><?php _e('No Resume available','noo')?></h3></td>
			   	</tr>
			 <?php } ?>
		<?php else:?>
			<?php
				$wp_query->max_num_pages = 0;
				list($title, $link) = jm_get_cannot_view_resume_message();
			?>
			<tr>
				<td colspan="6">
					<h3><?php echo $title; ?></h3>
					<?php if( !empty( $link ) ) echo $link; ?>
				</td>
			</tr>
		<?php endif;?>
	<?php else:?>
		<tr>
			<td colspan="6"><h3><?php _e('No Resume available','noo')?></h3></td>
		</tr>
	<?php endif;?>
	<?php if(!$ajax_item || $ajax_item == null ) ://ajax item ?>
				</tbody>
			</table>
		</div>
		<?php if($pagination) :
			if( $paginate == 'resume_nextajax') :
				if ( 1 < $wp_query->max_num_pages ) :
					?>
					<div class="pagination list-center"
						data-job-category="<?php echo esc_attr($job_category);?>"
						data-job-location="<?php echo esc_attr($job_location);?>"
						data-orderby="<?php echo esc_attr($orderby);?>"
						data-order="<?php echo esc_attr($order);?>"
						data-posts-per-page="<?php echo absint($posts_per_page)?>"
						data-current-page="1"
						data-max-page="<?php echo absint($wp_query->max_num_pages)?>">
						<a href="#" class="prev page-numbers disabled">
							<i class="fa fa-long-arrow-left"></i>
						</a>

						<a href="#" class="next page-numbers">
							<i class="fa fa-long-arrow-right"></i>
						</a>
					</div>
					<?php
				endif;
			else :

				( $live_search ? noo_pagination( '', $wp_query, $live_search) : noo_pagination( '', $wp_query) );

			endif;
		endif;
	?>
	</div>
	<?php
	endif;
endif;
?>