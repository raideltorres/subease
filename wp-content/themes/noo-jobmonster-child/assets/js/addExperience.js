
jQuery(document).ready(function() {
	jQuery(".add-experience").click ( function() {
		 var expCount = jQuery('input[type="text"][name*="_experience_employer"').length;
		 console.log ("Experience Count = " + expCount);
		 var $this = jQuery(this);
		jQuery.ajax({
			url : addExperience.ajax_url,
			type : 'post',
			data : {
				action : 'post_add_experience',
				offset : expCount
			},
			success : function( response ) {

				var $template = jQuery( response );
				$this.closest('.noo-metabox-addable').find('.noo-addable-fields').append( $template );
				$template.find('.form-control-editor').wysihtml5({
					"font-styles": true,
					"blockquote": true,
					"emphasis": true,
					"lists": true,
					"html": true,
					"link": true,
					"image": true,
					"stylesheets": [wysihtml5L10n.stylesheet_rtl]
				});
				$template.find('input:first-child').focus();
				return false;
			}
		});
	});
});

