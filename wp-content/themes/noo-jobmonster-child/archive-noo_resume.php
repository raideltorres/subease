<?php get_header(); ?>
<div class="container-wrap">
	<div class="main-content container-boxed offset">
		<div class="row resume-list">
			<div class="<?php noo_main_class(); ?>" role="main">
				<?php Noo_Resume::loop_display(); ?>
			</div> <!-- /.main -->
			<div class="search-sidebar"><?php get_sidebar(); ?></div>
		</div><!--/.row-->
	</div><!--/.container-boxed-->
</div><!--/.container-wrap-->

<?php get_footer(); ?>