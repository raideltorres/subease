<?php

function woocommerce_wp_select_multiple($field)
{
    global $thepostid, $post;

    $thepostid = empty($thepostid) ? $post->ID : $thepostid;
    $field['class'] = isset($field['class']) ? $field['class'] : 'select short';
    $field['style'] = isset($field['style']) ? $field['style'] : '';
    $field['wrapper_class'] = isset($field['wrapper_class']) ? $field['wrapper_class'] : '';
    $field['value'] = isset($field['value']) ? $field['value'] : get_post_meta($thepostid, $field['id'], true);
    $field['name'] = isset($field['name']) ? $field['name'] : $field['id'];

    // Custom attribute handling
    $custom_attributes = array();

    if (!empty($field['custom_attributes']) && is_array($field['custom_attributes'])) {

        foreach ($field['custom_attributes'] as $attribute => $value) {
            $custom_attributes[] = esc_attr($attribute) . '="' . esc_attr($value) . '"';
        }
    }

    echo '<p class="form-field ' . esc_attr($field['id']) . '_field ' . esc_attr($field['wrapper_class']) . '"><label for="' . esc_attr($field['id']) . '">' . wp_kses_post($field['label']) . '</label><select multiple="multiple id="' . esc_attr($field['id']) . '" name="' . esc_attr($field['name']) . '[]" class="' . esc_attr($field['class']) . '" style="' . esc_attr($field['style']) . '" ' . implode(' ', $custom_attributes) . '>';

    $val = array();
    if (!empty($field['value'])){
        $val = unserialize($field['value']);
    }

    $all_selected = in_array('all', $val) ? "selected='selected'" : '';
    echo '<option value="all" ' . $all_selected . '>' . esc_html__('All packages', 'noo') . '</option>';
    foreach ($field['options'] as $key => $value) {
        if (!empty($val) && in_array($key, $val)) {
            echo '<option value="' . $key . '" selected="selected">' . $value . '</option>';
        } else {
            echo '<option value="' . $key . '">' . $value . '</option>';
        }

    }

    echo '</select> ';

    if (!empty($field['description'])) {

        if (isset($field['desc_tip']) && false !== $field['desc_tip']) {
            echo wc_help_tip($field['description']);
        } else {
            echo '<span class="description">' . wp_kses_post($field['description']) . '</span>';
        }
    }
    echo '</p>';
}