<?php
if( !function_exists( 'jm_job_set_bookmarked' ) ) :
	function jm_job_set_bookmarked( $user_id = 0, $job_id = 0 ) {
		if( empty($user_id) ) {
			$user_id = get_current_user_id();
		}

		if( empty($user_id) ) {
			return false;
		}

		if( empty($job_id) ) {
			$job_id = get_the_ID();
		}

		$job_id = absint( $job_id );

		// if( empty($job_id) || 'noo_job' != get_post_type( $job_id ) ) {
		// 	return false;
		// }

		$bookmarks = get_option("noo_bookmark_job_{$user_id}");
		if( empty( $bookmarks ) || !is_array( $bookmarks ) ) {
			$bookmarks = array();
		}

		if( isset( $bookmarks[$job_id] ) && $bookmarks[$job_id] == 1 ) {
			return true;
		} else {
			$bookmarks[$job_id] = 1;
		}
		return update_option("noo_bookmark_job_{$user_id}", $bookmarks);
	}
endif;

if( !function_exists( 'jm_job_clear_bookmarked' ) ) :
	function jm_job_clear_bookmarked( $user_id = 0, $job_id = 0 ) {
		if( empty($user_id) ) {
			$user_id = get_current_user_id();
		}

		if( empty($user_id) ) {
			return false;
		}

		if( empty($job_id) ) {
			$job_id = get_the_ID();
		}

		$job_id = absint( $job_id );

		// if( empty($job_id) || 'noo_job' != get_post_type( $job_id ) ) {
		// 	return false;
		// }

		$bookmarks = get_option("noo_bookmark_job_{$user_id}", array());
		if( empty( $bookmarks ) || !is_array( $bookmarks ) ) {
			$bookmarks = array();
		}

		if( !isset($bookmarks[$job_id]) ) {
			return true;
		}

		unset($bookmarks[$job_id] );
		return update_option("noo_bookmark_job_{$user_id}", $bookmarks);
	}
endif;

if( !function_exists( 'jm_is_job_bookmarked' ) ) :
	function jm_is_job_bookmarked( $user_id = 0, $job_id = 0 ) {
		if( empty($user_id) ) {
			$user_id = get_current_user_id();
		}

		if( empty($user_id) ) {
			return false;
		}

		if( empty($job_id) ) {
			$job_id = get_the_ID();
		}

		if( empty($job_id) || 'noo_job' != get_post_type( $job_id ) ) {
			return false;
		}

		$job_id = absint( $job_id );

		$bookmarks = get_option("noo_bookmark_job_{$user_id}", array());

		if( empty( $bookmarks ) || !is_array( $bookmarks ) ) {
			return false;
		}

		return ( isset($bookmarks[$job_id]) && !empty($bookmarks[$job_id]) );
	}
endif;

if( !function_exists( 'jm_get_candidate_bookmarked_job' ) ) :
	function jm_get_candidate_bookmarked_job( $user_id = 0 ) {
		if( empty($user_id) ) {
			$user_id = get_current_user_id();
		}

		if( empty($user_id) ) {
			return array();
		}

		$bookmarks = get_option("noo_bookmark_job_{$user_id}", array());
		if( empty( $bookmarks ) || !is_array( $bookmarks ) ) {
			return array();
		}

		return $bookmarks;
	}
endif;
