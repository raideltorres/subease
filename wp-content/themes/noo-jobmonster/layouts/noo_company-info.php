<?php

$company_name		= get_post_field( 'post_title', $company_id );
$logo_company 		= Noo_Company::get_company_logo( $company_id );
?>
<div class="company-desc" itemscope itemtype="http://schema.org/Organization">
	<div class="company-header">
		<div class="company-featured"><a href="<?php echo get_permalink( $company_id ); ?>"><?php echo $logo_company;?></a></div>
		<h3 class="company-title" itemprop="name"><?php if( !is_singular( 'noo_company' ) ) : ?><a href="<?php echo get_permalink( $company_id ); ?>"><?php endif; ?><?php echo esc_html( $company_name );?><?php if( !is_singular( 'noo_company' ) ) : ?></a><?php endif; ?></h3>
	</div>
	<div class="company-info">
		<?php
		 	echo get_post_field( 'post_content', $company_id );
		 ?>

		<?php 
			// Job's social info
			$company_website 	= noo_get_post_meta($company_id, '_website', '');
			$facebook		= noo_get_post_meta( $company_id, "_facebook", '' );
			$twitter		= noo_get_post_meta( $company_id, "_twitter", '' );
			$google_plus	= noo_get_post_meta( $company_id, "_googleplus", '' );
			$linkedin		= noo_get_post_meta( $company_id, "_linkedin", '' );
			$instagram		= noo_get_post_meta( $company_id, "_instagram", '' );
			if($company_website || $facebook || $twitter || $google_plus || $linkedin || $instagram):
			?>
				<div class="job-social clearfix">
					<span class="noo-social-title"><?php _e('Connect with us','noo');?></span>
					<?php echo ( !empty($company_website) ? '<a title="' . esc_html__( 'Connect with us on Website' ,'noo' ) . '" rel="nofollow" class="noo-icon fa fa-link" href="' . esc_url( $company_website ) . '" target="_blank"></a>' : '' ); ?>
					<?php echo ( !empty($facebook) ? '<a title="' . esc_html__( 'Connect with us on Facebook' ,'noo' ) . '" rel="nofollow" class="noo-icon fa fa-facebook" href="' . esc_url( $facebook ) . '" target="_blank"></a>' : '' ); ?>
					<?php echo ( !empty($twitter) ? '<a title="' . esc_html__( 'Connect with us on Twitter' ,'noo' ) . '" rel="nofollow" class="noo-icon fa fa-twitter" href="' . esc_url( $twitter ) . '" target="_blank"></a>' : '' ); ?>
					<?php echo ( !empty($google_plus) ? '<a title="' . esc_html__( 'Connect with us on Google Plus' ,'noo' ) . '" rel="nofollow" class="noo-icon fa fa-google-plus" href="' . esc_url( $google_plus ) . '" target="_blank"></a>' : '' ); ?>
					<?php echo ( !empty($linkedin) ? '<a title="' . esc_html__( 'Connect with us on LinkedIn' ,'noo' ) . '" rel="nofollow" class="noo-icon fa fa-linkedin" href="' . esc_url( $linkedin ) . '" target="_blank"></a>' : '' ); ?>
					<?php echo ( !empty($instagram) ? '<a title="' . esc_html__( 'Connect with us on Instagram' ,'noo' ) . '" rel="nofollow" class="noo-icon fa fa-instagram" href="' . esc_url( $instagram ) . '" target="_blank"></a>' : '' ); ?>
				</div>
			<?php endif; ?>
			<?php
			if( $show_more_job ) :
				$exclude_this_job =  array();
				if( is_singular( 'noo_job' ) ) {
					$post_object = get_queried_object();
					$exclude_this_job  = get_queried_object_id();
				}

				$more_jobs = Noo_Company::get_company_jobs($company_id, (array) $exclude_this_job, 5 );
				if( !empty( $more_jobs ) ) :
			?>
					<div class="more-jobs clearfix">
						<strong><?php echo sprintf(__('More jobs from %s', 'noo'), $company_name); ?></strong>
						<ul>
							<?php foreach ($more_jobs as $job) : ?>
								<li><a class="more-job-title" href="<?php echo get_permalink( $job );?>"><?php echo get_the_title( $job );?></a></li>
							<?php endforeach; ?>
						</ul>
					</div>
				<?php endif; ?>
			<?php endif; ?>
	</div>
</div>